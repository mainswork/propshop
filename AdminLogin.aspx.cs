﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class AdminLogin : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAdminLogin_Click(object sender, EventArgs e)
    {
        
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            SqlCommand cmd = new SqlCommand("SELECT Admin_id,Admin_name from Admin where Admin_username=@emailid and Admin_password=@password", conn);
            cmd.Parameters.AddWithValue("@emailid", txtAdminEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@password", txtAdminPassword.Text.Trim());
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                Session["Admin_id"] = Convert.ToInt32(dr["Admin_id"]);
                Session["name"] = Convert.ToString(dr["Admin_name"]);
                Response.Redirect("AllProperty.aspx");
            }
            else
            {
                lblloginfailure.Visible = true;

            }
            dr.Close();
            conn.Close();
        }
        catch (Exception ex)
        {

        }

    }
    }