﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<style>
    ul li{
        list-style-type: none !important;
    }
    .card
    {
        min-height:450px;
     }
     .card-image
     {
         height:450px !important;
         overflow:hidden;
         }
      #map
      {
          height: 450px;
          }
</style>
<script>
//    var lat1 = Number(('#<%= lat.ClientID %>').val().trim());
//    var long = Number($('#<%= lng.ClientID %>').val().trim());
    var lat1 = 19.213711;
    var long = 72.841129;
//    var lat1 = document.getElementById('<%= lat.ClientID %>').value;
//    var long = document.getElementById('<%= lng.ClientID %>').value;
    var marker;
    function initMap() {
      
        var loc = { lat: lat1, lng: long };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom:,
            center: loc
        });
        marker = new google.maps.Marker({
            position: loc,
            draggable: true,
          animation: google.maps.Animation.DROP,
            map: map,
            title: 'Location'
        });
        marker.addListener('click', toggleBounce);
    }
    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUGlR82QEjYdKcW9bnuAu2cRmlWscxVQI&callback=initMap">
    </script>
 <br />
 <div class="container1">
 <div class="row">
 
 <div class="col s12 m12 l12 center">

<h4 class="text-center">About Us</h4>

 </div>

     <div class="row">
         <div class="col s12 m6 l6">
              <div class="card">
            <div class="card-image">
              <img src="resource/img/33.jpg" alt="propshop mision and vision" height="100%">
              </div>
              </div>
         </div>
         <div class="col s12 m6 l6">
         <div class="card">
             <div class="card-content">
              <span class="card-title">Mision Vision</span>
            
             <p><asp:Label ID="misionvision" runat="server"></asp:Label></p>
             </div>

    </div>     
        </div>
     </div>
       
      <div class="row">
         <div class="col s12 m6 l6">
               <div class="card">
            <div class="card-image">
              <img src="resource/img/aboutCompany.jpg" alt="propshop Company" height="100%">
              </div>
              </div>
         </div>
         <div class="col s12 m6 l6">
                     <div class="card">
             <div class="card-content">
              <span class="card-title">About Company</span>
               <p><asp:Label ID="abtCompany" runat="server"></asp:Label></p>
            </div>
            </div>
             
           
         </div>
     </div>

     <div class="row">
         <div class="col s12 m6 l6">
               <div class="card">
            <div class="card-image">
              <img src="resource/img/csr.jpg" alt="propshop csr" height="100%">
              </div>
              </div>
         </div>
         <div class="col s12 m6 l6">
          <div class="card">
             <div class="card-content">
              <span class="card-title">CSR</span>
               <p><asp:Label ID="csr" runat="server"></asp:Label></p>
              </div>
              </div>
             
            
         </div>
     </div>

     <div class="row">
         <div class="col s12 m6 l6">
               <div class="card">
            <div class="card-image">
              <img src="resource/img/team.jpg" alt="propshop team mem" height="100%">
              </div>
              </div>
         </div>
         <div class="col s12 m6 l6">
         <div class="card">
             <div class="card-content">
              <span class="card-title">Team Members</span>
                 <asp:BulletedList ID="teamMember" runat="server">        
             </asp:BulletedList>
              </div>
              </div>
         
           
         </div>
     </div>
     <div class="row">
         <div class="col s12 m12 l12">
            <h4 class="center">Contact Us</h4>
         </div>
        
         <asp:HiddenField ID="lat" runat="server"></asp:HiddenField>
         <asp:HiddenField ID="lng" runat="server"></asp:HiddenField>
       
 <div id="map" class="col s12 m6 l6 right"></div>

         
         <div class="col s12 m6 l6">
             <div class="card">
             <div class="card-content">
              <span class="card-title">Address</span>
              <p><asp:Label ID="address" runat="server"></asp:Label></p>
                <span class="card-title">Contact Person</span>
                  <asp:BulletedList ID="contactPerson" runat="server">
   
                 </asp:BulletedList>
                <span class="card-title">Email</span>
                  <asp:BulletedList ID="email" runat="server">
   
                    </asp:BulletedList>  
        
            </div>


             </div>
         </div>
     </div>
 


</div>
</div>
</asp:Content>

