﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Client : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    
    {
        if (!IsPostBack)
        {
            fillgridview();
        }

    }
    protected void fillgridview()
    {
        String query = "select * from PurchaseRequest_New where Purchase_id in (select MIN(Purchase_id) from PurchaseRequest_New where cust_contact in (select distinct cust_contact from PurchaseRequest_New) group by cust_contact)";
        SqlCommand cmd = new SqlCommand(query, conn);
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        gvClient.DataSource = ds.Tables[0];
        gvClient.DataBind();

    }
    protected void imgShowProperty_Click(object sender, EventArgs e)
    {
        try
        {
            String conId = Convert.ToString(((LinkButton)sender).CommandArgument);
           
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            String query = "select * from Property where Prop_id in (select distinct Prop_id from  PurchaseRequest_New where cust_contact='"+conId+"')";
            SqlCommand cmd = new SqlCommand(query, conn);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            dlstAllProperty.DataSource = ds.Tables[0];
            dlstAllProperty.DataBind();
            conn.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    protected void imgProperty_Click(object sender, ImageClickEventArgs e)
    {
        try
        {

            
            int propid = Convert.ToInt32(((ImageButton)sender).CommandArgument);
            
            Session["propid"] = propid;
            Response.Redirect("AdminPropDetails.aspx");
        }
        catch (Exception es)
        {
            throw es;
        }
    }


}