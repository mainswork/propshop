﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
public partial class Default2 : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (!IsPostBack)
        {
            loadProducts();
        }
    }
    private void loadProducts()
    {
        try
        {
            
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string query = "SELECT PID,PName,PPrice,PDescription,Image FROM Products;";
            SqlCommand cmd = new SqlCommand(query, conn);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            gvProduct.DataSource = ds.Tables[0];
            gvProduct.DataBind();

           
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }


        catch (Exception ex)
        {

        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        if (btnSubmit.Text=="Submit")
        {
            insertProduct();
        }
        else
        {
           // updateProduct();
            btnSubmit.Text = "Submit";
        }
        loadProducts();
    }
    private void insertProduct()
    {
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            String query = "Insert into [Products]([PName],[PPrice],[PDescription][Image]) values (@pname,@pprice,@pdescription,@image)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@pname", txtProdName.Text.Trim());
            cmd.Parameters.AddWithValue("@pprice", Convert.ToInt32(txtPrice.Text.Trim()));
            cmd.Parameters.AddWithValue("@pdescription", txtdescr.Text.Trim());
        //    cmd.Parameters.AddWithValue("@image", filename);
            int i = cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception e)
        {

        }

    }
    protected void gvProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
    
