﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="RegisterAdmin.aspx.cs" Inherits="RegisterAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="col-md-6">
        <div class="panel-primary">
            <div class="page-header">
                <h3>Register Admin..</h3>
            </div>
            <div class="jumbotron">
                <div class="container">
                    <fieldset>
                        <div class="form-group">
                            <asp:Label ID="lblName" runat="server" for="txtName" Text="Name:"></asp:Label>
                            <asp:TextBox ID="txtName" placeholder="Name" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Name is Mandatory"
                                Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Name is Mandatory"
                                ValidationGroup="vgEmployee" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="recName" runat="server" ErrorMessage="Please enter only character"
                                ControlToValidate="txtName" Display="Dynamic" ToolTip="Please enter only character" Text="*"
                                ValidationGroup="vgEmployee" ValidationExpression="^[a-zA-Z\s]+$"></asp:RegularExpressionValidator>


                             
                        </div>
                              </div>

                    
                        <div class="form-group">                 
                                <asp:Label CssClass="control-label" For="txtUsername" ID="lblusername" runat="server" Text="Username"></asp:Label>
                                <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" placeholder="Username"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ErrorMessage="username is Mandatory" Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Username is Mandatory" ValidationGroup="vgEmployee" ControlToValidate="txtUsername"></asp:RequiredFieldValidator>
                             </div>

                        <div class="form-group">
                                <asp:Label CssClass="control-label" For="txtPassword" ID="Password" runat="server" Text="Password"></asp:Label>
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" placeholder="Password" TextMode="Password" Text="*"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Password is Mandatory" Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Password is Mandatory" ValidationGroup="vgEmployee" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>    
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="form-control btn btn-primary" ValidationGroup="vgEmployee" OnClick="btnRegister_Click" />
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

