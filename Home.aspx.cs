﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;
using System.Text;
public partial class Home : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                this.BindData();
                fillDDLLocation();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void fillDDLLocation()
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            String query = "select  distinct(location) As loc from complex;";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                ddlLocation.Items.Add(Convert.ToString(dr["loc"]));
            }
            dr.Close();
            conn.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindData()
    {
        try
        {

            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            /*slider imges*/
            String imgDataListOld = "";
            String query = "select * from slider_images";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                imgDataListOld += dr["img_base64"].ToString() + "-";
            }

            if (imgDataListOld.Length > 0)
            {
                imgDataListOld = imgDataListOld.Substring(0, imgDataListOld.Length - 1);
            }
            sliderPreImgData.Value = imgDataListOld;
            /*slider images*/
            conn.Close();


            ddlLocation.SelectedIndex = 0;
            ddlPropFor.SelectedIndex = 0;
            ddlBeds.SelectedIndex = 0;
            ddlPropertType.SelectedIndex = 0;
            lblView.Text = "Featured Property";
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            query = "SELECT top 8 c.comp_name,c.location,c.pincode,p.Prop_id,p.price,p.images,p.Bhk,p.Bathroom,p.Area,p.types1,p.prop FROM complex c JOIN Property p ON c.Comp_id=p.Comp_id where p.isApproved='Y' and p.featured_status='Featured' and prop='Property';";
            cmd = new SqlCommand(query, conn);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            dlstProperty.DataSource = ds.Tables[0];
            dlstProperty.DataBind();
            query = "SELECT top 8 c.comp_name,c.location,c.pincode,p.Prop_id,p.price,p.images,p.Bhk,p.Bathroom,p.Area,p.types1,p.prop FROM complex c JOIN Property p ON c.Comp_id=p.Comp_id where p.isApproved='Y' and p.featured_status='Featured' and prop='Project';";
            cmd = new SqlCommand(query, conn);
            ds = new DataSet();
            da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            rptPeoject.DataSource = ds.Tables[0];
            rptPeoject.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindData(string query)
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            SqlCommand cmd = new SqlCommand(query, conn);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            dlstProperty.DataSource = ds.Tables[0];
            dlstProperty.DataBind();
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblView.Text = "Property Found";
            }
            else
            {
                lblView.Text = "No Property Found";
            }
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }


            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void imgProperty_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int propid = Convert.ToInt32(((ImageButton)sender).CommandArgument);
            //Response.Redirect("PropertyDetails.aspx?Prop_id=" + propid);
            Session["propid"] = propid;
            Response.Redirect("PropertyDetails.aspx");

            //if (Session["cust_id"] != null)
            //{
            //    int propid = Convert.ToInt32(((ImageButton)sender).CommandArgument);
            //    //Response.Redirect("PropertyDetails.aspx?Prop_id=" + propid);
            //    Session["propid"] = propid;
            //    Response.Redirect("PropertyDetails.aspx");
            //}

            //else
            //{
            //    Response.Redirect("Login1.aspx");
            //}
        }
        catch (Exception es)
        {
            throw es;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        //if ((txtMinArea.Text.Length > 0) || (txtMaxArea.Text.Length > 0))
        //{
        //    if ((Convert.ToInt32(txtMinArea.Text.Length) > 0) && (Convert.ToInt32(txtMaxArea.Text.Length) > 0))
        //    {
        //        // allow.. do nothing
        //    }
        //    else
        //    {
               
        //        lblError.Text = "if you enter MinArea then MaxArea is required";
        //        return;
        //    }

        //    if (Convert.ToInt32(txtMinArea.Text) > Convert.ToInt32(txtMaxArea.Text))
        //    {
        //        lblError.Text = "Min cannot be greater then MAX";

        //        return;
        //    }
        //    else { lblError.Text = ""; }
       
        //}

        //if ((txtMinPrice.Text.Length > 0) || (txtMaxPrice.Text.Length > 0))
        //{
        //    if ((Convert.ToInt32(txtMinPrice.Text.Length) > 0) && (Convert.ToInt32(txtMaxPrice.Text.Length) > 0))
        //    {
        //        // allow.. do nothing
        //    }
        //    else
        //    {

        //        lblError1.Text = "if you enter MinPrice then MaxPrice is required";
        //        return;
        //    }

        //    if (Convert.ToInt32(txtMinPrice.Text) > Convert.ToInt32(txtMaxPrice.Text))
        //    {                                                                                                                                              
        //        lblError1.Text = "Min cannot be greater then MAX";
        //        return;
        //    }
        //    else { lblError1.Text = ""; }
  
        //}

        StringBuilder query = new StringBuilder("select * from property P INNER JOIN complex C ON P.Comp_id = C.Comp_id WHERE p.isApproved='Y' AND p.featured_status='Featured'");
        if (ddlLocation.SelectedIndex > 0)
        {
            query.Append(" AND C.Location ='" + ddlLocation.SelectedItem.Text + "'");
        }
        if (ddlPropFor.SelectedIndex > 0)
        {
            query.Append(" AND P.Prop_for ='" + ddlPropFor.SelectedItem.Text.Trim() + "'");
        }
        if (ddlBeds.SelectedIndex > 0)
        {
            query.Append(" AND P.Bhk ='" + ddlBeds.SelectedItem.Text.Trim() + "'");
        }
        //if (ddlPropertyStatus.SelectedIndex > 0)
        //{
        //    query.Append(" AND P.prop_status='" + ddlPropertyStatus.SelectedItem.Text.Trim() + "'");
        //}
        if (ddlPropertType.SelectedIndex > 0)
        {
            query.Append(" AND P.types1='" + ddlPropertType.SelectedValue + "'");
        }
        //if (txtMinPrice.Text.Length > 0)
        //{
        //    query.Append(" AND P.price between " + Convert.ToInt32(txtMinPrice.Text.Trim()) + " AND " + Convert.ToInt32(txtMaxPrice.Text.Trim()));
        //}
        //if (txtMinArea.Text.Length > 0)
        //{
        //    query.Append(" AND P.area between " + Convert.ToInt32(txtMinArea.Text.Trim()) + " AND " + Convert.ToInt32(txtMaxArea.Text.Trim()));
        //}
        BindData(query.ToString());
    }

    protected void allProp_Click(object sender, EventArgs e)
    {

        this.BindData();
    }
}
