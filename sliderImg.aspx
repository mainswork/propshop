﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="sliderImg.aspx.cs" Inherits="sliderImg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script>
        var imgData = [];
        var base64Text;
        $(document).ready(function () {

            var imgDataStr = $('#<%= sliderPreImgData.ClientID %>').val();
        if (imgDataStr != '' && imgDataStr != undefined) {
            var imgDataList = imgDataStr.split("-");
            for (var i = 0; i < imgDataList.length; i++) {
                var imgDataSingle = imgDataList[i].split(",");
                imgData.push([imgDataSingle[0] + "," + imgDataSingle[1], imgDataSingle[2]]);
            }
            resetImageTble();
        }
        $('#<%= sliderImgData.ClientID %>').val('');

        $('#<%= fuImage.ClientID %>').on('change', function (evt) {
            var file = $('#<%= fuImage.ClientID %>')[0].files[0];
            if (file.length == 0) {
                alert("file not selected");
                return false;
            }
            var filename = $('#<%= fuImage.ClientID %>').val();
            var extension = filename.replace(/^.*\./, '');
            if (extension == filename) {
                extension = '';
            } else {
                extension = extension.toLowerCase();
            }
            switch (extension) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                    {
                        var tgt = evt.target || window.event.srcElement, files = tgt.files;

                        var fileSize = files[0].size;
                        var kb = fileSize / 1024;
                        if (kb > 500) {
                            alert("Please upload less than 500kb sizes image");
                            base64Text = '';
                            $('#<%= fuImage.ClientID %>').val('');
                            $("#imgAdmin").attr('src', '');
                            $("#imgAdmin").change();
                            $('#<%= txtImgName.ClientID %>').val('');
                            return false;
                        }
                        break;
                    }
                default:
                    alert('Only png and jpeg image is allowed!');
                    base64Text = '';
                    $('#<%= fuImage.ClientID %>').val('');
                    $("#imgAdmin").attr('src', '');
                    $("#imgAdmin").change();
                    $('#<%= txtImgName.ClientID %>').val('');
                    return false;
            }
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                var encodedFile = reader.result;
                base64Text = encodedFile;
                $("#imgAdmin").attr('src', encodedFile);
                $("#imgAdmin").change();
            }
        });

        $('#AddImg').click(function () {

            if (base64Text == '' || base64Text == undefined) {
                alert("file not selected");
                return false;
            }

            var imgName = $('#<%= txtImgName.ClientID %>').val();
            if (imgName == '' || imgName == undefined) {
                alert("Enter Image Name");
                return false;
            }

            imgData.push([base64Text, imgName]);
            resetImageTble();
            base64Text = '';
            $('#<%= fuImage.ClientID %>').val('');
            $("#imgAdmin").attr('src', '');
            $("#imgAdmin").change();
            $('#<%= txtImgName.ClientID %>').val('');
        });

        $('#<%= btnSubmit.ClientID %>').click(function () {

            if (imgData.length == 0) {
                alert("Select Images Of Your Properties");
                return false;
            }

            var imgStr = '';
            for (var i = 0; i < imgData.length; i++) {
                var base64Text = imgData[i][0];
                var imgName = imgData[i][1];
                imgStr += base64Text + "," + imgName + "-";

            }
            imgStr = imgStr.substring(0, imgStr.length - 1);
            $('#<%= sliderImgData.ClientID %>').val(imgStr);
            //alert($('#<%= sliderImgData.ClientID %>').val());
            $('#imageListTbl').empty();
            imgData = [];
        });
    });

    function resetImageTble() {

        $('#imageListTbl').empty();

        for (var i = 0; i < imgData.length; i++) {
            var base64Text = imgData[i][0];
            var imgName = imgData[i][1];
            $('#imageListTbl').append('<tr>' +
                                        '<td> <img src="' + base64Text + '" height="50px" width="50px"/></td>' +
                                        '<td>' + imgName + '</td>' +
                                        '<td>' +
                                        '    <button class="btn" onclick="deleteImg(' + i + ')"><i class="fa fa-remove"></i></button>' +
                                        '</td>' +
                                    '</tr>');
        }
    }

    function deleteImg(srno) {
        imgData.splice(srno, 1);
        resetImageTble();
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<div class="row">
        <div class="col s12 m12 l12 center">
           
                <h5>
                   Slider Image</h5>
            </div>
          
                <div class="container">
                        
                            <div class="row">
                                <div class="col s12 m12 l12 z-depth-3">
                                     <div class="input-field col s12 m6 l6">
                                        <asp:FileUpload runat="server" ID="fuImage" accept="image/.jpg" data-type='image'/>
                                        <img src="" Height="50" Width="50" Visible="true" id="imgAdmin" />                                
                                    </div>
                                    <div class="input-field col s12 m4 l4">
                                    <label for="txtImgName">Image Name</label>
                                         <asp:TextBox ID="txtImgName" CssClass="form-control" runat="server"></asp:TextBox>
                                     </div>
                                     <div class="input-field col s12 m2 l2">
                                        <input id="AddImg"  type="button"  class="btn center-block" value="Add"/>
                                     </div>

                                     <div class="col s12 m12 l12">
                                        <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Image</th>
                                            <th>Image Name</th>
                                            <th>Delete</th>
                                            </tr>
                                        </thead>
                                <tbody id="imageListTbl">
                                    <tr>
                                        <td> <img src="" height="50px" width="50px"/></td>
                                        <td>image name</td>
                                        <td>
                                            <button class="btn"><i class="fa fa-remove"></i></button>
                                        </td>
                                    </tr>
                                 </tbody>
                                </table>
                                         <hr />
                                </div>
                                    <div class="row">
                                     <div class="col s12 m12 l12 center">
                                        <asp:HiddenField ID="sliderImgData" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="sliderPreImgData" runat="server"></asp:HiddenField>
                                          <asp:Button ID="btnSubmit" type="submit" runat="server" CssClass=" btn btn-info center-block " Text="Submit" OnClick="btnSubmit_Click" />
                                    </div>
                                    </div>
                                   
                                </div>
                                
                            
                            </div>
                        <div class="col-lg-12">
                               
                            
                        </div>
                        <div class="form-group">
                            
                            <div class="col-lg-6 ">
                              
                            </div>
                        </div>
                     </fieldset>
            </div>
            </div>
</asp:Content>

