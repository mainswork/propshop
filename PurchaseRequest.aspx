﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="PurchaseRequest.aspx.cs" Inherits="PurchaseRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<style type="text/css">
        td, th
        {
            text-align: center;
        }
    </style>
    <div>
        <div class="page-header">
            <h2>
                Client Information..</h2>
        </div>
        <div class="jumbotron">
            <div class="container">
                <fieldset>
                    <div class="form-group">
                        <div class="col-md-4">
                        <asp:DropDownList ID="ddlPurchaseReq" runat="server" CssClass=" form-control dropdown dropdown-toggle ">
                            <asp:ListItem Value="type">Type</asp:ListItem>
                            <asp:ListItem Value="location">Location</asp:ListItem>
                            <asp:ListItem Value="prop_for">For</asp:ListItem>
                        </asp:DropDownList>
                        </div>
                        <div class="col-md-4">
                        <asp:TextBox ID="TxtSearch" runat="server" CssClass="form-control"></asp:TextBox></div>
                       <div class="col-md-3">
                        <asp:Button ID="Button1" runat="server" Text="Search" CssClass="btn-primary" /></div>
                    </div>
                    <div class="col-md-6">
                        <asp:GridView ID="gvPurchaseReq" runat="server" AutoGenerateColumns="False" Visible="True"
                            DataKeyNames="Cust_id" 
                            CssClass="table table-bordered table-condensed table-hover table-responsive" 
                            BackColor="White">
                            <AlternatingRowStyle BackColor="LightGray" />
                            <Columns>
                                <asp:BoundField DataField="Prop_id" HeaderText="Property Id" />
                                <asp:BoundField DataField="name" HeaderText="Name" />
                                <asp:BoundField DataField="email" HeaderText="Email" />
                                <asp:BoundField DataField="contact" HeaderText="Contact" />
                               
                                        <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                        <asp:TextBox ID="txtStatus"  runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                                                
                                   
                
                                <asp:TemplateField HeaderText="View Property">
                                    <ItemTemplate>
                                
                                    <asp:LinkButton ID="imgShowProperty"  CausesValidation="False" CommandArgument="<% Eval(Prop_id) %>"
                                            OnClick="imgShowProperty_Click" CssClass="fa  fa-user-plus" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>                                       
                                    
                             
                                        
                             
                            </Columns>
                        </asp:GridView>
                        </div>
                        <div class="col-md-6">
                        <asp:GridView ID="gvOwner" runat="server" AutoGenerateColumns="False" Visible="False"
                          
                            CssClass="table table-bordered table-condensed table-hover table-responsive" 
                            BackColor="White">
                            <AlternatingRowStyle BackColor="LightGray" />
                            <Columns>
                                
                                <asp:BoundField DataField="name" HeaderText="Owner Name" />
                                <asp:BoundField DataField="contact" HeaderText="Owner Contact" />
                                <asp:BoundField DataField="email" HeaderText="Owner Email" />
                               
                                
                            </Columns>
                            
                        </asp:GridView>
                        </div>

                             </fieldset>
            
        </div>
    </div>
</asp:Content>

