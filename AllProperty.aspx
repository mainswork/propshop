﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AllProperty.aspx.cs" Inherits="AllProperty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">

    $(document).ready(function () {
        var table = $('[id*=gvProperty]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
            "responsive": true,
            "sPaginationType": "full_numbers",
            dom: '<"row"<"col s6 m4 l4 left"l><"col s12 m4 l4 center"B><"col s6 m4 l4"f>><"row"<"col s12 m12 l12"<"tblData" t>>>ip',
            scrollX: true,
            responsive: false,
            "oLanguage": {
                "sLengthMenu": "Display records  _MENU_"
  },
            buttons: {
                buttons: [
            { extend: 'pdf', className: 'btn' },
            { extend: 'excel', className: 'excelButton btn' },
             { extend: 'print', className: 'PrintButton btn' }

                        ]             
            },
            "columnDefs": [
             { "width": "1%", "targets": 0 },
             { "width": "1%", "targets": 1 },
             { "width": "8%", "targets": 2 },
             { "width": "1%", "targets": 3 },
             { "width": "1%", "targets": 4 },
             { "width": "1%", "targets": 5 },
             { "width": "1%", "targets": 6 },
             { "width": "1%", "targets": 7 },
             { "width": "1%", "targets": 8 },
             { "width": "1%", "targets": 9 },
             { "width": "1%", "targets": 10 },
             { "width": "1%", "targets": 11 },
             { "width": "1%", "targets": 12 },
             { "width": "1%", "targets": 13 },
             { "width": "1%", "targets": 14 },
             { "width": "1%", "targets": 15 },
             { "width": "1%", "targets": 16 },
             { "width": "1%", "targets": 17 },
             { "width": "1%", "targets": 18 }

             ],
            "order": [[1, 'asc']]

        });
        table.on('order.dt search.dt', function () {
            table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

    });

    </script>
<style type="text/css">
        td, th
        {
            text-align: center;
        }
        .tblData
        {   
            width:100%;
            overflow-x: auto;
         }
          .tblData th,.tblData td
          {
                border:1px solid grey;
              }
         .container
         {
             margin-left:3% !important;
             margin-right:3% !important;
             }
             /*css for length (display records) start*/
     
 
	.dataTables_length select
	{
	    display:block !important;
	    
	    }
	
     /*css for length (display records) end*/
    </style>
    <div class="row">
       
            <h4 class="center">
                All Property</h4>
    
        <div class="jumbotron jumbotron-fluid">
            <div class="container-fluid ">
      
                    <div class="form-group">
                        
                    <div class="">
                        <asp:GridView ID="gvProperty" runat="server" AutoGenerateColumns="False" Visible="True"
                             CssClass="table table-bordered table-condensed table-hover table-responsive" BackColor="White">
                            <AlternatingRowStyle BackColor="LightGray" />
                            <Columns>
                            <asp:BoundField DataField="" HeaderText="Sr. No" />
                                 <asp:hyperlinkfield datatextfield="prop_id" datanavigateurlfields="prop_id" datanavigateurlformatstring="updateProperty.aspx?propid={0}"
            headertext="PropId" />
                               
                                <asp:BoundField DataField="Floor1" HeaderText="floor" />
                                <asp:BoundField DataField="Bhk" HeaderText="Beds" />
                                <asp:BoundField DataField="Bathroom" HeaderText="Bathroom" />
                                <asp:BoundField DataField="types1" HeaderText="Property Type" />
                                <asp:BoundField DataField="prop_status" HeaderText="Property Status" />
                                <asp:BoundField DataField="prop_for" HeaderText="For" />
                                <asp:BoundField DataField="Area" HeaderText="Area(sq.ft)" />
                                <asp:BoundField DataField="price" HeaderText="Price" />
                                <asp:BoundField DataField="comp_name" HeaderText="Complex Name" />
                                <asp:BoundField DataField="location" HeaderText="Location" />
                                <asp:BoundField DataField="address1" HeaderText="Address" />
                                <asp:BoundField DataField="city" HeaderText="City" />
                                <asp:BoundField DataField="state" HeaderText="State" />
                                <asp:BoundField DataField="Pincode" HeaderText="Pincode" />                                
                                <asp:BoundField DataField="prop" HeaderText="Property / Project" />
                                
                                <asp:hyperlinkfield datatextfield="featured_status" datanavigateurlfields="prop_id" datanavigateurlformatstring="AllProperty.aspx?propid={0}&type=featured" HeaderText="Featured / UnFeatured"/>
                                <asp:hyperlinkfield datatextfield="isApproved" datanavigateurlfields="prop_id" datanavigateurlformatstring="AllProperty.aspx?propid={0}&type=disable" HeaderText="Action"/>
                            </Columns>
                        </asp:GridView>
                        <asp:GridView ID="gvPropinfo" runat="server" AutoGenerateColumns="False" Visible="True"
                             CssClass="table table-bordered table-condensed table-hover table-responsive" BackColor="White">
                            <AlternatingRowStyle BackColor="LightGray" />
                            <Columns>
                               
                                

                          
                            </Columns>
                        </asp:GridView>
                        

                              
                        </div>
                    </div>
                 </div>
        </div>
</div>
</asp:Content>

