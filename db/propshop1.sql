USE [Propshop]
GO
/****** Object:  Table [dbo].[complex]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[complex](
	[Comp_id] [int] IDENTITY(1,1) NOT NULL,
	[comp_name] [nvarchar](50) NULL,
	[location] [nvarchar](20) NOT NULL,
	[address1] [nvarchar](150) NOT NULL,
	[city] [nchar](20) NOT NULL,
	[state] [nchar](25) NOT NULL,
	[pincode] [int] NOT NULL,
	[Latitude] [numeric](18, 10) NULL,
	[Longitude] [numeric](18, 10) NULL,
 CONSTRAINT [PK_complex] PRIMARY KEY CLUSTERED 
(
	[Comp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[city]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[city](
	[city_Id] [int] IDENTITY(1,1) NOT NULL,
	[state_Id] [int] NOT NULL,
	[city] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_city] PRIMARY KEY CLUSTERED 
(
	[city_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[Admin_id] [int] IDENTITY(1,1) NOT NULL,
	[Admin_name] [nvarchar](50) NOT NULL,
	[Admin_username] [nvarchar](50) NOT NULL,
	[Admin_password] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[Admin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[about_us]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[about_us](
	[about_us_id] [bigint] IDENTITY(1,1) NOT NULL,
	[mission_vision] [varchar](1000) NOT NULL,
	[about_co] [varchar](1000) NOT NULL,
	[co_address] [varchar](1000) NOT NULL,
	[csr] [varchar](1000) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[about_us_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[state]    Script Date: 04/16/2018 00:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[state](
	[state_Id] [int] IDENTITY(1,1) NOT NULL,
	[country_Id] [int] NOT NULL,
	[state] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_state] PRIMARY KEY CLUSTERED 
(
	[state_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PurchaseRequest]    Script Date: 04/16/2018 00:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseRequest](
	[P_id] [int] IDENTITY(1,1) NOT NULL,
	[cust_id] [int] NOT NULL,
	[Prop_id] [int] NOT NULL,
	[PurchaseReqDate] [date] NOT NULL,
	[Status1] [nvarchar](20) NULL,
 CONSTRAINT [PK_PurchaseRequest] PRIMARY KEY CLUSTERED 
(
	[P_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[property_inquiry]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[property_inquiry](
	[inq_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[contact] [nchar](10) NOT NULL,
	[prop_for] [nvarchar](20) NULL,
	[type] [nvarchar](20) NULL,
	[bedrooms] [nchar](10) NULL,
	[bathrooms] [nchar](10) NULL,
	[city] [nvarchar](10) NOT NULL,
	[location] [nvarchar](20) NOT NULL,
	[budget_min] [nvarchar](10) NULL,
	[budget_max] [nvarchar](10) NULL,
	[parking_type] [nchar](10) NULL,
 CONSTRAINT [PK_propert_inquiry] PRIMARY KEY CLUSTERED 
(
	[inq_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property](
	[Prop_id] [int] IDENTITY(1,1) NOT NULL,
	[Comp_id] [int] NOT NULL,
	[Floor1] [nvarchar](50) NOT NULL,
	[Area] [int] NOT NULL,
	[Bhk] [nvarchar](50) NOT NULL,
	[Bathroom] [nchar](10) NOT NULL,
	[types1] [nvarchar](20) NOT NULL,
	[prop_status] [nvarchar](50) NOT NULL,
	[prop_for] [nvarchar](10) NOT NULL,
	[price] [int] NOT NULL,
	[owner_id] [int] NOT NULL,
	[isApproved] [nchar](1) NULL,
	[images] [nvarchar](100) NULL,
	[parking] [nvarchar](20) NULL,
	[lift] [nvarchar](20) NULL,
	[water] [nvarchar](20) NULL,
	[fire] [nvarchar](20) NULL,
	[gym] [nvarchar](20) NULL,
	[power1] [nvarchar](20) NULL,
	[child] [nvarchar](20) NULL,
	[prop] [nvarchar](10) NULL,
 CONSTRAINT [PK_Property] PRIMARY KEY CLUSTERED 
(
	[Prop_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[customer]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customer](
	[cust_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[contact] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[SecurityQuestion] [nvarchar](40) NULL,
	[Answer] [nvarchar](20) NULL,
	[cust_type] [nvarchar](8) NULL,
 CONSTRAINT [PK_customer] PRIMARY KEY CLUSTERED 
(
	[cust_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[country]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[country](
	[country_Id] [int] IDENTITY(1,1) NOT NULL,
	[country] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_country] PRIMARY KEY CLUSTERED 
(
	[country_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[contact_details]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contact_details](
	[contact_details_id] [bigint] IDENTITY(1,1) NOT NULL,
	[contact_name] [varchar](1000) NOT NULL,
	[mob_no] [bigint] NOT NULL,
	[email_id] [varchar](50) NULL,
	[about_us_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[contact_details_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[management_team]    Script Date: 04/16/2018 00:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[management_team](
	[mng_team_id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](1000) NOT NULL,
	[about_us_id] [bigint] NOT NULL,
	[position] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[mng_team_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK__contact_d__about__1A14E395]    Script Date: 04/16/2018 00:40:48 ******/
ALTER TABLE [dbo].[contact_details]  WITH CHECK ADD FOREIGN KEY([about_us_id])
REFERENCES [dbo].[about_us] ([about_us_id])
GO
/****** Object:  ForeignKey [FK__managemen__about__1B0907CE]    Script Date: 04/16/2018 00:40:48 ******/
ALTER TABLE [dbo].[management_team]  WITH CHECK ADD FOREIGN KEY([about_us_id])
REFERENCES [dbo].[about_us] ([about_us_id])
GO
