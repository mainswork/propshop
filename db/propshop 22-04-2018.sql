USE [master]
GO
/****** Object:  Database [Propshop]    Script Date: 22-04-2018 17:20:35 ******/
CREATE DATABASE [Propshop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Propshop', FILENAME = N'E:\Sachin\propshop Realty\propshop Realty\Propshop.mdf' , SIZE = 10240KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Propshop_log', FILENAME = N'E:\Sachin\propshop Realty\propshop Realty\Propshop_log.ldf' , SIZE = 3456KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Propshop] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Propshop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Propshop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Propshop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Propshop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Propshop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Propshop] SET ARITHABORT OFF 
GO
ALTER DATABASE [Propshop] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Propshop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Propshop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Propshop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Propshop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Propshop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Propshop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Propshop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Propshop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Propshop] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Propshop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Propshop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Propshop] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Propshop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Propshop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Propshop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Propshop] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Propshop] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Propshop] SET  MULTI_USER 
GO
ALTER DATABASE [Propshop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Propshop] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Propshop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Propshop] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Propshop] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Propshop', N'ON'
GO
USE [Propshop]
GO
/****** Object:  Table [dbo].[about_us]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[about_us](
	[about_us_id] [bigint] IDENTITY(1,1) NOT NULL,
	[mission_vision] [varchar](1000) NOT NULL,
	[about_co] [varchar](1000) NOT NULL,
	[co_address] [varchar](1000) NOT NULL,
	[csr] [varchar](1000) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[about_us_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[Admin_id] [int] IDENTITY(1,1) NOT NULL,
	[Admin_name] [nvarchar](50) NOT NULL,
	[Admin_username] [nvarchar](50) NOT NULL,
	[Admin_password] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[Admin_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[city]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[city](
	[city_Id] [int] IDENTITY(1,1) NOT NULL,
	[state_Id] [int] NOT NULL,
	[city] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_city] PRIMARY KEY CLUSTERED 
(
	[city_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[complex]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[complex](
	[Comp_id] [int] IDENTITY(1,1) NOT NULL,
	[comp_name] [nvarchar](50) NULL,
	[location] [nvarchar](20) NOT NULL,
	[address1] [nvarchar](150) NOT NULL,
	[city] [nchar](20) NOT NULL,
	[state] [nchar](25) NOT NULL,
	[pincode] [int] NOT NULL,
	[Latitude] [numeric](18, 10) NULL,
	[Longitude] [numeric](18, 10) NULL,
 CONSTRAINT [PK_complex] PRIMARY KEY CLUSTERED 
(
	[Comp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[contact_details]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contact_details](
	[contact_details_id] [bigint] IDENTITY(1,1) NOT NULL,
	[contact_name] [varchar](1000) NOT NULL,
	[mob_no] [bigint] NOT NULL,
	[email_id] [varchar](50) NULL,
	[about_us_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[contact_details_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[country]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[country](
	[country_Id] [int] IDENTITY(1,1) NOT NULL,
	[country] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_country] PRIMARY KEY CLUSTERED 
(
	[country_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[customer]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customer](
	[cust_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[contact] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[SecurityQuestion] [nvarchar](40) NULL,
	[Answer] [nvarchar](20) NULL,
	[cust_type] [nvarchar](8) NULL,
 CONSTRAINT [PK_customer] PRIMARY KEY CLUSTERED 
(
	[cust_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[management_team]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[management_team](
	[mng_team_id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](1000) NOT NULL,
	[about_us_id] [bigint] NOT NULL,
	[position] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[mng_team_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Property]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property](
	[Prop_id] [int] IDENTITY(1,1) NOT NULL,
	[Comp_id] [int] NOT NULL,
	[Floor1] [nvarchar](50) NOT NULL,
	[Area] [int] NOT NULL,
	[Bhk] [nvarchar](50) NOT NULL,
	[Bathroom] [nchar](10) NOT NULL,
	[types1] [nvarchar](20) NOT NULL,
	[prop_status] [nvarchar](50) NOT NULL,
	[prop_for] [nvarchar](10) NOT NULL,
	[price] [int] NOT NULL,
	[owner_id] [int] NOT NULL,
	[isApproved] [nchar](1) NULL,
	[images] [nvarchar](100) NULL,
	[parking] [nvarchar](20) NULL,
	[lift] [nvarchar](20) NULL,
	[water] [nvarchar](20) NULL,
	[fire] [nvarchar](20) NULL,
	[gym] [nvarchar](20) NULL,
	[power1] [nvarchar](20) NULL,
	[child] [nvarchar](20) NULL,
	[prop] [nvarchar](10) NULL,
 CONSTRAINT [PK_Property] PRIMARY KEY CLUSTERED 
(
	[Prop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[property_imgages]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[property_imgages](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[img_name] [varchar](100) NULL,
	[img_base_64] [varchar](max) NULL,
	[property_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[property_inquiry]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[property_inquiry](
	[inq_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[contact] [nchar](10) NOT NULL,
	[prop_for] [nvarchar](20) NULL,
	[type] [nvarchar](20) NULL,
	[bedrooms] [nchar](10) NULL,
	[bathrooms] [nchar](10) NULL,
	[city] [nvarchar](10) NOT NULL,
	[location] [nvarchar](20) NOT NULL,
	[budget_min] [nvarchar](10) NULL,
	[budget_max] [nvarchar](10) NULL,
	[parking_type] [nchar](10) NULL,
 CONSTRAINT [PK_propert_inquiry] PRIMARY KEY CLUSTERED 
(
	[inq_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurchaseRequest]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseRequest](
	[P_id] [int] IDENTITY(1,1) NOT NULL,
	[cust_id] [int] NOT NULL,
	[Prop_id] [int] NOT NULL,
	[PurchaseReqDate] [date] NOT NULL,
	[Status1] [nvarchar](20) NULL,
 CONSTRAINT [PK_PurchaseRequest] PRIMARY KEY CLUSTERED 
(
	[P_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[state]    Script Date: 22-04-2018 17:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[state](
	[state_Id] [int] IDENTITY(1,1) NOT NULL,
	[country_Id] [int] NOT NULL,
	[state] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_state] PRIMARY KEY CLUSTERED 
(
	[state_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[about_us] ON 

INSERT [dbo].[about_us] ([about_us_id], [mission_vision], [about_co], [co_address], [csr]) VALUES (1, N'', N'', N'', N'')
SET IDENTITY_INSERT [dbo].[about_us] OFF
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([Admin_id], [Admin_name], [Admin_username], [Admin_password]) VALUES (2, N'Nehal', N'admin', N'123')
INSERT [dbo].[Admin] ([Admin_id], [Admin_name], [Admin_username], [Admin_password]) VALUES (3, N'Nishit', N'admin1', N'NishitRV')
INSERT [dbo].[Admin] ([Admin_id], [Admin_name], [Admin_username], [Admin_password]) VALUES (4, N'Uttam', N'admin6', N'456')
INSERT [dbo].[Admin] ([Admin_id], [Admin_name], [Admin_username], [Admin_password]) VALUES (5, N'Akash', N'AkshAdmin', N'789')
INSERT [dbo].[Admin] ([Admin_id], [Admin_name], [Admin_username], [Admin_password]) VALUES (6, N'Akash', N'AkshAdmin', N'789')
SET IDENTITY_INSERT [dbo].[Admin] OFF
SET IDENTITY_INSERT [dbo].[city] ON 

INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (1, 4, N'Ahmedabad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (2, 4, N'Amreli')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (3, 4, N'Anand')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (4, 4, N'Ankleshwar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (5, 4, N'Bharuch')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (6, 4, N'Bhavnagar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (7, 4, N'Bhuj')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (8, 4, N'Cambay')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (9, 4, N'Dahod')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (10, 4, N'Deesa')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (11, 4, N'Dholka')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (12, 4, N'Gandhinagar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (13, 4, N'Godhra')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (14, 4, N'Himatnagar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (15, 4, N'Idar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (16, 4, N'Jamnagar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (17, 4, N'Junagadh')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (18, 4, N'Kadi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (19, 4, N'Kalavad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (20, 4, N'Kalol')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (21, 4, N'Kapadvanj')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (22, 4, N'Karjan')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (23, 4, N'Keshod')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (24, 4, N'Khambhalia')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (25, 4, N'Khambhat')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (26, 4, N'Kheda')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (27, 4, N'Khedbrahma')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (28, 4, N'Kheralu')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (29, 4, N'Kodinar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (30, 4, N'Lathi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (31, 4, N'Limbdi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (32, 4, N'Lunawada')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (33, 4, N'Mahesana')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (34, 4, N'Mahuva')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (35, 4, N'Manavadar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (36, 4, N'Mandvi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (37, 4, N'Mangrol')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (38, 4, N'Mansa')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (39, 4, N'Mehmedabad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (40, 4, N'Modasa')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (41, 4, N'Morvi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (42, 4, N'Nadiad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (43, 4, N'Navsari')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (44, 4, N'Padra')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (45, 4, N'Palanpur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (46, 4, N'Palitana')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (47, 4, N'Pardi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (48, 4, N'Patan')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (49, 4, N'Petlad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (50, 4, N'Porbandar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (51, 4, N'Radhanpur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (52, 4, N'Rajkot')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (53, 4, N'Rajpipla')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (54, 4, N'Rajula')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (55, 4, N'Ranavav')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (56, 4, N'Rapar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (57, 4, N'Salaya')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (58, 4, N'Sanand')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (59, 4, N'Savarkundla')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (60, 4, N'Sidhpur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (61, 4, N'Sihor')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (62, 4, N'Songadh')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (63, 4, N'Surat')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (64, 4, N'Talaja')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (65, 4, N'Thangadh')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (66, 4, N'Tharad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (67, 4, N'Umbergaon')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (68, 4, N'Umreth')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (69, 4, N'Una')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (70, 4, N'Unjha')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (71, 4, N'Upleta')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (72, 4, N'Vadnagar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (73, 4, N'Vadodara')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (74, 4, N'Valsad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (75, 4, N'Vapi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (76, 4, N'Vapi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (77, 4, N'Veraval')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (78, 4, N'Vijapur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (79, 4, N'Viramgam')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (80, 4, N'Visnagar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (81, 4, N'Vyara')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (82, 4, N'Wadhwan')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (83, 4, N'Wankaner')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (84, 4, N'Adalaj')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (85, 4, N'Adityana')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (86, 4, N'Alang')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (87, 4, N'Ambaji')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (88, 4, N'Ambaliyasan')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (89, 4, N'Andada')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (90, 4, N'Anjar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (91, 4, N'Anklav')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (92, 4, N'Antaliya')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (93, 4, N'Arambhada')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (94, 4, N'Atul')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (95, 12, N'Jalna')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (96, 12, N'Kalyan')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (97, 12, N'Kolhapur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (98, 12, N'Latur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (99, 12, N'Loha')
GO
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (100, 12, N'Lonar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (101, 12, N'Lonavla')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (102, 12, N'Mahad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (103, 12, N'Mahuli')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (104, 12, N'Malegaon')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (105, 12, N'Malkapur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (106, 12, N'Manchar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (107, 12, N'Mangalvedhe')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (108, 12, N'Mangrulpir')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (109, 12, N'Manjlegaon')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (110, 12, N'Manmad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (111, 12, N'Manwath')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (112, 12, N'Mehkar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (113, 12, N'Mhaswad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (114, 12, N'Miraj')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (115, 12, N'Morshi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (116, 12, N'Mukhed')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (117, 12, N'Mul')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (118, 12, N'Mumbai')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (119, 12, N'Murtijapur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (120, 12, N'Nagpur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (121, 12, N'Nalasopara')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (122, 12, N'Nanded-Waghala')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (123, 12, N'Nandgaon')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (124, 12, N'Nandura')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (125, 12, N'Nandurbar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (126, 12, N'Narkhed')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (127, 12, N'Nashik')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (128, 12, N'Navi Mumbai')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (129, 12, N'Nawapur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (130, 12, N'Nilanga')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (131, 12, N'Osmanabad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (132, 12, N'Ozar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (133, 12, N'Pachora')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (134, 12, N'Paithan')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (135, 12, N'Palghar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (136, 12, N'Pandharkaoda')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (137, 12, N'Pandharpur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (138, 12, N'Panvel')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (139, 12, N'Parbhani')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (140, 12, N'Parli')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (141, 12, N'Parola')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (142, 12, N'Partur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (143, 12, N'Pathardi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (144, 12, N'Pathri')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (145, 12, N'Patur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (146, 12, N'Pauni')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (147, 12, N'Pen')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (148, 12, N'Phaltan')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (149, 12, N'Pulgaon')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (150, 12, N'Pune')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (151, 12, N'Purna')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (152, 12, N'Pusad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (153, 12, N'Rahuri')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (154, 12, N'Rajura')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (155, 12, N'Ramtek')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (156, 12, N'Ratnagiri')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (157, 12, N'Raver')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (158, 12, N'Risod')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (159, 12, N'Sailu')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (160, 12, N'Sangamner')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (161, 12, N'Sangli')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (162, 12, N'Sangole')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (163, 12, N'Sasvad')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (164, 12, N'Satana')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (165, 12, N'Satara')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (166, 12, N'Savner')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (167, 12, N'Sawantwadi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (168, 12, N'Shahade')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (169, 12, N'Shegaon')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (170, 12, N'Shendurjana')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (171, 12, N'Shirdi')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (172, 12, N'Shirpur-Warwade')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (173, 12, N'Shirur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (174, 12, N'Shrigonda')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (175, 12, N'Shrirampur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (176, 12, N'Sillod')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (177, 12, N'Sinnar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (178, 12, N'Solapur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (179, 12, N'Soyagaon')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (180, 12, N'Talegaon Dabhade')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (181, 12, N'Talode')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (182, 12, N'Tasgaon')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (183, 12, N'Tirora')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (184, 12, N'Tuljapur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (185, 12, N'Tumsar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (186, 12, N'Uran')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (187, 12, N'Uran Islampur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (188, 12, N'Wadgaon Road')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (189, 12, N'Wai')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (190, 12, N'Wani')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (191, 12, N'Wardha')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (192, 12, N'Warora')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (193, 12, N'Warud')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (194, 12, N'Washim')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (195, 12, N'Yevla')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (196, 12, N'Uchgaon')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (197, 12, N'Udgir')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (198, 12, N'Umarga')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (199, 12, N'Umarkhed')
GO
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (200, 12, N'Umred')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (201, 12, N'Vadgaon Kasba')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (202, 12, N'Vaijapur')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (203, 12, N'Vasai')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (204, 12, N'Virar')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (205, 12, N'Vita')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (206, 12, N'Yavatmal')
INSERT [dbo].[city] ([city_Id], [state_Id], [city]) VALUES (207, 12, N'Yawal')
SET IDENTITY_INSERT [dbo].[city] OFF
SET IDENTITY_INSERT [dbo].[complex] ON 

INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (54, N'RajKishore Soc.', N'Kandivali', N'D/4 Rajkishore building,nr.Kandivali village,m.g road,kandivali west', N'Mumbai              ', N'maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (55, N'Jeevan Anand', N'Borivali', N'A/5-Sai baba nagar,jeevan Anand hos sco.,S.v Road,Borivali west', N'mumba               ', N'maharashtra              ', 400066, CAST(19.2371877000 AS Numeric(18, 10)), CAST(72.8441358000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (56, N'Hiranandani Complex', N'Santacruz', N'Flat no 304,  Bld no. 3', N'Mumbai              ', N'Maharashtra              ', 400047, CAST(36.9741171000 AS Numeric(18, 10)), CAST(-122.0307963000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (57, N'Romell Aether', N'Goregaon', N'A/2 Romell Aether, Mumbai Goregaon east', N'Mumbai              ', N'Maharashtra              ', 400063, CAST(19.1551485000 AS Numeric(18, 10)), CAST(72.8678551000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (58, N'Raj Florenza', N'Mira road', N'Bld 7/1204, Raj Florenza', N'Mumbai              ', N'Maharashtra              ', 401107, CAST(19.2871393000 AS Numeric(18, 10)), CAST(72.8688419000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (59, N'Royal Oasis', N'Malad', N'1B, 704, Royal Oasis, Malad West', N'Mumbai              ', N'Maharashtra              ', 400097, CAST(42.1915872000 AS Numeric(18, 10)), CAST(-112.2507986000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (60, N'Acme Boulevard', N'Andheri', N'A4/1601,Acme Boulebard,
Andheri west,', N'Mumbai              ', N'Maharahtra               ', 400069, CAST(19.1136450000 AS Numeric(18, 10)), CAST(72.8697339000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (61, N'Sky Villa', N'Dadar', N'506/Bld 4, Sky Villa, Mumbai, Dadar west', N'Mumbai              ', N'Maharashtra              ', 400014, CAST(19.0213240000 AS Numeric(18, 10)), CAST(72.8424178000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (62, N'Raheja Classique', N'Vile Parle', N'A15/501, Raheja Classique, Mumbai, Vile Parle East', N'Mumbai              ', N'maharashtra              ', 400056, CAST(19.1030524000 AS Numeric(18, 10)), CAST(72.8466914000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (63, N'Humlog Society', N'Nalasopara', N'building no 4,flat 505', N'Mumbai              ', N'Maharashtra              ', 401209, CAST(19.4178206000 AS Numeric(18, 10)), CAST(72.8108543000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (64, N'arundhati bldng', N'Kandivali', N'3/A arundhati building ,Kandivali west', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (65, N'arundhati bldng', N'Kandivali', N'3/A arundhati building ,Kandivali west', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (66, N'arundhati bldng', N'Kandivali', N'3/A arundhati building ,Kandivali west', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (67, N'Arundhati building', N'Kandivali', N'3/a-Arundhti Building ,Kandivali west', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (68, N'Arundhati building', N'Kandivali', N'3/a-Arundhti Building ,Kandivali west', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (69, N'Arundhati building', N'Kandivali', N'3/a-Arundhti Building ,Kandivali west', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (70, N'Arundhati building', N'Kandivali', N'3/a-Arundhti Building ,Kandivali west', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (71, N'Arundhati building', N'Kandivali', N'3/a-Arundhti Building ,Kandivali west', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (72, N'Arundhati building', N'Kandivali', N'3/A-Arundhati building ,Nr.Kandivli Station,Kandivali w', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (73, N'Jeevan Vaibhav', N'Kandivali', N'404/C-Jeevan Vaibhav', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (77, N'Jeevan Vaibhav', N'Kandivali', N'404/C-Jeevan Vaibhav', N'Mumbai              ', N'Maharashtra              ', 400067, CAST(19.1998211000 AS Numeric(18, 10)), CAST(72.8425940000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (87, N'shriji vijay', N'aaaa', N'asasasSas', N'mumbai              ', N'maharashtra              ', 4000101, CAST(40.6132750000 AS Numeric(18, 10)), CAST(-74.1222710000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (88, N'New Complex', N'maharashtra', N'Appapada,', N'MUMBAI              ', N'MAHARASHTRA              ', 400097, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0.0000000000 AS Numeric(18, 10)))
INSERT [dbo].[complex] ([Comp_id], [comp_name], [location], [address1], [city], [state], [pincode], [Latitude], [Longitude]) VALUES (89, N'New Complex', N'maharashtra', N'Appapada,', N'MUMBAI              ', N'MAHARASHTRA              ', 400097, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0.0000000000 AS Numeric(18, 10)))
SET IDENTITY_INSERT [dbo].[complex] OFF
SET IDENTITY_INSERT [dbo].[country] ON 

INSERT [dbo].[country] ([country_Id], [country]) VALUES (1, N'Afghanistan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (2, N'Albania')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (3, N'Algeria')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (4, N'American Samoa')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (5, N'Andorra')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (6, N'Angola')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (7, N'Anguilla')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (8, N'Antarctica')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (9, N'Antigua and Barbuda')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (10, N'Argentina')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (11, N'Armenia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (12, N'Aruba')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (13, N'Australia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (14, N'Austria')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (15, N'Azerbaijan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (16, N'Bahamas')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (17, N'Bahrain')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (18, N'Bangladesh')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (19, N'Barbados')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (20, N'Belarus')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (21, N'Belgium')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (22, N'Belize')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (23, N'Benin')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (24, N'Bermuda')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (25, N'Bhutan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (26, N'Bolivia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (27, N'Bosnia and Herzegovina')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (28, N'Botswana')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (29, N'Bouvet Island')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (30, N'Brazil')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (31, N'British Indian Ocean Territory')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (32, N'Brunei Darussalam')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (33, N'Bulgaria')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (34, N'Burkina Faso')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (35, N'Burundi')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (36, N'Cambodia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (37, N'Cameroon')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (38, N'Canada')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (39, N'Cape Verde')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (40, N'Cayman Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (41, N'Central African Republic')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (42, N'Chad')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (43, N'Chile')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (44, N'China')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (45, N'Christmas Island')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (46, N'Cocos (Keeling) Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (47, N'Colombia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (48, N'Comoros')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (49, N'Congo')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (50, N'Cook Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (51, N'Costa Rica')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (52, N'Croatia (Hrvatska)')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (53, N'Cuba')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (54, N'Cyprus')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (55, N'Czech Republic')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (56, N'Denmark')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (57, N'Djibouti')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (58, N'Dominica')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (59, N'Dominican Republic')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (60, N'East Timor')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (61, N'Ecuador')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (62, N'Egypt')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (63, N'El Salvador')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (64, N'Equatorial Guinea')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (65, N'Eritrea')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (66, N'Estonia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (67, N'Ethiopia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (68, N'Falkland Islands (Malvinas)')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (69, N'Faroe Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (70, N'Fiji')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (71, N'Finland')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (72, N'France')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (73, N'France, Metropolitan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (74, N'French Guiana')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (75, N'French Polynesia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (76, N'French Southern Territories')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (77, N'Gabon')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (78, N'Gambia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (79, N'Georgia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (80, N'Germany')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (81, N'Ghana')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (82, N'Gibraltar')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (83, N'Guernsey')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (84, N'Greece')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (85, N'Greenland')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (86, N'Grenada')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (87, N'Guadeloupe')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (88, N'Guam')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (89, N'Guatemala')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (90, N'Guinea')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (91, N'Guinea-Bissau')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (92, N'Guyana')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (93, N'Haiti')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (94, N'Heard and Mc Donald Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (95, N'Honduras')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (96, N'Hong Kong')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (97, N'Hungary')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (98, N'Iceland')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (99, N'India')
GO
INSERT [dbo].[country] ([country_Id], [country]) VALUES (100, N'Isle of Man')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (101, N'Indonesia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (102, N'Iran (Islamic Republic of)')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (103, N'Iraq')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (104, N'Ireland')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (105, N'Israel')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (106, N'Italy')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (107, N'Ivory Coast')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (108, N'Jersey')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (109, N'Jamaica')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (110, N'Japan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (111, N'Jordan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (112, N'Kazakhstan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (113, N'Kenya')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (114, N'Kiribati')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (115, N'Korea, Democratic People''s Republic of')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (116, N'Korea, Republic of')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (117, N'Kosovo')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (118, N'Kuwait')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (119, N'Kyrgyzstan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (120, N'Lao People''s Democratic Republic')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (121, N'Latvia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (122, N'Lebanon')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (123, N'Lesotho')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (124, N'Liberia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (125, N'Libyan Arab Jamahiriya')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (126, N'Liechtenstein')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (127, N'Lithuania')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (128, N'Luxembourg')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (129, N'Macau')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (130, N'Macedonia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (131, N'Madagascar')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (132, N'Malawi')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (133, N'Malaysia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (134, N'Maldives')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (135, N'Mali')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (136, N'Malta')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (137, N'Marshall Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (138, N'Martinique')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (139, N'Mauritania')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (140, N'Mauritius')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (141, N'Mayotte')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (142, N'Mexico')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (143, N'Micronesia, Federated States of')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (144, N'Moldova, Republic of')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (145, N'Monaco')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (146, N'Mongolia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (147, N'Montenegro')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (148, N'Montserrat')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (149, N'Morocco')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (150, N'Mozambique')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (151, N'Myanmar')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (152, N'Namibia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (153, N'Nauru')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (154, N'Nepal')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (155, N'Netherlands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (156, N'Netherlands Antilles')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (157, N'New Caledonia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (158, N'New Zealand')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (159, N'Nicaragua')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (160, N'Niger')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (161, N'Nigeria')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (162, N'Niue')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (163, N'Norfolk Island')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (164, N'Northern Mariana Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (165, N'Norway')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (166, N'Oman')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (167, N'Pakistan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (168, N'Palau')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (169, N'Palestine')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (170, N'Panama')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (171, N'Papua New Guinea')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (172, N'Paraguay')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (173, N'Peru')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (174, N'Philippines')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (175, N'Pitcairn')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (176, N'Poland')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (177, N'Portugal')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (178, N'Puerto Rico')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (179, N'Qatar')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (180, N'Reunion')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (181, N'Romania')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (182, N'Russian Federation')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (183, N'Rwanda')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (184, N'Saint Kitts and Nevis')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (185, N'Saint Lucia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (186, N'Saint Vincent and the Grenadines')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (187, N'Samoa')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (188, N'San Marino')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (189, N'Sao Tome and Principe')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (190, N'Saudi Arabia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (191, N'Senegal')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (192, N'Serbia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (193, N'Seychelles')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (194, N'Sierra Leone')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (195, N'Singapore')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (196, N'Slovakia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (197, N'Slovenia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (198, N'Solomon Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (199, N'Somalia')
GO
INSERT [dbo].[country] ([country_Id], [country]) VALUES (200, N'South Africa')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (201, N'South Georgia South Sandwich Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (202, N'Spain')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (203, N'Sri Lanka')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (204, N'St. Helena')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (205, N'St. Pierre and Miquelon')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (206, N'Sudan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (207, N'Suriname')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (208, N'Svalbard and Jan Mayen Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (209, N'Swaziland')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (210, N'Sweden')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (211, N'Switzerland')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (212, N'Syrian Arab Republic')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (213, N'Taiwan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (214, N'Tajikistan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (215, N'Tanzania, United Republic of')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (216, N'Thailand')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (217, N'Togo')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (218, N'Tokelau')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (219, N'Tonga')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (220, N'Trinidad and Tobago')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (221, N'Tunisia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (222, N'Turkey')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (223, N'Turkmenistan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (224, N'Turks and Caicos Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (225, N'Tuvalu')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (226, N'Uganda')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (227, N'Ukraine')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (228, N'United Arab Emirates')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (229, N'United Kingdom')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (230, N'United States')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (231, N'United States minor outlying islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (232, N'Uruguay')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (233, N'Uzbekistan')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (234, N'Vanuatu')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (235, N'Vatican City State')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (236, N'Venezuela')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (237, N'Vietnam')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (238, N'Virgin Islands (British)')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (239, N'Virgin Islands (U.S.)')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (240, N'Wallis and Futuna Islands')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (241, N'Western Sahara')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (242, N'Yemen')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (243, N'Zaire')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (244, N'Zambia')
INSERT [dbo].[country] ([country_Id], [country]) VALUES (245, N'Zimbabwe')
SET IDENTITY_INSERT [dbo].[country] OFF
SET IDENTITY_INSERT [dbo].[customer] ON 

INSERT [dbo].[customer] ([cust_id], [name], [email], [contact], [password], [SecurityQuestion], [Answer], [cust_type]) VALUES (4, N'nehal', N'n@gmail.com', N'7506048815', N'123', NULL, NULL, NULL)
INSERT [dbo].[customer] ([cust_id], [name], [email], [contact], [password], [SecurityQuestion], [Answer], [cust_type]) VALUES (5, N'Sahil', N's@gmail.com', N'9820365139', N'123456', NULL, NULL, NULL)
INSERT [dbo].[customer] ([cust_id], [name], [email], [contact], [password], [SecurityQuestion], [Answer], [cust_type]) VALUES (6, N'Nishit', N'nishitRV@gmail.com', N'9820365139', N'11111', N'Who is your favourite teacher?', N'shanu', NULL)
INSERT [dbo].[customer] ([cust_id], [name], [email], [contact], [password], [SecurityQuestion], [Answer], [cust_type]) VALUES (9, N'Uttam', N'uttamwork99@gmail.com', N'9970650225', N'uttu', N'Who is your favourite subject?', N'Harshada', NULL)
INSERT [dbo].[customer] ([cust_id], [name], [email], [contact], [password], [SecurityQuestion], [Answer], [cust_type]) VALUES (10, N'nehal', N'nehaljethwa6@gmail.com', N'7506048815', N'nehal06', N'What is your pet name?', N'sahil', NULL)
INSERT [dbo].[customer] ([cust_id], [name], [email], [contact], [password], [SecurityQuestion], [Answer], [cust_type]) VALUES (11, N'sweta', N'sweta@gmail.com', N'8149642479', N'sweta123', N'Who is your favourite subject?', N'SE', NULL)
INSERT [dbo].[customer] ([cust_id], [name], [email], [contact], [password], [SecurityQuestion], [Answer], [cust_type]) VALUES (12, N'Prasad Bankar', N'prasad@gmail.com', N'9865646624', N'123123', N'What is your pet name?', N'Jon Snow', NULL)
SET IDENTITY_INSERT [dbo].[customer] OFF
SET IDENTITY_INSERT [dbo].[management_team] ON 

INSERT [dbo].[management_team] ([mng_team_id], [name], [about_us_id], [position]) VALUES (23, N'', 1, N'NA')
SET IDENTITY_INSERT [dbo].[management_team] OFF
SET IDENTITY_INSERT [dbo].[Property] ON 

INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (18, 54, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 5, N'Y', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (19, 55, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 5, N'Y', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (20, 56, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 9, N'Y', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (21, 57, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 9, N'N', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (22, 58, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 9, N'Y', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (23, 59, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 9, N'N', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (24, 60, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 10, N'Y', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (25, 61, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 10, N'N', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (26, 62, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 12, N'N', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (27, 63, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 12, N'Y', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (35, 72, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 5, N'N', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (39, 77, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 0, N'Y', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (42, 87, N'Ground floor only', 400, N'1RK', N'1         ', N'Residencial', N'Ready Possession', N'Rent', 100000, 4, N'N', N' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Project')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (47, 88, N'below 10 floor', 345, N'1RK', N'1         ', N'Commercial', N'Pre-Launch', N'Rent', 1500000, 0, N'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Property')
INSERT [dbo].[Property] ([Prop_id], [Comp_id], [Floor1], [Area], [Bhk], [Bathroom], [types1], [prop_status], [prop_for], [price], [owner_id], [isApproved], [images], [parking], [lift], [water], [fire], [gym], [power1], [child], [prop]) VALUES (48, 89, N'below 10 floor', 600, N'1RK', N'1         ', N'Commercial', N'Pre-Launch', N'Rent', 8000000, 0, N'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Property')
SET IDENTITY_INSERT [dbo].[Property] OFF
SET IDENTITY_INSERT [dbo].[property_imgages] ON 

INSERT [dbo].[property_imgages] ([id], [img_name], [img_base_64], [property_id]) VALUES (10, N'img333', N'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAAz1BMVEX///+qqqrf39+WoaFCY2N1d3fExMTHx8fg4OAAAACtra3Y2NiGjIzw8PCDiYmnuLh3hYW9zs7f//+t19eCkpKQsLC8/v6g/v58/PxUrKxSbGzV3d1bgYGB/f128PBu4eE8X1+Ul5fIyMjv7++uu7thxcVr29tYtLRwcnK45vrb///Y/f/N9f3G8Pyx4fmp3Pib0vYAAP93AIj/iIiIAHf/d3f/Pz//eHj/h4fHx/+cnP/t7f9TU/9GRv/S0v8DA/9gYP//QEDIUoioMojQSHY7HjgZAAACg0lEQVRo3u2ZeW/UMBDFJykUHrDLUe6bcrTceLulQFvu7/+Z+COJd+zEsR2PLVTlaZUeWvmnsWeeZhyiZFVE9da589sXLjZ/g8B+gkRUEVF9STMI1K0uDKH6chcHmieapxCjgeTWGYNcWeSHLK9eq3NDltdv7CzrTXZtcksqvyq6eev2nbv37tcGBLxoBCAPHj56/OTps93nbZ1DV70gZPHi5au9/e3dhQnZbFfl1iTvQvsBaSLRa6ciIMy7wD5tQFIQ5l362WQXEYHeOBUFGVcRyFunBCHvnIqCjHvXe6diIB7vEsku7l1tfehCBEAfnAqHGN5lFwuI6KNT4RDTuwYgn5wKhxjehaZ9AOsmRCCWd5kGJhWJ5V15tqvnXW126d+SKjnqe/khKBIJBCDj3uXuvCulpLxrBLJaHdjye1fXysNe2AFZrw8tfT4K864ISH+7vhyFeRcvRA+k/y8HhHtX51gGBAIQc2bszNGMJDyFXRBjZjRauwnF6ITwmVGHgIChMQoi6F2pEOSEuGeVKIjfu/rlGQsJ8q5ECPeujXlpCzOGiakQ7l3MV5qiZNWJFMiwd5E9QKZFYs6MQxCkQ3p9F4eMW/FE77LOxOOS07zLzq7ytjJDvn5Luu8KgRyfKJV03xUCOVVK2fdd+rILNGwlkZDvSv342e+7QCNG4of8MhvK36vVn4PBmTEBQlZH+Xe9xuGAd/GNiodQb7uAwZkR/IDSIHSqmvXMmdE4k3TI8Um7oNV3ddnVLZV4PSj4ZmEckp3Sbn5Vpd+Y+iCZYwEVoIAKUPwjiBwEgu+vnHWS/eBRAIL8oaDEfsm+8PGeDJ0NSqFQCjBmzZo1a9asWf+X/gFdg4AlwMIrZQAAAABJRU5ErkJggg==', 18)
INSERT [dbo].[property_imgages] ([id], [img_name], [img_base_64], [property_id]) VALUES (11, N'img44', N'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAADs0lEQVQ4y7XUb2hWVRzA8e+59z73Xh+fZ88cstzWln+mom1ojtEsVOaLLCzfFOgIIZCUGIkFtReBsz+vgmhEgWQvkl5YCmpWUCaRL1RSMwg0pS2nxtzcsz1/3XPvufec04tHB5JlQf3gnDc/+HD4/X7nB/9xiHvknQFobmhJNkaVqWBwnJFdcP1fg+9Cc9tjy3fMaml80kvNWGRZBgxEkY6nCuXT2YtD+zacubwbiO8JHmxv7pm7sn13Om3XiOIoOj+OkiEgMF4SUddA5NWRG8mfvXjgeM82GPxL8HDn/N55K1rfT05dI7w6hDGAbWOMQBmDUgYlJcL1cea2UawkJoaOnV69pRBd+BO4f2lL18LO5lMz8oNEuRzC9bDRRFqgDCgNaI1WBqlAhSFO0wIqovbST1+eeegVqABYt8Gm+TUDfm6IuFjA8nyMDBieiAgrAcaAUDHZfMBYPgSlUZbHzSu/YU1lFy/pWrD9tmMBfLr4vhVuOPGwLhcRTgItA0br2rn/naNk564izJYZuxGQefE96vv2MBE4GKXRIkFp7DoyvLn1EDhw60qKqbW6bKgIwFJUCpIHtm1lYfcampcd5Oiz62hYu56u53sB+ObQh5R+Pk3oeASxQgSF+anWzCIGCxccANfW84QwKANxBLgeowOvMmtOA7O7N/DU4RP4noORAWdfe4HJH39AeS461igtAI1ELgAuWABaK1dpUAa0AWXZyGKRE9s3Ucnn8D0HgF/2f8K5gY8xvovW1UZVG2YwWnvTNdTYE7GG6jHoSDFegllb+nFraqfHak73evzuRyhlZRW6hQkMtmXdmAalss7FWqCURsWaXCBo2fkBq3f0YVuCUy8/x/CRfdQ1NfL0ga9JrVlHeDOuvgxwbLeSjuPz02ChbB2Vwi/GWiBjjbATPLhmFQAnX9rM+T17+b53M1ePfUUqk6alfSky1BitsRMunpv49onfo4k7Bvvz5XNe932zMyzliMIYt76B9NIOLh85gpVykYHESWdoeHQtg18cwnYtRMIjlc7gFwudz1ypnL0DfBOcZR2NJy2XzqnJLLIiiUKwZrrV4gOxjJBlQyIJTirNzJpanFLpjY2/5vvv+pc/gvrZHU2HSforK8UCYblELONq97UBIbA9HzdTi5/wYHL87U2Dpb6/3TZvQWJJW/0uUVPTG1tWJlYKFccIYWHZNpYQiCC4xPhY/8Zr8rN/tA8B9kL9jNb048ZPduDYjWgjieSwKeSPmxH1Xc9dduH/En8AqKfFnXKlmtwAAAAASUVORK5CYII=', 18)
INSERT [dbo].[property_imgages] ([id], [img_name], [img_base_64], [property_id]) VALUES (17, N'111', N'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAABn0lEQVR42tWUyytEcRTH54/CKBulbMjWRll5ZIEskAVmmIgNOxbMJI9QkzwbNaUsJuOVRx6NwpCFuHPfj7l3+jq/nxnSpHlk41efTt1zz+ee0/39fi7Xv1kjo0PTo2M+FAOrzRKyRLGL1f4qLGma/4EgvHESCYF4z8rnLSxrWfiWEaIofMFyBQlZQVXnChqHN9OdCZAkkVPXG0Rl+zLKWxfyE5Y2z/PRGrwbGPLvY277lHclySKm147QN7WH+oF1KIoEN304p1AUExxJSiCweYpQ5AYHFw84vo4jfHgHPz1TVRmKqvCYU8hEbDSZjUhdRc7vIVNUFRknJNVIxNFIqKm5hezFT1ToxNH1E0UNhqHj9vEVpmnAtExYlgHLNHMLdV0jVBi6jomVKHYiMVzEXhCLvyJ69YzF3UuSmUgmLYpWfj+FdVPbE0TLeAjjiwe8K1Y8u3WGjskwarqDXJj/tiFpRdsSqrtW0+OxjpKwCbad3IXuwwyZsSyKtm3DSVPUSclg20lOynHgOKnCj96fXQ5e3+BMsdeXx9Mf+D8X9QeyEjOnWfOQgwAAAABJRU5ErkJggg==', 48)
INSERT [dbo].[property_imgages] ([id], [img_name], [img_base_64], [property_id]) VALUES (18, N'11', N'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAC3klEQVR42tXU20sUYRgGcK/qX/AmKAiCMLNiwS7K0jAVg6y8CtR2c1E6QEGJiWyGipqWZQhGdRVBCamruSdN1/VQmabrqOV5dV0Pq6t7mPl2dnZ7+ma29Mbdrh14eGdufjzfO8NEROyq68yD9qyUsm5DanmPg4ajIVIqxNlLUsu6SVKJkYm92/nscPqHPf8FE4s6TcsO95rdyfrtLg5253ZWN8WwGLVuoqBxDvLqcfZg8vO9YcGkYuPqhpv4e2a86Jvl0TfHo3eGh2maoOMXh1bGBY4PQD3mQcHHWVx7Os6FRZNLTRwF0W/x4buFp5PHVwkl6JzgoGXcFBTQb/Whb8GLRy1ziLtveBe6YamJiOAABQfneQzM+yT8y6wXXZME+nEPppdc0tGdLI+ldTfiCzqEsKDDRTBMG5hphhcF/JBQsaVXaqmjLZvNTjQNbcBB95ygMpKQ4PkSE1mn4OiiHwzFRmjMVgFDFP9Gd9ozHUQNPz3Q0T2uu7xIeGgKDSZS0E7BMZtfyqhNhP0YXgg2lVDa1DgZ3KmdgvGqMOC5IhNZdRIwW5iw1VRExb1+swSbdtMsb3oRVxgGjFd1kRX6vTGL25gEWoWtlq/19bhTm4HrlReQWZaEs3l5oV9KXGEXsW1wGFkIHjMYnxQRq2t9C9X7TKjNNRi0alGly0F6VRRO5ERW7gieyjcSq4PFZTWHK2oWl5pZpNH7tEYOF5tYXC1NQsPwEzQw1QgEAqjQK1Clz4YsJ3LnY5+895lY7Cxudgp4MxbAq9EA6pgAaul8QWdKfgxazC8l7F8ah2ogy43EjqDstoGdWnb9VrYLyDb4oGjjIW/zScmiz7G3DqBcm4USbYaElWgywjeMUepmbGse19SKJ0CDqWUPJv5mxelFZX0x0ioO4bFWLjUTp/gccocxSs2NaLmm7ahCux6t0JCtyDXkCE2U/BM5pswUZLn7pWPKcvb5jytPt+6un/QfzEnCevoUvTYAAAAASUVORK5CYII=', 48)
SET IDENTITY_INSERT [dbo].[property_imgages] OFF
SET IDENTITY_INSERT [dbo].[property_inquiry] ON 

INSERT [dbo].[property_inquiry] ([inq_id], [name], [email], [contact], [prop_for], [type], [bedrooms], [bathrooms], [city], [location], [budget_min], [budget_max], [parking_type]) VALUES (1, N'nehal', N'nehal@gmail.com', N'9820365139', N'Rent/Lease', N'Residencial', N'1RK       ', N'1         ', N'mumbai', N'Kandivali', N'10000', N'15000', N'Common    ')
INSERT [dbo].[property_inquiry] ([inq_id], [name], [email], [contact], [prop_for], [type], [bedrooms], [bathrooms], [city], [location], [budget_min], [budget_max], [parking_type]) VALUES (2, N'', N'', N'          ', N'Buy', N'Commercial', N'1RK       ', N'1         ', N'', N'', N'', N'', N'None      ')
INSERT [dbo].[property_inquiry] ([inq_id], [name], [email], [contact], [prop_for], [type], [bedrooms], [bathrooms], [city], [location], [budget_min], [budget_max], [parking_type]) VALUES (3, N'nehal', N'nehaljetthwa@gmail.com', N'7506048815', N'Buy', N'Commercial', N'1BHK      ', N'2         ', N'Mumbai', N'kandivali', N'2000', N'400000', N'Common    ')
INSERT [dbo].[property_inquiry] ([inq_id], [name], [email], [contact], [prop_for], [type], [bedrooms], [bathrooms], [city], [location], [budget_min], [budget_max], [parking_type]) VALUES (4, N'Nikita', N'gorulenikita1997@gmail.com', N'7208946411', N'Buy', N'Residencial', N'1RK       ', N'1         ', N'Mumbai', N'Kandivali', N'400000', N'700000', N'Common    ')
INSERT [dbo].[property_inquiry] ([inq_id], [name], [email], [contact], [prop_for], [type], [bedrooms], [bathrooms], [city], [location], [budget_min], [budget_max], [parking_type]) VALUES (5, N'Nehal Jethwa', N'nehaljethwa6@gmail.com', N'7506048815', N'Buy', N'Commercial', N'1RK       ', N'1         ', N'Mumbai', N'Kandivali', N'20000', N'10000000', N'Common    ')
SET IDENTITY_INSERT [dbo].[property_inquiry] OFF
SET IDENTITY_INSERT [dbo].[PurchaseRequest] ON 

INSERT [dbo].[PurchaseRequest] ([P_id], [cust_id], [Prop_id], [PurchaseReqDate], [Status1]) VALUES (45, 5, 19, CAST(N'2017-03-27' AS Date), N'Sold')
INSERT [dbo].[PurchaseRequest] ([P_id], [cust_id], [Prop_id], [PurchaseReqDate], [Status1]) VALUES (46, 0, 20, CAST(N'2017-03-29' AS Date), N'Sold')
INSERT [dbo].[PurchaseRequest] ([P_id], [cust_id], [Prop_id], [PurchaseReqDate], [Status1]) VALUES (47, 5, 39, CAST(N'2017-03-31' AS Date), N'Progress')
INSERT [dbo].[PurchaseRequest] ([P_id], [cust_id], [Prop_id], [PurchaseReqDate], [Status1]) VALUES (48, 5, 22, CAST(N'2017-03-31' AS Date), N'Progress')
INSERT [dbo].[PurchaseRequest] ([P_id], [cust_id], [Prop_id], [PurchaseReqDate], [Status1]) VALUES (49, 5, 27, CAST(N'2017-03-31' AS Date), N'Progress')
INSERT [dbo].[PurchaseRequest] ([P_id], [cust_id], [Prop_id], [PurchaseReqDate], [Status1]) VALUES (50, 5, 18, CAST(N'2017-03-31' AS Date), N'Progress')
INSERT [dbo].[PurchaseRequest] ([P_id], [cust_id], [Prop_id], [PurchaseReqDate], [Status1]) VALUES (51, 4, 27, CAST(N'2018-01-21' AS Date), N'Progress')
SET IDENTITY_INSERT [dbo].[PurchaseRequest] OFF
SET IDENTITY_INSERT [dbo].[state] ON 

INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (1, 99, N'ANDHRA PRADESH')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (2, 99, N'ASSAM')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (3, 99, N'ARUNACHAL PRADESH')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (4, 99, N'GUJRAT')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (5, 99, N'BIHAR')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (6, 99, N'HARYANA')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (7, 99, N'HIMACHAL PRADESH')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (8, 99, N'JAMMU & KASHMIR')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (9, 99, N'KARNATAKA')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (10, 99, N'KERALA')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (11, 99, N'MADHYA PRADESH')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (12, 99, N'MAHARASHTRA')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (13, 99, N'MANIPUR')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (14, 99, N'MEGHALAYA')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (15, 99, N'MIZORAM')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (16, 99, N'NAGALAND')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (17, 99, N'ORISSA')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (18, 99, N'PUNJAB')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (19, 99, N'RAJASTHAN')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (20, 99, N'SIKKIM')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (21, 99, N'TAMIL NADU')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (22, 99, N'TRIPURA')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (23, 99, N'UTTAR PRADESH')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (24, 99, N'WEST BENGAL')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (25, 99, N'DELHI')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (26, 99, N'GOA')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (27, 99, N'PONDICHERY')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (28, 99, N'LAKSHDWEEP')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (29, 99, N'DAMAN & DIU')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (30, 99, N'DADRA & NAGAR')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (31, 99, N'CHANDIGARH')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (32, 99, N'ANDAMAN & NICOBAR')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (33, 99, N'UTTARANCHAL')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (34, 99, N'JHARKHAND')
INSERT [dbo].[state] ([state_Id], [country_Id], [state]) VALUES (35, 99, N'CHATTISGARH')
SET IDENTITY_INSERT [dbo].[state] OFF
ALTER TABLE [dbo].[contact_details]  WITH CHECK ADD FOREIGN KEY([about_us_id])
REFERENCES [dbo].[about_us] ([about_us_id])
GO
ALTER TABLE [dbo].[management_team]  WITH CHECK ADD FOREIGN KEY([about_us_id])
REFERENCES [dbo].[about_us] ([about_us_id])
GO
ALTER TABLE [dbo].[property_imgages]  WITH CHECK ADD FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Prop_id])
GO
USE [master]
GO
ALTER DATABASE [Propshop] SET  READ_WRITE 
GO
