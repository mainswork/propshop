﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddServices.aspx.cs" Inherits="AddServices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
<div class="row">
    <div class="col s12 m12 l12">
       
        <div class="container">
              <div class="col s12 m12 l12 z-depth-3">
                 <h5 class="center">Add Services Information
        </h5>
                        <div class="row">
                            <div class="col s12 m12 l10 offset-l1">
                                <asp:Label CssClass="control-label" For="txtFinance" ID="lbltextFInance" runat="server" Text="Finance Services"></asp:Label>
                                <asp:TextBox ID="txtFinance" runat="server" CssClass="materialize-textarea" placeholder="Finance" Rows="8" TextMode="MultiLine" required="true"></asp:TextBox>
                            </div>
                            
                        </div>
                         <div class="row">
                            <div class="col s12 m12 l10 offset-l1">
                                <asp:Label CssClass="control-label" For="txtNriService" ID="Label1" runat="server" Text="Nri services"></asp:Label>
                                <asp:TextBox ID="txtNriService" runat="server" CssClass="materialize-textarea" placeholder="Finance" Rows="8" TextMode="MultiLine" required="true"></asp:TextBox>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col s12 m12 l10 offset-l1">
                                <asp:Label CssClass="control-label" For="txtInterior" ID="lblInterior" runat="server" Text="Interior Services"></asp:Label>
                                <asp:TextBox ID="txtInterior" runat="server" CssClass="materialize-textarea" placeholder="Finance" Rows="8" TextMode="MultiLine" required="true"></asp:TextBox>
                            </div>
                            
                        </div>
                  <div class="row">
                          <div class="col s12 m12 l12 center">
                              <asp:Button  ID="submitService"  runat="server" text="save" 
                                  CssClass="btn btn-success card-link" onclick="submitService_Click"  />
                       </div>
                  
                  </div>
                    </div> 
                     
            
        </div>

        </div>
        </div>
       
</asp:Content>

