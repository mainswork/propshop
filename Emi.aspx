﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Emi.aspx.cs" Inherits="Emi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<!--for slider range-->

<!--<script src="resources/js/nouislider.min.js"></script>-->

<!--high chart for -->
<script src="resource/js/highchart.js"></script>

<!--for slider range-->
<link rel="stylesheet" href="resource/css/nouislider.css">

<style>
    .container
    {
        width:80%;
        }
.noUi-connect {
	background: #ff7474;
	border-radius: 10px;
	box-shadow: inset 0 0 3px rgba(51, 51, 51, 0.45);
	-webkit-transition: background 450ms;
	transition: background 450ms;
}

.noUi-horizontal .noUi-handle {
	width: 34px;
	height: 16px;
	left: -17px;
	top: -3px;
}

.noUi-target {
	background: #FAFAFA;
	border-radius: 10px;
	border: 1px solid #D3D3D3;
	box-shadow: inset 0 1px 1px #F0F0F0, 0 3px 6px -5px #BBB;
}

.noUi-horizontal {
	height: 12px;
}

.noUi-handle:before, .noUi-handle:after {
	content: "";
	display: block;
	position: absolute;
	height: 5px;
	width: 1px;
	background: #E8E7E6;
	left: 14px;
	top: 6px;
}

table, td, th {
	border: 1px solid #9e9e9e !important;
	font-size: 13px;
	text-align:center;
	/*border-width: 1px;*/
}

tbody tr:hover {
	background-color: #DDDDDD !important;
	z-index: 2;
}

.peach {
	background-color: #ff7474;
}

label {
	color: black;
}
 @media only screen and (max-width:600px){
.noUi-value {
    position: absolute;
    white-space: nowrap;
    text-align: center;
    font-size: 10px;
}

}
</style>
<script>
    $(document).ready(function () {
        $(".dropdown-content li").click(function () {
            var a = $(".dropdown-content li").hasClass('active');
            if (a === true) {

                $("#selectService-error").css("display", "none");
            }
            else if (a === false) {
                $("#selectService-error").css("display", "block");
            }

        });
        $('.num').keypress(function (event) {
            var key = event.which;

            if (!(key >= 48 && key <= 57 || key === 13))
                event.preventDefault();
        });
        $('#roipara').keydown(function (e) {
-1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        });

        document.getElementById('roipara').onkeypress = function (e) {

            if (e.keyCode === 46 && this.value.split('.').length === 2) {
                return false;
            }

        }
        if ($(window).width() <= 992) {

            $(".amt").focus(function () { $(this).attr("type", "number") });

        }
    });
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />
    <br />
    <div class="container">
<div class="row">
		<div class="col l12 m12 s12 cneter">
			<h4 class="text-center">Emi Calculator</h4>
			<br />
		</div>

		<div class="col l8 m8 s12">
			<div class="col s12 m12 l12">	
               <div id="lamount"></div> 
			<br /><br /><br />
				<div class="input-field col s12 m6 l6">
					<label for="lamountpara"> Loan Amount(Rs)</label>
					 <input type="text" id="lamountpara" class="num amt" required value="5000000"> 
				</div>
			</div>	
            <div class="col s12 m12 l12">
                             <p>
                      <input name="month" type="radio" id="month" value="month" checked="checked"/>
                      <label for="month">Month</label>
   
                      <input name="month" type="radio" id="year" value="year" />
                      <label for="year">Year</label>
                    </p>
				
                    <br />
			</div>
			<div class="col s12 m12 l12">
			
				<div id="noOfMonth" class=""></div> 
				<br/><br/><br/>
				<div class="input-field col s12 m6 l6">
					<label for="noOfMonthpara"><span id="labelForyear">Number of Months</span></label>
					 <input type="text" id="noOfMonthpara" class="num amt" required>
                     <br />
				<br />
                </div>
			</div>
			<div class="col l12 m12 s12">
				<div id="roi"></div>
				<br/> <br/><br/>
				<div class="input-field col s12 m6 l6">
					<label for="roipara">Rate of Interest(%)</label> 
                    <input type="text" id="roipara" class="amt" required> <br/> 
				</div>
                 <br />
			</div>
		</div>
		<div class="col s12 m4 l4">
			<div id="container" class="center-align  greycolor"
				style="height: 350px"></div>
			<!--<div id="container"></div>-->
		</div>
        </div>
		<div class="row">
			<div class="col l3 m3 s12">
            <div class="card blue-grey lighten-4">
              <div class="card-content">
                <h4 class="card-title">Monthly Emi</h4>
                <h5 class="card-text">&#8377;<span id="emipara"></span></h5>
   
              </div>
            </div>		

			</div>
            <div class="col l3 m3 s12">
            <div class="card blue-grey lighten-4">
              <div class="card-content">
                <h4 class="card-title">Total Interest</h4>
                <h5 class="card-text">&#8377;<span id="totalInterest"></span></h5>
   
              </div>
            </div>		

			</div>

            <div class="col l3 m3 s12">
            <div class="card blue-grey lighten-4">
              <div class="card-content">
                <h4 class="card-title">Payble Amount</h4>
                <h5 class="card-text">&#8377;<span id="paybleAmtPara"></span></h5>
   
              </div>
            </div>		

			</div>
            <div class="col l3 m3 s12">
            <div class="card blue-grey lighten-4">
              <div class="card-content">
                <h4 class="card-title">Interest Percentage</h4>
                <h5><span id="interestPercentage"></span>%</h5>
   
              </div>
            </div>		

			</div>
			
</div>
   
       <div class="col m12 l12 s12 center-align">
   		<table class="table table-bordered center-align" id="tblData" cellspacing="0" width="100%">
				<thead>
					<tr class="table-secondary">
						<th width="10%;">Payment No.</th>
						<th width="10%;">Principal</th>
						<th width="10%;">Interest</th>
						<th width="10%;">EMI</th>
						<th width="10%;">Balance</th>
					</tr>
				</thead>
				<tbody id="tblcontent">
				</tbody>
			</table>
		</div>


</div>
    <!--script for emi calulator start-->
	<script src="resource/js/wNumb.js"></script>
	<script src="resource/js/nouislider.js"></script>
	<script>
         var laSlider = document.getElementById("lamount"); 
        var lainput = document.getElementById("lamountpara");
        var monthSlider = document.getElementById("noOfMonth");
        var monthinput = document.getElementById("noOfMonthpara");
        var roiSlider = document.getElementById("roi");
        var roiinput = document.getElementById("roipara");
        var year = document.getElementById('year');
    	var month = document.getElementById('month');

    	 function updateSliderRange ( min, max ) {
    		 monthSlider.noUiSlider.updateOptions({
    	    		range: {
    	    			'min': min,
    	    			'max': max
    	    		}
    	    	});
    	    }
    	    	
    	    year.addEventListener('click', function(){
    	    	updateSliderRange(0, 30);
    	    });

    	    month.addEventListener('click', function(){
    	    	updateSliderRange(0, 360);
    	    });
    	
        // loan Amount  slider
         noUiSlider.create(laSlider, {
                start: [500000],
                connect: 'lower', // direction: 'rtl',
                range: {
                    'min': 0,
                    'max': 20000000
                },
                tooltips: true,
                format: wNumb({
                    decimals: 0,
                    thousand: ',',
                    prefix: ''
                }),
                pips: {
                    mode: 'count',
                    density: 1,
                    values: 6,
                }
            }

        ); 
      

   
        // month Slider
        noUiSlider.create(monthSlider, {
                start: [12],
                connect: 'lower', // direction: 'rtl',
                range: {
                    'min': 0,
                    'max': 360
                },
                tooltips: true,
                format: wNumb({
                    decimals: 0,
                    thousand: ',',
                    prefix: ''
                }),
                pips: {
                    mode: 'count',
                    density: 1,
                    values: 6,

                }
            }

        );

        // roi slider

        noUiSlider.create(roiSlider, {
                start: [7],
                connect: 'lower', // direction: 'rtl',
                range: {
                    'min': 5,
                    'max': 30,
                },
                tooltips: true,
                format: wNumb({
                    decimals: 2,
                    thousand: ',',
                    prefix: ''
                }),
                pips: {
                    mode: 'count',
                    density: 1,
                    values: 6,

                }
            }

        );
		
     // loan Amount  slider
         lainput.addEventListener('blur', function() {
                  alert(this.value);
                laSlider.noUiSlider.set(this.value);

            }

        );
         laSlider.noUiSlider.on('update', function(values) {
                // alert(values);
                lainput.value = values;
                emiCal();
            }

        ); 
       
        // month Slider

         
        
        monthinput.addEventListener('blur', function() {
            //  alert(this.value);
            monthSlider.noUiSlider.set(this.value);

        }

    );
        monthSlider.noUiSlider.on('update', function(values) {
                // alert(values);
                monthinput.value = values;
                emiCal();
            }

        );

        
     // roi slider
        roiinput.addEventListener('blur', function() {
                //  alert(this.value);
                roiSlider.noUiSlider.set(this.value);
            }

        );
        roiSlider.noUiSlider.on('update', function(values) {
                // alert(values);
                roiinput.value = values;
                emiCal();
            }

        );

        $(document).ready(function() {
            emiCal();
            $("#lamountpara").blur(function(){
            	emiCal();
            });
        });

        function emiCal() {
            // for remove comma
            var num = $("#lamountpara").val();
            num = num.replace(/,/g, "");
            // alert(num);
            var loanAmount = num;
            var abc;
            var numberOfMonths;
            var monthOrYear = $("#noOfMonthpara").val();
            if($("input[name='month']:checked").val()==="month"){
        		//alert("month");
        		abc=monthOrYear
        	}
        		else if($("input[name='month']:checked").val()==="year"){
        			//alert("year");
        			abc=monthOrYear*12;
        		}    
            $("input[name='month']").change(function(){
            //	console.log($("input[name='month']").val());
            	if($("input[name='month']:checked").val()==="month"){
            		$("#labelForyear").text("Number of Months");
            	}
            		else if($("input[name='month']:checked").val()==="year"){
            			//alert("year");
            			abc=monthOrYear*12;
            			//$("#noOfMonthpara").val(abc);		
            			numberOfMonths = abc;
            			$("#labelForyear").text("Number of Years");
            		}           	
            });
            numberOfMonths = abc;
           // var numberOfMonths = $("#noOfMonthpara").val();
            var rateOfInterest = $("#roipara").val();
            var monthlyInterestRatio = (rateOfInterest / 100) / 12;
            //alert(monthlyInterestRatio);

            // top part of formula (1+roi)powerOf(n)
            var top = Math.pow((1 + monthlyInterestRatio), numberOfMonths);
            //alert(top);

            // bottom part of formula ((1+roi)powerOf(n)-1)
            var bottom = top - 1;
            // alert(bottom);
            var sp = top / bottom;
            //alert(sp);
            var emi = ((loanAmount * monthlyInterestRatio) * sp);
         //   console.log(emi);
            var paybleAmt = emi * numberOfMonths;
            var Totalinterest = paybleAmt - loanAmount;
            var interestPercentage = (Totalinterest / paybleAmt) * 100;

            // set the values total interest and payble amount 
            $("#emipara").text(Math.round(emi));
            $("#totalInterest").text(Math.round(Totalinterest));
            $("#paybleAmtPara").text(Math.round(paybleAmt));
            $("#interestPercentage").text(Math.round(interestPercentage));


            // calculating table content
            var beginingAmt = parseInt(loanAmount);
            var interestAmt = 0;
            var principle = 0;
            var endingAmt = 0;
            var a = 0;
            // Return today's date and time
            var currentTime = new Date()
                // returns the year (four digits)
            var year = currentTime.getFullYear();
            //returns current month
            var currentMonth = currentTime.getMonth();
           
          	//var currentMonth=10;
            //we calculate c to iterrate 1st year
            var c = 12 - currentMonth;
            // calcuate j to iterate last year
            //var jNumber = 13 - c;
            //calcuates how many times outer loop should rotate
            //var iNumber = numberOfMonths - c;
             var exitLastLoop;
            var yearData = 0;
            var newMonth;
            var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            //outer loop for printing year n times given by user
            var tblcontent = "";
            for (var i = 1; i <= numberOfMonths; i++) {
                if (c > 0 && c<numberOfMonths) {
                    for (var x = 0; x < c; x++) {
                        interestAmt = beginingAmt * ((rateOfInterest / 100) / 12);
                        principle = emi - interestAmt;
                        endingAmt = beginingAmt - principle;
                        yearData = year;
                        var setMonth = currentMonth + x;
                        tblcontent += "<tr><td>" + yearData + "    " + monthNames[setMonth] + "</td><td>" + Math.round(principle) + "</td><td> " + Math.round(interestAmt) + "</td><td>" + Math.round(emi) + "</td><td>" + Math.round(endingAmt) + "</td></tr>";
                        console.log(yearData);
                        beginingAmt = beginingAmt - principle;
                        //  arr.push(year);
                        a = 13;
                        i = i + 1;
                    }
                    //c set to 0 so as to disable loop of 1st year
                    c = 0;
                    // this variable executes last loop if number months are greater than 12
                    exitLastLoop=1;
                }
                else if(c > 0 && c>=numberOfMonths){
                	   for (var x = 0; x < numberOfMonths; x++) {
                           interestAmt = beginingAmt * ((rateOfInterest / 100) / 12);
                           principle = emi - interestAmt;
                           endingAmt = beginingAmt - principle;
                           yearData = year;
                           var setMonth = currentMonth + x;
                           tblcontent += "<tr><td>" + yearData + "    " + monthNames[setMonth] + "</td><td>" + Math.round(principle) + "</td><td> " + Math.round(interestAmt) + "</td><td>" + Math.round(emi) + "</td><td>" + Math.round(endingAmt) + "</td></tr>";
                           console.log(yearData);
                           beginingAmt = beginingAmt - principle;
                           //  arr.push(year);
                           a = 13;
                           i = i + 1;
                       }
                       //c set to 0 so as to disable loop of 1st year
                       c = 0;   
                       // this variable prevents last loop to execute if number months are less than 12
                       exitLastLoop=0;
                      
                }
               if( exitLastLoop===1){
                if (a >= 12) {
                    year = year + 1;
                    a = 0;
                }
                //prints year
                yearData = year;
                //reset month number after reaching december
                if (setMonth >= 11) {
                    setMonth = 0;
                }
                if (newMonth >= 11) {
                    newMonth = 0;
                }
                newMonth = setMonth + a;
                interestAmt = beginingAmt * ((rateOfInterest / 100) / 12);
                principle = emi - interestAmt;
                endingAmt = beginingAmt - principle;
                tblcontent += "<tr><td>" + yearData + "    " + monthNames[newMonth] + "</td><td>" + Math.round(principle) + "</td><td> " + Math.round(interestAmt) + "</td><td>" + Math.round(emi) + "</td><td>" + Math.round(endingAmt) + "</td></tr>";
                console.log(yearData);
                a = a + 1;
                beginingAmt = beginingAmt - principle;
                //arr.push(year);
               // return false;
            }
               else{
               	
               }
            }
            
            $("#tblcontent").html(tblcontent);
            $("#tblcontent").change();
            $('#container').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: 'EMI Calculator'
                },
                colors: ['#ff7474', '#b0bec5'

                ],
                tooltip: {
                    //pointFormat: '{series.name}: <b>{point.value}%</b>'
                },
                //for remove chart linked to highchart.com
                credits: ({
                    enabled: false
                }),
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            //	enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Amount',
                    data: [
                        ['Loan', eval(loanAmount)],
                        ['Interest', eval(Math.round(Totalinterest))]
                    ]
                }]
            });
        }
    </script>

	<!--script for emi calulator End-->

</asp:Content>

