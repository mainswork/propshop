﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AboutusForm.aspx.cs" Inherits="AboutusForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    var teams = [];
    var contactItems = [];

    $(document).ready(function () {
        var teamsStr = $('#<%= managementTeamPreData.ClientID %>').val();
        var contactPerStr = $('#<%= contactPersonPreData.ClientID %>').val();
        if (teamsStr != '' && teamsStr != undefined) {
            var teamDataList = teamsStr.split("-");
            for (var i = 0; i < teamDataList.length; i++) {
                //var imgDataSingle = imgDataList[i].split(",");
                teams.push([teamDataList[i]]);
            }
            resetTeamMember();
        }
        if (contactPerStr != '' && contactPerStr != undefined) {
            var contactPerDataList = contactPerStr.split("-");
            for (var i = 0; i < contactPerDataList.length; i++) {
                var contactInfoSingle = contactPerDataList[i].split(",");
                contactItems.push([contactInfoSingle[0], contactInfoSingle[1], contactInfoSingle[2]]);
            }
            resetContactDetail();
        }
        $('#<%= managementTeamPreData.ClientID %>').val("");
        $('#<%= contactPersonPreData.ClientID %>').val("");
        $('#addTeam').click(function () {

            var teamMemberName = $('#txtTeam').val();
            if (teamMemberName.trim() === "") {
                alert("Enter team member name");
                return false;
            }

            for (var i = 0; i < teams.length; i++) {
                var teamMemberOld=teams[i].toString();
                if (teamMemberOld.toLowerCase() === teamMemberName.trim().toLowerCase()) {
                    alert("This team member already added");
                    return false;
                }
            }

            teams.push(teamMemberName);
            $('#listMember').empty();
            resetTeamMember();
            $('#txtTeam').val('');
        });

        $('#txtContactNo').keypress(function (event) {
            var key = event.which;

            if (!(key >= 48 && key <= 57 || key === 13))
                event.preventDefault();
        });

        $('#contactItem').click(function () {
            var txtContactNo = $('#txtContactNo').val();
            var txtContactPerson = $('#txtContactPerson').val();
            var txtEmail = $('#txtEmail').val();

            if (txtContactNo.trim() === "" || txtContactPerson.trim() === "" || txtEmail.trim() === "") {
                alert("Enter Contact Details");
                return false;
            }

            if (txtContactNo.length != 10) {
                alert("Mobile number must be 10 digit");
                return false;
            }

            if (!txtEmail.includes("@") || !txtEmail.includes(".")) {
                alert("Invalid Email Id");
                return false;
            }

            for (var i = 0; i < contactItems.length; i++) {
                var personName = contactItems[i][0].toString();
                var personEmail =contactItems[i][2].toString();
                if (personName.toLowerCase() === txtContactPerson.trim().toLowerCase() ||
                    parseInt(contactItems[i][1]) == parseInt(txtContactNo) ||
                    personEmail.toLowerCase() === txtEmail.trim().toLowerCase()) {
                    alert("This contact detail already exist");
                    return false;
                }
            }

            contactItems.push([txtContactPerson, parseInt(txtContactNo), txtEmail]);
            resetContactDetail();

        });

        $('#<%= submitAboutUs.ClientID %>').click(function () {
            //txtTeamMemberList
            var txtTeamMemberList = "";
            for (var i = 0; i < teams.length; i++) {
                txtTeamMemberList += teams[i] + ",";
            }
            txtTeamMemberList = txtTeamMemberList.substring(0, txtTeamMemberList.length - 1);
            $('#<%= txtTeamMemberList.ClientID %>').val(txtTeamMemberList);
            //alert(txtTeamMemberList);
            if (txtTeamMemberList == "") {
                alert("Enter Management team members");
                return false;
            }
            //txtContactPersonList
            var txtContactPersonList = "";
            for (var i = 0; i < contactItems.length; i++) {
                txtContactPersonList += contactItems[i][0] + "," + contactItems[i][1] + "," + contactItems[i][2] + "-";
            }
            txtContactPersonList = txtContactPersonList.substring(0, txtContactPersonList.length - 1);
            $('#<%= txtContactPersonList.ClientID %>').val(txtContactPersonList);
            // alert(txtContactPersonList);

            if (txtContactPersonList == "") {
                alert("Enter Contact Persons Details");
                return false;
            }

        });

    });
    function resetTeamMember() {
        $('#listMember').empty();
        for (var i = 0; i < teams.length; i++) {
            $('#listMember').append('<tr>' +
                                    '<td>' + (i + 1) + '</td>' +
                                    '<td>' + teams[i] + '</td>' +
                                    '<td><button class="btn" type="button"  onclick="deleteTeamMember(' + i + ')"><i class="fa fa-remove"></i></button></td>' +
                                    '</tr>');
        }
    }
    function resetContactDetail() {
        $('#tblContact').empty();
        for (var i = 0; i < contactItems.length; i++) {
            $('#tblContact').append('<tr>' +
                                    '<th>' + (i + 1) + '</th>' +
                                    '<th>' + contactItems[i][0] + '</th>' +
                                    '<th>' + contactItems[i][1] + '</th>' +
                                    '<th>' + contactItems[i][2] + '</th>' +
                                    '<td><button class="btn" type="button"  onclick="deleteContactDetail(' + i + ')"><i class="fa fa-remove"></i></button></td>' +
                                    '</tr>');
        }
    }
    function deleteTeamMember(index) {
        teams.splice(index, 1);
        resetTeamMember();
        
    }
    function deleteContactDetail(index) {
        contactItems.splice(index, 1);
        resetContactDetail();
    }
</script>
<style>
    .tblborder
    .tblborder th,
    .tblborder td
    {
        border:1px solid grey !important;
    }
</style>
<div class="row">
    <div class="col s12 m12 l12 z-depth-3">
        <h5 class="center">About us form
        </h5>
     
        <div class="container">
             <asp:HiddenField ID="managementTeamPreData" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="contactPersonPreData" runat="server"></asp:HiddenField>
            <div class="row">
                <div class="input-filed col s12 m6 l6">
                    <asp:Label CssClass="control-label" For="txtMissionVison" ID="lblMissionVison" runat="server" Text="Mission Vision"></asp:Label>
                       <asp:TextBox ID="txtMissionVison" runat="server" CssClass="materialize-textarea" placeholder="Mission Vision" Rows="8" TextMode="MultiLine" required="true"></asp:TextBox>
                </div>
            
                <div class="input-filed col s12 m6 l6">
                     <asp:Label CssClass="control-label" For="txtAboutCompany" ID="lblAboutCompany" runat="server" Text="About Company"></asp:Label>
                       <asp:TextBox ID="txtAboutCompany" runat="server" CssClass="materialize-textarea" placeholder="About Company" Rows="8" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="input-filed col s12 m6 l6">
                    <asp:Label CssClass="control-label" For="txtAddress" ID="lblAddress" runat="server" Text="Address"></asp:Label>
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="materialize-textarea" placeholder="Address" Rows="8" TextMode="MultiLine"></asp:TextBox>
                </div>
           

                <div class="input-filed col s12 m6 l6">
                     <asp:Label CssClass="control-label" For="txtCSR" ID="lblCSR" runat="server" Text="CSR"></asp:Label>
                       <asp:TextBox ID="txtCSR" runat="server" CssClass="materialize-textarea" placeholder="Mission Vision" Rows="8" TextMode="MultiLine" required="true"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 l6 input-field">
                           <asp:Label CssClass="control-label" For="txtlat" ID="Label1" runat="server" Text="Enter Lat" ></asp:Label>
                           <input type="text" name="txtlat" runat="server" id="txtlat" value=" "  class="form-control" placeholder="" required/>
                    </div>
                    <div class="col s12 m6 l6 input-field">
                           <asp:Label CssClass="control-label" For="txtlong" ID="Label3" runat="server" Text="Enter Long"></asp:Label>
                           <input type="text" name="txtlong" runat="server" id="txtlong" value=" "  class="form-control" placeholder="" required/>
                    </div>
            </div>
            <div class="row">
                <div class="input-filed col s12 m6 l6">
                     <asp:Label CssClass="control-label" For="txtTeam" ID="lblTeam" runat="server" Text="Management Team"></asp:Label>
                         <input type="text" name="txtTeam" id="txtTeam" value=" "  class="form-control" placeholder="Management Team" />
                            <input type="hidden" name="txtTeamMemberList"  id="txtTeamMemberList" runat="server" />
                          
                </div>
                 <div class="input-filed col s12 m6 l6 center">
                     <button type="button" id="addTeam" class="btn">add</button>
                 </div>
                <div class="row">
                    <div class="col s12 m12 l12">
                        <h6 class="center">
                        Member details table
                        </h6>
                    <table class="striped centered">
                            <thead>
                                <tr>
                                <th>Sr.No</th>
                                <th>Name</th>
                                <th>Remove</th>
                                </tr>
                            </thead> 
                            <tbody id="listMember">
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12 m4 l4 input-field">
                           <asp:Label CssClass="control-label" For="txtContactPerson" ID="lblContactPerson" runat="server" Text="Contact Person"></asp:Label>
                            <input type="text" name="txtContactPerson" id="txtContactPerson" value=" "  class="form-control" placeholder="Contact Person" />
                    </div>
                     <div class="col s12 m4 l4 input-field">
                           <asp:Label CssClass="control-label" For="txtContactNo" ID="lblContactNo" runat="server" Text="Contact Number"></asp:Label>
                           <input type="text" name="txtContactNo" id="txtContactNo" value=" " minlength="10" maxlength="10" class="form-control" placeholder="Contact Number" />
                    </div>
                     <div class="col s12 m4 l4 input-field">
                           <asp:Label CssClass="control-label" For="txtEmail" ID="Label2" runat="server" Text="Contact Email"></asp:Label>
                            <input type="text" name="txtEmail" id="txtEmail" value=" "  class="form-control" placeholder="Contact Email" />
                           <input type="hidden" name="txtTeam" id="txtContactPersonList"  runat="server" />
                            
                    </div>
                    <div class="col s12 m12 l12 center"> 
                     <button type="button" class="btn" id="contactItem">add</button>
                    </div>       

                         </div>
                <div class="col s12 m12 l12">
                 <table class="striped centered">
                            <thead>
                                <tr>
                                <th>Sr.No</th>
                                <th>Contact Person</th>
                                <th>Mobile Number</th>
                                <th>Email Id</th>
                                <th>Remove</th>
                                </tr>
                            </thead> 
                            <tbody id="tblContact">
                            </tbody>
                        </table>
                </div>
                <div>
                </div>

                <div class="row">
                    <div class="col s12 m12 l12 center">
                        <br />
                     <asp:Button ID="submitAboutUs"  runat="server" text="Save" CssClass="btn btn-success card-link" OnClick="btnAboutUsSubmit_Click" />
                    </div>
                </div>
                           </div>
          
                    
            <div>
               
            </div>
        </div>
        </div>
    </div>
</div>
</asp:Content>

