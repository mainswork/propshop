﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Forgot_password : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        lblSecurityQ.Visible = false;
        ddlSecurity.Visible = false;
        txtAns.Visible = false;
    }
    protected void btndone_Click(object sender, EventArgs e)
    {
        String s2 = txtAns.Text;
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
                SqlCommand cmd2 = new SqlCommand("select count(*) from customer where Answer='" + s2 + "'");
                cmd2.Connection = conn;
                int result2 = Convert.ToInt32(cmd2.ExecuteScalar());
                if (result2 > 0)
                {

                    String s3 = Convert.ToString(Session["cust_email"]);
                    SqlCommand cmd = new SqlCommand("SELECT password FROM customer WHERE email ='" + s3 + "';", conn);
                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        String display = "Your password is:" + Convert.ToString(dr["password"]);
                        ClientScript.RegisterStartupScript(this.GetType(), "Myalert", "alert('" + display + "');", true);
                     }
                }
                


            }


        }
        catch (Exception ex)
        {

        }
    }

    protected void btnverify_Click(object sender, EventArgs e)
    {
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select count(*) from customer  where email='" + txtEmail.Text + "'");
                cmd.Connection = conn;
                int result = Convert.ToInt32(cmd.ExecuteScalar());
                if (result > 0)
                {
                    Session["cust_email"] = txtEmail.Text.Trim();
                    ddlSecurity.Visible = true;
                    String s1 = ddlSecurity.SelectedItem.Text;
                    SqlCommand cmd1 = new SqlCommand("select count(*) from customer where SecurityQuestion='" + s1 + "'");
                    cmd1.Connection = conn;
                    ddlSecurity.Visible = true;
                    txtAns.Visible = true;
                    int result1 = Convert.ToInt32(cmd1.ExecuteScalar());
                    if (result1 > 0)
                    {
                        txtAns.Visible = true;
                        btnVerify.Visible = true;
                        btndone.Visible = false;


                    }

                }
                else
                {
                    String display = "invalid Email";
                    ClientScript.RegisterStartupScript(this.GetType(), "Myalert", "alert('" + display + "');", true);
                    conn.Close();
                }
            }
        }
        catch (Exception ex2)
        {
        }
    }

}