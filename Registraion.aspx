﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Registraion.aspx.cs" Inherits="Registraion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="panel-primary">
            <div class="page-header">
                <h3>Register..</h3>
            </div>
            <div class="jumbotron">
                <div class="container">
                    <fieldset>
                        <div class="form-group">
                            <asp:Label ID="lblName" runat="server" for="txtName" Text="Name:"></asp:Label>
                            <asp:TextBox ID="txtName" placeholder="Name" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Name is Mandatory" Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Name is Mandatory" ValidationGroup="vgEmployee" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="recName" runat="server" ErrorMessage="Please enter only character"
                                ControlToValidate="txtName" Display="Dynamic" ToolTip="Please enter only character" Text="*"
                                ValidationGroup="vgEmployee" ValidationExpression="^[a-zA-Z\s]+$"></asp:RegularExpressionValidator>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="lblMobileNo" runat="server" for="txtMobileNo" Text="Mobile No"></asp:Label>
                            <asp:TextBox ID="txtMobileNo" placeholder="Mobile No" CssClass="form-control" runat="server"></asp:TextBox>
                           <asp:RegularExpressionValidator ID="revtxtMobileNo" runat="server" ControlToValidate="txtMobileNo"
                                        ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
                                        Text="Enter Valid Mobile Number." ToolTip="Enter Valid Mobile Number." ValidationGroup="vgEmployee"
                                        ForeColor="Red" SetFocusOnError="True"></asp:RegularExpressionValidator>

                        </div>

                        <div class="form-group">                 
                                <asp:Label CssClass="control-label" For="txtEmail" ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Invalid Email."
                                ControlToValidate="txtEmail" Display="Dynamic" ToolTip="Invalid Email." Text="*"
                                ValidationGroup="vgEmployee" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                
                             </div>

                        <div class="form-group">
                                <asp:Label CssClass="control-label" For="txtPassword" ID="Password" runat="server" Text="Password"></asp:Label>
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" placeholder="Password" TextMode="Password" Text="*"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Password is Mandatory" Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Password is Mandatory" ValidationGroup="vgEmployee" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>    
                        </div>
                        <div class="form-group">
                                <asp:Label CssClass="control-label" For="ddlSecurity" ID="lblSecurityQ" runat="server" Text="Select Security Question"></asp:Label><br />
                                <div class="col-lg-6">
                                <asp:DropDownList ID="ddlSecurity" runat="server" CssClass="form-control">
                                        <asp:ListItem>What is your pet name?</asp:ListItem>
                                        <asp:ListItem>Who is your favourite teacher?</asp:ListItem>
                                        <asp:ListItem>Who is your favourite subject?</asp:ListItem> 
                                        <asp:ListItem>Who is your First Mobile No.?</asp:ListItem>
                                    </asp:DropDownList>
                                                    </div>
                                                    
                                     <div class="col-lg-6">                                                   
                                    <asp:TextBox ID="txtAns" runat="server" CssClass="form-control" placeholder="Answer" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAns" runat="server" ErrorMessage="Answer is Mandatory" Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Answer is Mandatory" ValidationGroup="vgEmployee" ControlToValidate="txtAns"></asp:RequiredFieldValidator>    
                        </div> 
                        </div>                                                                                                                                                     <div class="form-group">
                            <div class="col-lg-6 ">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btnCancel_Click" />
                            </div>
                            <div class="col-lg-6 ">
                                <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="btn btn-primary" ValidationGroup="vgEmployee" OnClick="btnRegister_Click" />
                            </div>
                        </div>
                  </fieldset>
                </div>
            </div>
        </div>
    </div>
   
</asp:Content>

