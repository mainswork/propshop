﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Configuration;

public partial class sliderImg : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

        this.DataBind();

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
        }
        /*slider imges*/
        String imgDataListOld = "";
        String query = "select * from slider_images";
        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
            imgDataListOld += dr["img_base64"].ToString() + "," + dr["img_name"].ToString() + "-";
        }

        if (imgDataListOld.Length > 0)
        {
            imgDataListOld = imgDataListOld.Substring(0, imgDataListOld.Length - 1);
        }
        sliderPreImgData.Value = imgDataListOld;
        /*slider images*/
        conn.Close();

       

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }

            SqlCommand cmd = new SqlCommand("delete from slider_images;", conn);
            cmd.ExecuteNonQuery();
            conn.Close();

            String imgDataListNew = sliderImgData.Value.Trim();
            String[] imgDataArr = imgDataListNew.Split('-');
            for (int i = 0; i < imgDataArr.Length; i++)
            {
                conn.Open();
                String[] imgData = imgDataArr[i].Split(',');
                String queryImg = "insert into slider_images ([img_name],[img_base64]) " +
                "values (@imgName,@imgBase64);";
                cmd = new SqlCommand(queryImg, conn);
                cmd.Parameters.AddWithValue("@imgName", imgData[2]);
                cmd.Parameters.AddWithValue("@imgBase64", imgData[0] + "," + imgData[1]);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            Response.Redirect("sliderImg.aspx");
        }
        catch (Exception ex) {
            throw ex;
        }
    }
}