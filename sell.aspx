﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="sell.aspx.cs" Inherits="sell" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">  
   </asp:ScriptManager>  
    <script>
        $(document).ready(function () {
            $('#<%= fuImage.ClientID %>').on('change', function () {
                var file = $('#<%= fuImage.ClientID %>')[0].files[0];
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    var encodedFile = reader.result;
                    $("#<%= imgUser.ClientID %>").attr('src',encodedFile);
                    $("#<%= imgUser.ClientID %>").change();
                }
            });
        });
    </script>
    <div class="col-md-8">
        <div class="panel-primary">
            <div class="page-header">
                <h2>Property selling form</h2>
            </div>
            <div class="jumbotron">
                <div class="container">
                    <fieldset>
                        <legend>Property Address</legend>
                        <div class="form-group">                                                                    
                            <div class="col-md-3">
                                <asp:Label ID="lblCompName" runat="server" Text="Complex Name:"></asp:Label>
                            </div>
                            <div class="col-lg-7">
                                <asp:TextBox ID="txtCompName" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>  
                        <div class="form-group">
                         <div class="col-md-7">
                        <asp:UpdatePanel ID="countrypanel" runat="server">
                        <ContentTemplate>
                      <spanclass="style1"><strong>Select Country:</strong></span>  
                      <asp:DropDownList ID="ddlcountry" CssClass="form-control" runat="server" AutoPostBack ="true" AppendDataBoundItems="true" onselectedindexchanged="ddlcountry_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                        <Triggers>  
                        
                             <asp:AsyncPostBackTrigger ControlID ="ddlcountry" />  
                    </Triggers>
                    </asp:UpdatePanel>
                    </div>                        


 

                    <br/>  
   <div class="col-md-7">
 <asp:UpdatePanel ID="statepanel" runat="server">      
    <ContentTemplate>  
        <spanclass="style1"><strong>     Select State:</strong></span>  
        <asp:DropDownList runat="server" ID="ddlstate" CssClass="form-control" AutoPostBack ="true"  AppendDataBoundItems="true"  onselectedindexchanged="ddlstate_SelectedIndexChanged"></asp:DropDownList>
      
    </ContentTemplate>  
    <Triggers>  
       <asp:AsyncPostBackTrigger ControlID ="ddlstate"/>  
       </Triggers>  
    </asp:UpdatePanel>  
       <br/>  
  </div>
  <div class="col-md-7">
<asp:UpdatePanel ID="citypanel" runat="server">       
   <ContentTemplate>        
       <spanclass="style1"><strong> Select City:</strong></span>        
     <asp:DropDownList ID="ddlcity" AutoPostBack ="true" CssClass="form-control" AppendDataBoundItems="true" runat="server">  
     </asp:DropDownList>  
  </ContentTemplate>  
  <Triggers>  
    <asp:AsyncPostBackTrigger ControlID ="ddlcity"/>        
     </Triggers>    
 </asp:UpdatePanel>     
 </div> 




                            <div class="form-group">
                                <div class="col-md-3">
                                    <asp:Label ID="lblCity" runat="server" Text="City:"></asp:Label>
                                </div>
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtCity" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtCity" runat="server" ErrorMessage="City is Mandatory"
                                        Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="City is Mandatory"
                                        ValidationGroup="vgEmployee" ControlToValidate="txtCity"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-lg-2">
                                    <asp:Label ID="lblLocation" runat="server" Text="Location:"></asp:Label>
                                </div>
                                <div class="col-lg-4">
                                    <asp:TextBox ID="txtLocation" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ErrorMessage="Location is Mandatory"
                                        Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Location is Mandatory"
                                        ValidationGroup="vgEmployee" ControlToValidate="txtCity"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <asp:Label ID="lblState" runat="server" Text="State:"></asp:Label>
                            </div>
                            <div class="col-lg-3">
                                <asp:TextBox ID="txtState" CssClass="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="state is Mandatory"
                                    Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="state is Mandatory"
                                    ValidationGroup="vgEmployee" ControlToValidate="txtState"></asp:RequiredFieldValidator>
                            </div>

                            <div class="col-md-3">
                                <asp:Label ID="lblPincode" runat="server" Text="Pincode:"></asp:Label>
                            </div>
                            <div class="col-lg-3">
                                <asp:TextBox ID="txtPincode" CssClass="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Pincode is Mandatory"
                                    Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Pincode is Mandatory"
                                    ValidationGroup="vgEmployee" ControlToValidate="txtPincode"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-3">
                                <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label>
                            </div>
                            <div class="col-lg-6">
                                <asp:TextBox ID="txtAddress" Columns="6" Rows="3" runat="server" 
                                    TextMode="MultiLine" Height="61px" Width="264px"></asp:TextBox>
                            </div>
                        </div>
                        <legend>Property Information</legend>
                        <div class="form-group">
                            <div class="col-lg-3">
                                <asp:Label ID="lblWant" runat="server" for="txtWant" Text="I want to:"></asp:Label>
                            </div>
                            <div class="col-lg-9">
                                <asp:TextBox ID="txtWant" CssClass="form-control" runat="server" Text="Sell" ReadOnly="True"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <div class="col-lg-3">
                                    <asp:Label ID="lblPropType" runat="server" Text="Property Type:"></asp:Label>
                                </div>
                                <div class="col-lg-9">
                                    <asp:DropDownList ID="ddlPropType" runat="server" CssClass="form-control">
                                        <asp:ListItem>Commercial</asp:ListItem>
                                        <asp:ListItem>Residencial</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-3">
                                <asp:Label ID="lblFor" runat="server" Text="For"></asp:Label>
                            </div>
                            <div class="col-lg-4">
                                <asp:DropDownList ID="ddlFor" runat="server" CssClass="form-control">
                                    <asp:ListItem>Rent</asp:ListItem>
                                    <asp:ListItem>Sell</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-5">
                                <asp:DropDownList ID="ddlPropStatus" runat="server" CssClass="form-control">
                                    <asp:ListItem>Property Status(Any)</asp:ListItem>
                                    <asp:ListItem>Pre-Launch</asp:ListItem>
                                    <asp:ListItem>Ready Possession</asp:ListItem>
                                    <asp:ListItem>Resale</asp:ListItem>
                                    <asp:ListItem>Under Construction</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <asp:Label ID="lblBedrooms" runat="server" Text="Bedrooms"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList ID="ddlBedrooms" runat="server" CssClass="form-control">
                                    <asp:ListItem>1RK</asp:ListItem>
                                    <asp:ListItem>1BHK</asp:ListItem>
                                    <asp:ListItem>1.5BHK</asp:ListItem>
                                    <asp:ListItem>2BHK</asp:ListItem>
                                    <asp:ListItem>3BHK</asp:ListItem>
                                    <asp:ListItem>4BHK</asp:ListItem>
                                    <asp:ListItem>5+</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="lblBathroom" runat="server" Text="Bathrooms"></asp:Label>
                            </div>
                            <div class="col-lg-3">
                                <asp:DropDownList ID="ddlBathroom" runat="server" CssClass="form-control">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5+</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <asp:Label ID="lblfloor" runat="server" Text="Floor"></asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control">
                                    <asp:ListItem>below 10 floor</asp:ListItem>
                                    <asp:ListItem>Above 10 floor</asp:ListItem>
                                    <asp:ListItem>Ground floor only</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <asp:Label ID="lblArea" runat="server" Text="Area"></asp:Label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtArea" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-lo-3">
                                <asp:Label ID="lblSqft" runat="server" Text="Sq.ft"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <asp:Label ID="lblPrice" runat="server" Text="Price"></asp:Label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtPrice" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-3">
                                <asp:Label ID="lblParking" runat="server" Text="Parking type"></asp:Label>
                            </div>
                            <div class="col-md-7">
                            <asp:DropDownList ID="ddlParking" runat="server" CssClass="form-control">
                                <asp:ListItem>None</asp:ListItem>
                                <asp:ListItem>Common</asp:ListItem>
                                <asp:ListItem>Private</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                            
                            <div class="col-lg-12">
                                <asp:FileUpload runat="server" ID="fuImage" />
                                <asp:Image ImageUrl="#" Height="30" Width="30" Visible="true" runat="server" ID="imgUser" />
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="col-lg-6 ">
                                <asp:Button ID="btnCancel" runat="server" Text="cancel"  CssClass="btn btn-default center-block"
                                    OnClick="btnCancel_Click" />
                            </div>
                            <div class="col-lg-6 ">
                                <asp:Button ID="btnSubmit" runat="server" CssClass=" btn btn-info center-block "
                                    Text="Submit" OnClick="btnSubmit_Click" />
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
