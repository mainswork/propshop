﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Configuration;
public partial class sell : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    public void Bind_ddlCountry()
    {
        conn.Open();
        SqlCommand cmd = new SqlCommand("select country_Id,country from country", conn);
        SqlDataReader dr = cmd.ExecuteReader();
        ddlcountry.DataSource = dr;
        ddlcountry.Items.Clear();
        ddlcountry.Items.Add("--Please Select country--");
        ddlcountry.DataTextField = "country";
        ddlcountry.DataValueField = "country_Id";
        ddlcountry.DataBind();
        conn.Close();
    }
    public void Bind_ddlState()
    {
        conn.Open();

        SqlCommand cmd = new SqlCommand("select state,state_Id from state where country_Id='" + ddlcountry.SelectedValue + "'", conn);

        SqlDataReader dr = cmd.ExecuteReader();
        ddlstate.DataSource = dr;
        ddlstate.Items.Clear();
        ddlstate.Items.Add("--Please Select state--");
        ddlstate.DataTextField = "state";
        ddlstate.DataValueField = "state_Id";
        ddlstate.DataBind();
        conn.Close();
    }
    public void Bind_ddlCity()
    {
        conn.Open();
        SqlCommand cmd = new SqlCommand("select * from city where state_Id ='" + ddlstate.SelectedValue + "'", conn);

        SqlDataReader dr = cmd.ExecuteReader();
        ddlcity.DataSource = dr;
        ddlcity.Items.Clear();
        ddlcity.Items.Add("--Please Select city--");
        ddlcity.DataTextField = "city";
        ddlcity.DataValueField = "city_Id";
        ddlcity.DataBind();
        conn.Close();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["name"] != null)
        {
            //Label lbl = (Label)this.Master.FindControl("lblCustName");
            //lbl.Text = "Welcome" + Convert.ToString(Session["name"]);
        }
        else
        {
            Response.Redirect("Login1.aspx");
        }
        if (!IsPostBack)
        {
            Bind_ddlCountry();
        }
    }
    
    protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        Bind_ddlState();
    }
    protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
    {
        Bind_ddlCity();
    }  
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        String filename = "";
        //string lat_long = getLatLong();
        //string lat = lat_long.Split('#')[0];
        //string lng = lat_long.Split('#')[1];
        if (fuImage.HasFile)
        {
            try
            {
                
                filename = Path.GetFileName(fuImage.FileName);
                fuImage.SaveAs(Server.MapPath("~/Image/") + filename);
                

            }
            catch (Exception es)
            {
            }
        }

        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            String query = "insert into [complex](comp_name,location,address1,city,state,pincode,Latitude,Longitude) values (@name,@location,@add,@city,@state,@pincode,@lat,@lng);";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@name", txtCompName.Text.Trim());
            cmd.Parameters.AddWithValue("@location", txtLocation.Text.Trim());
            cmd.Parameters.AddWithValue("@add", txtAddress.Text.Trim());
            cmd.Parameters.AddWithValue("@city", txtCity.Text.Trim());
            cmd.Parameters.AddWithValue("@state", txtState.Text.Trim());
            cmd.Parameters.AddWithValue("@pincode", txtPincode.Text.Trim());
            cmd.Parameters.AddWithValue("@lat", 0);
            cmd.Parameters.AddWithValue("@lng", 0);
            cmd.ExecuteNonQuery();

            String query2 = "Select Max (Comp_id) As compid from complex;";
            cmd = new SqlCommand(query2, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {

                int compid = Convert.ToInt16(dr["compid"]);
                ViewState["compid"] = compid;
            }
            dr.Close();

            String query1 = "insert into [Property] ([Comp_id],[Floor1],[Area],[Bhk],[Bathroom],[types1],[prop_status],[prop_for],[price],[images],[owner_id],[isApproved]) values (@compid,@floor,@area,@bhk,@bathroom,@Type,@prop_status,@prop_for,@price,@image,@ownerid,@isApproved);";
            cmd = new SqlCommand(query1, conn);
            cmd.Parameters.AddWithValue("@compid", Convert.ToInt16(ViewState["compid"].ToString()));
            cmd.Parameters.AddWithValue("@floor", ddlFloor.SelectedValue);
            cmd.Parameters.AddWithValue("@area", Convert.ToInt32(txtArea.Text.Trim()));
            cmd.Parameters.AddWithValue("@bhk", ddlBedrooms.SelectedValue);
            cmd.Parameters.AddWithValue("@bathroom", ddlBathroom.SelectedValue);
            cmd.Parameters.AddWithValue("@type", ddlPropType.SelectedValue);
            cmd.Parameters.AddWithValue("@prop_status", ddlPropStatus.SelectedValue);
            cmd.Parameters.AddWithValue("@prop_for", ddlFor.SelectedValue);
            cmd.Parameters.AddWithValue("@price", txtPrice.Text.Trim());
            cmd.Parameters.AddWithValue("@image", filename);
            cmd.Parameters.AddWithValue("@ownerid", Session["Cust_id"]);
            cmd.Parameters.AddWithValue("@isApproved", 'N');
            cmd.ExecuteNonQuery();
            conn.Close();
            clearAll();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('Requested Property Information has been sent successfully.');", true);
            Response.Redirect("Home.aspx");
        }
        catch (Exception ex)
        {

        }
    }

    private string getLatLong()
    {
       
            string latLong = "";
            string url = "http://maps.google.com/maps/api/geocode/xml?address=" + txtLocation.Text + "&sensor=false";
            WebRequest request = WebRequest.Create(url);
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    DataSet dsResult = new DataSet();
                    dsResult.ReadXml(reader);
                    foreach (DataRow row in dsResult.Tables["result"].Rows)
                    {
                        string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
                        DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
                        latLong = location["lat"] + "#" + location["lng"];
                    }

                }
            }
            return latLong;
        }
    
    private void clearAll()
    {
        txtCompName.Text = String.Empty;
        txtCity.Text = String.Empty;
        txtLocation.Text = String.Empty;
        txtState.Text = String.Empty;
        txtPincode.Text = String.Empty;
        txtAddress.Text = String.Empty;
        txtArea.Text = String.Empty;
        txtPrice.Text = String.Empty;

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        clearAll();
    }
}