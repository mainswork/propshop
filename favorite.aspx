﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="favorite.aspx.cs" Inherits="favorite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="jumbotron">
<div>
  Purchase Date: <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
</div>
<div >
  Status of Your Property: <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
</div>  
                                                                  
                <asp:DataList ID="dlstProperty" runat="server"
                 BorderStyle="None" BorderWidth="1px" CellPadding="3" 
        CellSpacing="2" Font-Size="Small"
                 RepeatColumns="5" RepeatDirection="Horizontal" Width="900px" 
        Height="258px" >
                 
                    <HeaderTemplate> Your Intereted Property.. </HeaderTemplate>
                     <ItemStyle HorizontalAlign="Center"  />
                     <ItemTemplate>
                     <div class="row">
                        <div class="col-lg-5 col-md-10">
                              <br />
                 <asp:HyperLink ID="HyperLink1" class="preview" NavigateUrl='<%# Bind("images", "Image/{0}") %>'
runat="server"></asp:HyperLink>


               <asp:ImageButton Width="180px" Style="border: 3px solid #ccc;" Height="170px" ID="imgProperty"
                CommandArgument='<%# Eval("Prop_id") %>' ImageUrl='<%# Bind("images", "Image/{0}") %>' runat="server"  OnClick="imgProperty_Click"/>
                 <div  height: 35px; font-size: 12px;">
             <b>Location:</b><asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location") %>'></asp:Label>
             <b>Price:</b><asp:Label ID="lblPrice" runat="server" Text=' <%# Bind("price") %>'></asp:Label>
                                 </div>
          <asp:Button ID="btndelete" runat="server" Text="" CssClass="fa  fa-bold fa-remove" OnClick="btndelete_Click" CommandArgument='<%# Eval("Prop_id") %>'  />
             </div>
              </div>
          </ItemTemplate>

     <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
              </asp:DataList>
</div>
    
</asp:Content>

