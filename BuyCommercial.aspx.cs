﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;

public partial class BuyCommercial : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                
                string propType = Request.QueryString["propType"];
                string prop = Request.QueryString["prop"];
                ViewState["propType"] = propType;
                ViewState["prop"] = prop; 
                this.BindData(propType,prop);
                fillDDLLocation();
     
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
    private void fillDDLLocation()
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            String query = "select  distinct(location) As loc from complex;";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                ddlLocation.Items.Add(Convert.ToString(dr["loc"]));
            }
            dr.Close();
            conn.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
        
        private void BindData(string propType,string prop)
    {
        string query = "";
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
                if (propType == "C" & prop=="Y")
                {
                    lblView.Text = "Select Property";
                    query = "SELECT * FROM complex c JOIN Property p ON c.Comp_id=p.Comp_id Where p.types1='Commercial' and p.isApproved='Y' and p.prop='Property';";
                }
                else if (propType == "R" & prop == "Y")
                {
                    lblView.Text = "Select Property";
                    query = "SELECT * FROM complex c JOIN Property p ON c.Comp_id=p.Comp_id Where p.types1='Residencial' and p.isApproved='Y' and p.prop='Property';";
                }
                else if (propType == "C" & prop == "N") 
                {
                    lblView.Text = "Select Project";
                    query = "SELECT * FROM complex c JOIN Property p ON c.Comp_id=p.Comp_id Where p.types1='Commercial' and p.isApproved='Y' and p.prop='Project';";
                }
                else if (propType == "R" & prop == "N")
                {
                    lblView.Text = "Select Project";
                    query = "SELECT * FROM complex c JOIN Property p ON c.Comp_id=p.Comp_id Where p.types1='Residencial' and p.isApproved='Y' and p.prop='Project';";
                }
            
            SqlCommand cmd = new SqlCommand(query, conn);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            dlstProperty.DataSource = ds.Tables[0];
            dlstProperty.DataBind();
            string s6 = "";
            if (ds.Tables[0].Rows.Count > 0)
            {
                s6 = ds.Tables[0].Rows[0][3].ToString();
                Session["cat"] = s6;
            }
            else
            {
                if (prop == "Y")
                {
                    lblView.Text = "No Property Found";
                }
                
               else {
                   lblView.Text = "No Project Found";
                }
            }
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                String propType = Convert.ToString(ViewState["propType"]);
                String prop = Convert.ToString(ViewState["prop"]);
                ddlLocation.SelectedIndex = 0;
                ddlPropFor.SelectedIndex = 0;
                ddlBeds.SelectedIndex = 0;
                txtMaxPrice.Text="";
                 txtMinPrice.Text="";
                 lblError1.Text = "";
                this.BindData(propType, prop);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        protected void imgProperty_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                int propid = Convert.ToInt32(((ImageButton)sender).CommandArgument);
                
                Session["propid"] = propid;
                    Response.Redirect("PropertyDetails.aspx");
               

              }
            catch (Exception es)
            {
                throw es;
            }
        }

        private void BindData(string query)
        {
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                SqlCommand cmd = new SqlCommand(query, conn);
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                dlstProperty.DataSource = ds.Tables[0];
                dlstProperty.DataBind();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblView.Text = "Property Found";
                }
                else
                {
                    lblView.Text = "No Property Found";
                }
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }


                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {

            //if ((txtMinArea.Text.Length > 0) || (txtMaxArea.Text.Length > 0))
            //{
            //    if ((Convert.ToInt32(txtMinArea.Text.Length) > 0) && (Convert.ToInt32(txtMaxArea.Text.Length) > 0))
            //    {
            //        // allow.. do nothing
            //    }
            //    else
            //    {

            //        lblError.Text = "if you enter MinArea then MaxArea is required";
            //        return;
            //    }

            //    if (Convert.ToInt32(txtMinArea.Text) > Convert.ToInt32(txtMaxArea.Text))
            //    {
            //        lblError.Text = "Min cannot be greater then MAX";

            //        return;
            //    }
            //    else { lblError.Text = ""; }

            //}

            if ((txtMinPrice.Text.Length > 0) || (txtMaxPrice.Text.Length > 0))
            {
                if ((Convert.ToInt32(txtMinPrice.Text.Length) > 0) && (Convert.ToInt32(txtMaxPrice.Text.Length) > 0))
                {
                    // allow.. do nothing
                }
                else
                {

                    lblError1.Text = "if you enter MinPrice then MaxPrice is required";
                    return;
                }

                if (Convert.ToInt32(txtMinPrice.Text) > Convert.ToInt32(txtMaxPrice.Text))
                {
                    lblError1.Text = "Min cannot be greater then MAX";
                    return;
                }
                else { lblError1.Text = ""; }

            }
            String propType = Convert.ToString(ViewState["propType"]);
            String prop = Convert.ToString(ViewState["prop"]);
            StringBuilder query = new StringBuilder("select * from property P INNER JOIN complex C ON P.Comp_id = C.Comp_id WHERE ");
            if (propType == "C" & prop == "Y")
            {
                lblView.Text = "Select Property";
                query.Append("  P.types1='Commercial' and P.isApproved='Y' and P.prop='Property'");
            }
            else if (propType == "R" & prop == "Y")
            {
                lblView.Text = "Select Property";
                query.Append("  P.types1='Residencial' and P.isApproved='Y' and P.prop='Property'");
                
            }
            else if (propType == "C" & prop == "N")
            {
                lblView.Text = "Select Project";
                query.Append(" P.types1='Commercial' and P.isApproved='Y' and P.prop='Project'");
                
            }
            else if (propType == "R" & prop == "N")
            {
                lblView.Text = "Select Project";
                query.Append("  P.types1='Residencial' and P.isApproved='Y' and P.prop='Project'");
               
            }
            if (ddlLocation.SelectedIndex > 0)
            {
                query.Append(" AND C.Location ='" + ddlLocation.SelectedItem.Text + "'");
            }
            if (ddlPropFor.SelectedIndex > 0)
            {
                query.Append(" AND P.Prop_for ='" + ddlPropFor.SelectedItem.Text.Trim() + "'");
            }
            if (ddlBeds.SelectedIndex > 0)
            {
                query.Append(" AND P.Bhk ='" + ddlBeds.SelectedItem.Text.Trim() + "'");
            }
            //if (ddlPropertyStatus.SelectedIndex > 0)
            //{
            //    query.Append(" AND P.prop_status='" + ddlPropertyStatus.SelectedItem.Text.Trim() + "'");
            //}
            //if (ddlPropertType.SelectedIndex > 0)
            //{
            //    query.Append(" AND P.types1='" + ddlPropertType.SelectedValue + "'");
            //}
            if (txtMinPrice.Text.Length > 0)
            {
                query.Append(" AND P.price between " + Convert.ToInt32(txtMinPrice.Text.Trim()) + " AND " + Convert.ToInt32(txtMaxPrice.Text.Trim()));
            }
            //if (txtMinArea.Text.Length > 0)
            //{
            //    query.Append(" AND P.area between " + Convert.ToInt32(txtMinArea.Text.Trim()) + " AND " + Convert.ToInt32(txtMaxArea.Text.Trim()));
            //}
            BindData(query.ToString());
        }

}
