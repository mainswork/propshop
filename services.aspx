﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="services.aspx.cs" Inherits="services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.parallax').parallax();
        });
        
    </script>
    <style type="text/css">
            .headerText
            {
                padding:20px;
                background-color:White;
                opacity:0.5;
                position:relative;
                z-index:90;
                top:33%;
                width:50%;
                left:20%;
                }
                 .headerText h4
                 {
                       z-index:100;
                 }
    </style>
<div class="row">
 <div class="parallax-container">
      <div class="parallax">
      <img src="resource/img/finance.jpg">

      </div>
    </div>
    <div class="row">
        <div class="col s12 m12 l12 center">
            <h4> Finance Services
            </h4>
        </div>
        <div class="col s12 m10 l10 offset-l1 offset-m1">
            
            <p>
                <asp:Label Text="" ID="lblFinance" runat="server" />
            </p>
        </div>
    </div>
   <div class="parallax-container">
      <div class="parallax"><img src="resource/img/interior.jpg"></div>
    </div>
    <div class="row">
        <div class="col s12 m12 l12 center">
            <h4> Interior Services
            </h4>
        </div>
        <div class="col s12 m10 l10 offset-l1 offset-m1">
            
            <p>
                <asp:Label Text="" ID="lblInterior" runat="server" />
            </p>
        </div>
    </div>
  <div class="parallax-container">
      <div class="parallax"><img src="resource/img/nri.jpg">
         
      </div>
    </div>
    </div>
    <div class="row">
        <div class="col s12 m12 l12 center">
            <h4> NRI Services
            </h4>
        </div>
        <div class="col s12 m10 l10 offset-l1 offset-m1">
            
            <p>
               <asp:Label Text="" ID="lblNri" runat="server" />
            </p>
        </div>
    </div>
</asp:Content>

