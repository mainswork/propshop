﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Security.Cryptography;
/// <summary>
/// Summary description for DBOperations
/// </summary>
public class DBOperations
{
   static DBOperations obj_DBOperations = new DBOperations();
    //public static SqlConnection conn = new SqlConnection(Decrypt( ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString));
    public static SqlConnection conn = null;
    public DBOperations()
    {

    }
    public static string ExecuteScalar(Hashtable htParams, string procedurename)
    {
        SqlCommand cmd = null;
        string result = string.Empty;
       // DBOperations obj_DBOperations = new DBOperations();
        // conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        string con = obj_DBOperations.Decrypt(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        conn = new SqlConnection(con);

        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            cmd = new SqlCommand(procedurename, conn);
            foreach (string param in htParams.Keys)
            {
                cmd.Parameters.AddWithValue(param, htParams[param]);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            result = Convert.ToString(cmd.ExecuteScalar());
        }
        catch (SqlException sqlex)
        {
            DBOperations.LogException(sqlex.Message, sqlex.ToString());
            throw sqlex;
        }
        catch (Exception ex)
        {
            DBOperations.LogException(ex.Message, ex.ToString());
            throw ex;
        }
        finally
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                
            }
        }
        return result;
    }
    public static void LogException(string error, string errorDetails)
    {
        Hashtable htParams = new Hashtable();
        htParams.Add("@ErrorMessage", error);
        htParams.Add("@ErrorDetails", errorDetails);
        //htParams.Add("@ExceptionOccuredOn", DateTime.Now);
        ExecuteNonQuery(htParams, "uspExceptionInsert_Prasad");
    }

    public static DataSet ExecuteDataSet(string procedurename)
    {
        SqlDataAdapter da = null;
        SqlCommand cmd = null;
        DataSet ds = new DataSet();
       // conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        string con = obj_DBOperations.Decrypt(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        conn = new SqlConnection(con);
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            cmd = new SqlCommand(procedurename, conn);
            da = new SqlDataAdapter();
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (SqlException sqlex)
        {
            throw sqlex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        return ds;
    }

    public static DataSet ExecuteDataSet(Hashtable htParams, string procedurename)
    {
        SqlDataAdapter da = null;
        SqlCommand cmd = null;
        DataSet ds = new DataSet();
       // conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        string con = obj_DBOperations.Decrypt(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        conn = new SqlConnection(con);
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            cmd = new SqlCommand(procedurename, conn);
            foreach (string param in htParams.Keys)
            {
                cmd.Parameters.AddWithValue(param, htParams[param]);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (SqlException sqlex)
        {
            throw sqlex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        return ds;
    }

    public static SqlDataReader ExecuteReader(SqlConnection connection, Hashtable htParams, string procedurename)
    {
        SqlDataReader dr = null;
        SqlCommand cmd = null;
        try
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            cmd = new SqlCommand(procedurename, connection);
            foreach (string param in htParams.Keys)
            {
                cmd.Parameters.AddWithValue(param, htParams[param]);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            dr = cmd.ExecuteReader();
        }
        catch (SqlException sqlex)
        {
            throw sqlex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
        return dr;
    }

    public static SqlDataReader ExecuteReader( string procedurename)
    {
        SqlDataReader dr = null;
        SqlCommand cmd = null;
        SqlDataAdapter da = null;
        DataSet ds = new DataSet();
        //conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        string con = obj_DBOperations.Decrypt(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        conn = new SqlConnection(con);

        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            cmd = new SqlCommand(procedurename, conn);
            
            cmd.CommandType = CommandType.StoredProcedure;
            dr = cmd.ExecuteReader();
        }
        catch (SqlException sqlex)
        {
            throw sqlex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
        return dr;
    }

    //Author:- shankar
    public static SqlDataReader ExecuteReader( Hashtable htParams, string procedurename)
    {
        SqlDataReader dr = null;
        SqlCommand cmd = null;
        SqlDataAdapter da = null;
        DataSet ds = new DataSet();
        //conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        string con = obj_DBOperations.Decrypt(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        conn = new SqlConnection(con);
       
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            cmd = new SqlCommand(procedurename, conn);
            foreach (string param in htParams.Keys)
            {
                cmd.Parameters.AddWithValue(param, htParams[param]);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            dr = cmd.ExecuteReader();
        }
        catch (SqlException sqlex)
        {
            throw sqlex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
        return dr;
    }


   
    public static int ExecuteNonQuery(Hashtable htParams, string procedurename)
    {
      //  string con = Decrypt(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
      //  conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        string con = obj_DBOperations.Decrypt(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        conn = new SqlConnection(con);
        SqlCommand cmd = null;
        int noOfRowsAffected = 0;
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            cmd = new SqlCommand(procedurename, conn);
            foreach (string param in htParams.Keys)
            {
                cmd.Parameters.AddWithValue(param, htParams[param]);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            noOfRowsAffected = cmd.ExecuteNonQuery();
        }
         catch (SqlException sqlex)
        {
            throw sqlex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        return noOfRowsAffected;
    }
    public static int ExecuteNonQuery(string procedurename)
    {
        SqlCommand cmd = null;
        int noOfRowsAffected = 0;
        string con = obj_DBOperations.Decrypt(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
        conn = new SqlConnection(con);
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            cmd = new SqlCommand(procedurename, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            noOfRowsAffected = cmd.ExecuteNonQuery();
        }
        catch (SqlException sqlex)
        {
            throw sqlex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        return noOfRowsAffected;
    }
    public static void FillDDL(DropDownList DDL, string sp, string value, string text)
    {
        SqlConnection conn = null;
        try
        {
            SqlDataReader dr = null;
            Hashtable htParams = new Hashtable();
            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
            string con = ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString;
            conn = new SqlConnection(con);
            ListItem list = null;
            DDL.Items.Clear();
            list = new ListItem();
            list.Text = "Select";
            list.Value = "-1";
            DDL.Items.Add(list);
            dr = DBOperations.ExecuteReader(conn, htParams, sp);
            while (dr.Read())
            {
                list = new ListItem();
                list.Value = dr[value].ToString();
                list.Text = dr[text].ToString();
                DDL.Items.Add(list);
            }
        }
        catch (Exception ex)
        {
        }
    }
    public static void FillDDL(DropDownList DDL,Hashtable htParam, string sp, string value, string text)
    {
        try
        {
            SqlDataReader dr = null;
           // Hashtable htParams = new Hashtable();
            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AutoPlusDBConnectionString"].ConnectionString);
            ListItem list = null;
            DDL.Items.Clear();
            list = new ListItem();
            list.Text = "Select";
            list.Value = "-1";
            DDL.Items.Add(list);
            dr = DBOperations.ExecuteReader( htParam, sp);
            while (dr.Read())
            {
                list = new ListItem();
                list.Value = dr[value].ToString();
                list.Text = dr[text].ToString();
                DDL.Items.Add(list);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public  string Decrypt(string cipherText)
    {

        //string EncryptionKey = "MAKV2SPBNI99212";
        //byte[] cipherBytes = Convert.FromBase64String(cipherText);
        //using (Aes encryptor = Aes.Create())
        //{
        //    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
        //    encryptor.Key = pdb.GetBytes(32);
        //    encryptor.IV = pdb.GetBytes(16);
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
        //        {
        //            cs.Write(cipherBytes, 0, cipherBytes.Length);
        //            cs.Close();
        //        }
        //        cipherText = Encoding.Unicode.GetString(ms.ToArray());
        //    }
        //}
        return cipherText;
    }
}