﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdminLogin.aspx.cs" Inherits="AdminLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <br />
      
        <div class="col s12 m12 l12">
            
            
            <div class="container">
                <br />
                <div class="row">
                    <div class="col s12 m10 l10 offset-l1 z-depth-3">
                          <h5 class="center">
            Login
        </h5>
                        <div class="row">
                            <div class="input-field col s12 m10 l10 offset-l1">
                                 <asp:Label CssClass="control-label" For="txtAdminEmail" ID="lblAdminEmail" runat="server" Text="Email"></asp:Label>
                                  <asp:TextBox ID="txtAdminEmail" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m10 l10 offset-l1">
                                 <asp:Label CssClass="control-label" For="txtAdminPassword" ID="AdminPassword" runat="server" Text="Password"></asp:Label>
                                <asp:TextBox ID="txtAdminPassword" runat="server" CssClass="form-control" 
                                placeholder="Password" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 l12 center">
                                <asp:Button ID="btnAdminLogin" runat="server" Text="Login" CssClass="btn btn-success" OnClick="btnAdminLogin_Click" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 l12 center">
                                <asp:Label CssClass="control-label"  ID="lblloginfailure" runat="server" Text="Invalid Lodig Id or Password" Visible="False" ForeColor="Red"></asp:Label>   
                            </div>
                        </div>
                    </div>
                    </div>
                
            </div>
        </div>
    </div>
</asp:Content>

