﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="Inquiry.aspx.cs" Inherits="Inquiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            var table = $('[id*=gvInquiry]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
            "responsive": true,
            "sPaginationType": "full_numbers",
            dom: '<"row"<"col s6 m4 l4 left"l><"col s12 m4 l4 center"B><"col s6 m4 l4"f>><"row"<"col s12 m12 l12"<"tblData" t>>>ip',
            
            responsive: false,
            "oLanguage": {
                "sLengthMenu": "Display records  _MENU_"
  },
            buttons: {
                buttons: [
            { extend: 'pdf', className: 'btn' },
            { extend: 'excel', className: 'excelButton btn' },
             { extend: 'print', className: 'PrintButton btn' }

                        ]             
            },
            "columnDefs": [
             { "width": "1%", "targets": 0 },
             { "width": "1%", "targets": 1 },
             { "width": "8%", "targets": 2 },
             { "width": "1%", "targets": 3 },
             { "width": "1%", "targets": 4 },
             { "width": "1%", "targets": 5 },
             { "width": "1%", "targets": 6 },
             { "width": "1%", "targets": 7 },
             { "width": "1%", "targets": 8 },
             { "width": "1%", "targets": 9 },
             { "width": "1%", "targets": 10 }
             ],
            "order": [[1, 'asc']]

        });
        table.on('order.dt search.dt', function () {
            table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

    });

          
  
    </script>
   
    <style type="text/css">
        td, th
        {
            text-align: center;
        }
           
             /*css for length (display records) start*/
     
 
	.dataTables_length select
	{
	    display:block !important;
	    
	    }

    </style>
    <div class="row">
        <div class="col s12 m12 l12 center">
            <h5>Client Inquiry 
            </h5>
        </div>
        <div class="col s12 m12 l12">
         
               <%-- <div class="form-group">
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass=" form-control dropdown dropdown-toggle ">
                        <asp:ListItem Value="type">Type</asp:ListItem>
                        <asp:ListItem Value="location">Location</asp:ListItem>
                        <asp:ListItem Value="prop_for">For</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="TxtSearch" runat="server"  ></asp:TextBox>
                    <asp:Button ID="Button1" runat="server" Text="Button" CssClass="btn-info" OnClick="Button1_Click" />
                </div>--%>
               
                    <asp:GridView ID="gvInquiry" runat="server" AutoGenerateColumns="False" CellPadding="4"
                         CssClass="table table-bordered table-condensed table-hover table-responsive" BackColor="White" GridLines="None" Visible="True">
                        <AlternatingRowStyle BackColor="LightGray" />
                        <Columns>
                        <asp:BoundField DataField="" HeaderText="Sr No" />
                            <asp:BoundField DataField="name" HeaderText="Name" />
                            <asp:BoundField DataField="email" HeaderText="Email" />
                            <asp:BoundField DataField="contact" HeaderText="Contact" />
                            <asp:BoundField DataField="type" HeaderText="Type" />
                            <asp:BoundField DataField="prop_for" HeaderText="For" />
                            <asp:BoundField DataField="location" HeaderText="Location" />
                            <asp:BoundField DataField="city" HeaderText="City" />
                            <asp:BoundField DataField="bedrooms" HeaderText="Bedrooms" />
                            <asp:BoundField DataField="bathrooms" HeaderText="Bathrooms" />
                            <asp:BoundField DataField="parking_type" HeaderText="Parking" />
                            <asp:BoundField DataField="Budget" HeaderText="Budget" />
                            <%--<asp:TemplateField HeaderText="Budget" >
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "budget_min")%>-<%# DataBinder.Eval(Container.DataItem, "budget_max")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            --%>
                        </Columns>
                        
                       
                                                                   </asp:GridView>
               
        </div>
    </div>
</asp:Content>
