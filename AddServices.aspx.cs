﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class AddServices : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                this.bindData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    protected void bindData()
    {
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
        }
        String query = "select * from services";
        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
            submitService.Text = "update";
            txtFinance.Text=dr["finance"].ToString();
            txtInterior.Text = dr["interior"].ToString();
            txtNriService.Text = dr["nri"].ToString();

        }
    }
    protected void submitService_Click(object sender, EventArgs e)
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            String query = "";
            if (submitService.Text == "update")
            {
                query = "update services set finance=@finance,nri=@nri,interior=@interior where service_id=1";
            }
            else
            {
                query = "insert into services (finance,interior,nri) values (@finance,@interior,@nri);";
            }
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@finance", txtFinance.Text.ToString());
            cmd.Parameters.AddWithValue("@nri", txtNriService.Text.ToString());
            cmd.Parameters.AddWithValue("@interior", txtInterior.Text.ToString());
            cmd.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("AddServices.aspx");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
     
}