﻿

<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="Home.aspx.cs" Inherits="Home" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script>

        $(document).ready(function () {
            $('#sliderImages').empty();
            var imgDataStr = $('#<%= sliderPreImgData.ClientID %>').val();
            if (imgDataStr != '' && imgDataStr != undefined) {
                var imgDataList = imgDataStr.split("-");
                for (var i = 0; i < imgDataList.length; i++) {
                    var imgDataSingle = imgDataList[i];
                    $('#sliderImages').append('<a class="carousel-item" href="#one!"><img src="' + imgDataSingle + '"></a>');
                }
            }
        });

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        #preview
        {
            position: absolute;
            border: 3px solid #ccc;
            background: #333;
            padding: 5px;
            display: none;
            color: #fff;
            box-shadow: 4px 4px 3px rgba(103, 115, 130, 1);
        }
       .middle-indicator{
   position:absolute;
   top:45%;
   }
  .middle-indicator-text{
   font-size: 4.2rem;
  }
  a.middle-indicator-text{
    color:white !important;
  }
.content-indicator{
    width: 64px;
    height: 64px;
    background: none;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    border-radius: 50px;
     cursor:pointer; 
  }
  .carousel .carousel-item>img {
    height:80vh !important;
}
    </style>
    <script>
        $(document).ready(function () {
            $('.carousel.carousel-slider').carousel(
            {
                fullWidth: true ,
                indicators	:true
             }
            );
        });
    </script>

   <%--slider home page start--%>
        <div class="row">
            <div class="carousel carousel-slider center">
             <div class="carousel-fixed-item center middle-indicator">
     <div class="left">
      <a  class="movePrevCarousel middle-indicator-text waves-effect waves-light content-indicator" onclick="$('.carousel').carousel('prev');"><i class="material-icons left  middle-indicator-text">chevron_left</i></a>
     </div>
     
     <div class="right">
     <a  class="moveNextCarousel middle-indicator-text waves-effect waves-light content-indicator" onclick="$('.carousel').carousel('next');"><i class="material-icons right middle-indicator-text">chevron_right</i></a>
     </div>
    </div>
    <div id="sliderImages">
    <a class="carousel-item" href="#one!"><img src="img/slider2.jpg"></a>
    <a class="carousel-item" href="#two!"><img src="img/slider3.jpg"></a>
    <a class="carousel-item" href="#three!"><img src="img/slider4.jpg"></a>
    </div>
  </div>
        
        </div>
        <%--slider home page end--%>





           
      <%--<img class="img-fluid" src="img/img1.jpg" alt="Slider image 1">
      <img class="img-fluid" src="img/slider2.jpg" alt="Slider image 2">
      <div class="carousel-caption">
    <h2>Welcome to</h2>
    <h4>We had such a great time in LA!</h4>
      <img class="img-fluid" src="img/slider3.jpg" alt="Slider image 3">
      <img class="img-fluid" src="img/slider4.jpg" alt="Slider image 4">--%>
 
   
    
<div class="container">
    <asp:HiddenField ID="sliderPreImgData" runat="server"></asp:HiddenField>
   <%-- <div class="form-control">--%>
    <div class="row">
            <div class="col s12 m12 l12">
                <div class="col s6 m4 l3">
                <label>Location</label>              
                        <asp:DropDownList  ID="ddlLocation" runat="server" CssClass="browser-default">
                            <asp:ListItem>Location</asp:ListItem>
                        </asp:DropDownList>
            
                </div>
                <div class="col s6 m3 l2">
                <label>Property For</label>              
                       <asp:DropDownList ID="ddlPropFor" runat="server" CssClass="browser-default">
                            <asp:ListItem>Property For(Any)</asp:ListItem>
                            <asp:ListItem>Buy</asp:ListItem>
                            <asp:ListItem>Rent</asp:ListItem>
                        </asp:DropDownList>
                </div>
                <div class="col s6 m4 l2">
                <label>Bedrooms</label>     
                    <asp:DropDownList ID="ddlBeds" runat="server" CssClass="browser-default" >
                            <asp:ListItem>Min Beds(Any)</asp:ListItem>
                                    <asp:ListItem> 1RK </asp:ListItem>
                                    <asp:ListItem> 1BHK </asp:ListItem>
                                    <asp:ListItem> 1.5BHK </asp:ListItem>
                                    <asp:ListItem> 2BHK </asp:ListItem>
                                    <asp:ListItem> 3BHK </asp:ListItem>
                                    <asp:ListItem> 4BHK </asp:ListItem>
                                    <asp:ListItem> 5+ </asp:ListItem>
                                     </asp:DropDownList>
                </div>
                <div class="col s6 m4 l2">
                      <label>Property Type</label>     
                      <asp:DropDownList ID="ddlPropertType" runat="server" CssClass="browser-default">
                            <asp:ListItem>Property Type(Any)</asp:ListItem>
                            <asp:ListItem Value="Commercial">-Commercial</asp:ListItem>
                            <asp:ListItem Value="Residencial">-Residential</asp:ListItem>                       
                        </asp:DropDownList>
                </div>
                <div class="col s6 m3 l2 input-field">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn"  OnClick="btnSearch_Click"/>
                </div>
                <div class="col s6 m4 l1 input-field">
                    <asp:Button ID="allProp" runat="server" Text="All Property" 
                        CssClass="btn" onclick="allProp_Click"/>
                </div>
            </div>
           
                
                </div>
                <%--</div>--%>
             
                
               
                <br />
               
                
       
        <div class="row">
            <div style="width: 100%;">
                <%-- <table style="width: 100%; border: 1px solid black; padding: 25px; margin-top: 15px; height: 170px; vertical-align: top" class="Modelpupup">--%>
                            
                   <%-- <colgroup>
                        <col style="width: 90%"/>
                        <col style="width: 10%" />
                    </colgroup>--%>
                   <%-- <tr>
                        <td align="center">
                            <asp:Repeater ID="rptAlphabets" runat="server">
                                <ItemTemplate>
                                    <span style="margin-left: 5px">
                                        <asp:LinkButton ID="lnkAlphabet" runat="server" Text='<%#Eval("Value")%>'
                                            Enabled='<%# Eval("isNotSelected")%>' /></span>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>--%>
                    <%--<tr>

                        <td style="vertical-align: top; width: 100%; margin-left: 40px;" align="center">--%> 
                <div class="row">
                    <div class="col s12 m12 l12"> 
                        <h5><asp:Label ID="lblView" runat="server" Text="Featured Property" /></h5>
                    </div>
                    
                            <%--<asp:DataList ID="dlstProperty" runat="server"
                                BorderStyle="None"  CellPadding="3" CellSpacing="5" 
                                Font-Size="Small" RepeatColumns="4" RepeatDirection="Horizontal"
                                Width="100%" Height="200px">
                                <ItemStyle HorizontalAlign="left" />--%>
                                 <asp:Repeater ID="dlstProperty" runat="server">
                                <ItemTemplate>
                                   <%-- <div class="row">
                                        <div class="col l3 m3 s3">--%>
                                            
                                            <div class="col s6 m4 l3">
                                            <div class="card propCard">
                                                <div class="card-image">
                                                      <asp:HyperLink ID="HyperLink1" class="preview" NavigateUrl='<%# Bind("images") %>' runat="server">
                                                   <asp:ImageButton Width="100%" CssClass="card-img-top"  Height="180px" ID="imgProperty" OnClick="imgProperty_Click" CommandArgument='<%# Eval("Prop_id") %>' ImageUrl='<%# Eval("images") %>' runat="server" />
                                              </asp:HyperLink>
                                            
                                                </div>
                                            <%--<img class="card-img-top" src="img_avatar1.png" alt="Card image">--%>
                                                  <div class="card-content">
                                                    <h4 class="card-title"><asp:Label ID="lblProp" runat="server" Text='<%# Bind("prop") %>'></asp:Label></h4>
                                                    <h6 class="card-text truncate"><b>Location:</b><asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location") %>'></asp:Label></h6>
                                                    <h6 class="card-text truncate"><b>Price:&#8377;</b><asp:Label ID="lblPrice" runat="server" Text=' <%# Bind("price") %>'></asp:Label></asp:Label></h6>
                                                    
                                                  </div>
                                             </div>
                                             </div>  
                                               
                           
                                            <%--<div style=" height: 35px; font-size: 12px;">
                                                <h6><b>Location:</b></h6>
                                                <h6><asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location") %>'></asp:Label></h6>
                                                <h6><b>Price:</b></h6>
                                                <h6><asp:Label ID="lblPrice" runat="server" Text=' <%# Bind("price") %>'></asp:Label></h6>
                                            </div>--%>
                                             
                                        <%--</div>
                                    </div>--%>
                                </ItemTemplate>
                                <%--<SelectedItemStyle  Font-Bold="True" ForeColor="White" />--%>
                                </asp:Repeater>
                           <%-- </asp:DataList>--%>
                </div>
                <div class="row">
                 <div class="col s12 m12 l12"> 
                        <h5><asp:Label ID="lblprojects" runat="server" Text="Featured Project" /></h5>
                    </div>
                    <asp:Repeater ID="rptPeoject" runat="server">
                                <ItemTemplate>
                                   <%-- <div class="row">
                                        <div class="col l3 m3 s3">--%>
                                            
                                            <div class="col s6 m4 l3">
                                            <div class="card propCard">
                                                <div class="card-image">
                                                      <asp:HyperLink ID="HyperLink1" class="preview" NavigateUrl='<%# Bind("images") %>' runat="server">
                                                   <asp:ImageButton Width="100%" CssClass="card-img-top"  Height="180px" ID="imgProperty" OnClick="imgProperty_Click" CommandArgument='<%# Eval("Prop_id") %>' ImageUrl='<%# Eval("images") %>' runat="server" />
                                              </asp:HyperLink>
                                            
                                                </div>
                                            <%--<img class="card-img-top" src="img_avatar1.png" alt="Card image">--%>
                                                  <div class="card-content">
                                                    <h4 class="card-title"><asp:Label ID="lblProp" runat="server" Text='<%# Bind("prop") %>'></asp:Label></h4>
                                                    <h6 class="card-text"><b>Location:</b><asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location") %>'></asp:Label></h6>
                                                    <h6 class="card-text"><b>Price:</b><asp:Label ID="lblPrice" runat="server" Text=' <%# Bind("price") %>'></asp:Label></asp:Label></h6>
                                                    
                                                  </div>
                                             </div>
                                             </div>  
                                               
                           
                                            <%--<div style=" height: 35px; font-size: 12px;">
                                                <h6><b>Location:</b></h6>
                                                <h6><asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location") %>'></asp:Label></h6>
                                                <h6><b>Price:</b></h6>
                                                <h6><asp:Label ID="lblPrice" runat="server" Text=' <%# Bind("price") %>'></asp:Label></h6>
                                            </div>--%>
                                             
                                        <%--</div>
                                    </div>--%>
                                </ItemTemplate>
                                <%--<SelectedItemStyle  Font-Bold="True" ForeColor="White" />--%>
                                </asp:Repeater>
                </div>
                      <%--  </td>
                    </tr>
                </table>--%>
            </div>            
        </div>
    </div>
                               

        
    <script type="text/javascript">
        $(document).ready(function () {
            ShowImagePreview();
        });
        // Configuration of the x and y offsets
        function ShowImagePreview() {
            xOffset = -20;
            yOffset = 40;

            $("a.preview").hover(function (e) {
                this.t = this.title;
                this.title = "";
                var c = (this.t != "") ? "<br/>" + this.t : "";
                $("body").append("<p id='preview'><img src='" + this.href + "' height='250' width='350' alt='Image preview'/>" + c + "</p>");
                $("#preview")
                .css("top", (e.pageY - xOffset) + "px")
                .css("left", (e.pageX + yOffset) + "px")
                .fadeIn("slow");
            },

            function () {
                this.title = this.t;
                $("#preview").remove();
            });

            $("a.preview").mousemove(function (e) {
                $("#preview")
                .css("top", (e.pageY - xOffset) + "px")
                .css("left", (e.pageX + yOffset) + "px");
            });
            $("a.preview").click(function (e) {
                $("#preview").fadeOut("fast");

            });
        };
    </script>
    <script type="text/javascript">
        function show() {
            location.reload(true);
        }
    </script>
</asp:Content>
