﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NriCorner.aspx.cs" Inherits="NriCorner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1 class="text-center">NRI Corner...
    </h1>
<div class="col-md-offset-1 col-md-10">
<div class="jumbotron"> 

       
        
        <img src="img/nriservices.jpg" width="100%" height="250p"/>
        
       <hr />        <div id="project-content-div2" >
                 <h3> <strong> WELCOME TO HOME AWAY FROM HOME, YOUR SECOND HOME IN MUMBAI</strong></h3><br /><hr />
                   <p>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"Propshop Realty" as a leading Residential Real Estate Developer in Mumbai, having 
                conducted operations for more than 50 years,  Propshop Realty has predominantly 
                concentrated on developing residential houses in and around the suburbs of Mumbai. </p>
					 <p>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp We have had the distinction of constructing upwards of 16 Housing Societies and 23 
                    Housing Societies are under Propshop Upgrade, consisting of both commercial & residential 
                    buildings, some of which are now known landmarks of Mumbai suburbs. </p>
                    <p>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp At Propshop Realty buyers are assured of transparency in Carpet Area Disclosure and Pricing,
                 with Full Cheque Payments, and on schedule delivery.</p>
				 
				
             </div>
            
			<hr /> 
			
			 
             <h2><strong> The tallest thing we ever built was our reputation.</strong></h2><hr />
                 <p>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp We have a focused dedicated sales team which offers real estate solutions 
                to our prestigious NRI clients. The team reaches out to NRIs in various regions 
                by way of exhibitions, seminars and fulfils sales through our dedicated project sales 
                team. We understand the requirements of our NRI clients and provide real estate solutions which fit their needs.</p>
                 <p> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Below are few frequently asked questions for NRI. for any further information or an enquiry please 
                <b>Contact Us - +91 98199316013  </b> </p>
				
              
                <div class="accordian col-md-offset-2" id="accordion" >
               		<a data-toggle="collapse" data-parent="#accordion" data-target="#demo"><i class="fa fa-arrow-right "></i> Who is a Non-Resident Indians (NRI) ?</a>
                    <div class="collapse" id="demo">
                    	<p> An Indian citizen who stays abroad for employment/carrying on business or vocation 
                        outside India or stays abroad under circumstances indicating an intention for an uncertain 
                        duration of stay abroad is a non-resident. (Persons posted in U.N. organisations and officials
                         deputed abroad by Central/State Governments and Public Sector undertakings on temporary assignments
                          are also treated as non-temporary assignments are also treated as non-residents). 
                          Non-resident foreign citizens of Indian origin are treated on par with non- resident Indian citizens (NRIs).
                        </p>
                    </div>
                    
                 <br />
                 <br />   
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#demo1"><i class="fa fa-arrow-right "></i>Who Is A PIO ?</a>
                    <div class="collapse" id="demo1">
                    	<p>A person of Indian origin means an individual (not being a citizen of Pakistan or Bangladesh 
                        or Sri Lanka or Afghanistan or China or Iran or Nepal or Bhutan) who:</p>
                        <ul class="inside-bullet">
                        	<li> Held an Indian Passport at any time, or </li>
                            <li> Who or whose father or paternal grand father was a citizen of India by virtue of the Constitution of India or the Citizenship Act, 1955</li>
                        </ul>
                    </div>                 <br />
                 <br />   
                 
                    
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#demo2"><i class="fa fa-arrow-right "></i>What are the various facilities available to NRI ?</a>
                    <div class="collapse" id="demo2">
                    	<p> NRIs are granted the following facilities: </p>
                        <ul class="inside-bullet">
                        	<li> Maintenance of bank accounts in India </li>
                            <li> Investments in securities/shares of, and deposits with, Indian firms/companies </li>
                            <li> Investments in immovable properties in India </li>
                        </ul>
                    </div>
                 <br />
                 <br />   
                   
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#demo3"> <i class="fa fa-arrow-right "></i>Who Can Purchase Immovable Property In India ? </a>
                    <div class="collapse" id="demo3">
                    	<ul class="inside-bullet"> 
                        <li>Under the general permission available, the following categories can freely purchase immovable property in India:
                        <br /> <br /> 
                        <p> i) Non-Resident Indian (NRI) - that is a citizen of India resident outside India <br />
						ii) Person of Indian Origin (PIO) - that is an individual (not being a citizen of Pakistan 
                        or Bangladesh or Sri Lanka or Afghanistan or China or Iran or Nepal or Bhutan), who</p>
                       </li>
                        <li> At any time, held Indian passport, or </li>
                        <li> Who or either of whose father or grandfather was a citizen of India by virtue 
                        of the Constitution of India or the Citizenship Act, 1955 (57 of 1955).</li>
                         </ul>
                         <p> The general permission, however, covers only purchase of residential and commercial property. </p>
                    </div>
                   
                 <br />
                 <br />   
                    
                    
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#demo4"><i class="fa fa-arrow-right "></i> Can NRIs sell the properties they hold in India without Reserve Bank's Permission ? </a>
                    <div class="collapse" id="demo4"> Yes </div>
                 <br />
                 <br />   
                    
                    <a data-toggle="collapse" data-target="#demo5"><i class="fa fa-arrow-right "></i> Can NRIs Acquire or dispose residential property by way of Gift </a>
                    <div class="collapse" id="demo5"> 
                    <p> Yes, the Reserve Bank has granted general permission to NRIs to acquire 
                    or dispose of NRI India Properties by way of gift from or to a relative who may be an Indian 
                    citizen or a person of Indian origin (PIO) whether resident in India or not. </p>
                    </div>
                 <br />
                 <br />   
                   
                    
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#demo6"><i class="fa fa-arrow-right "></i> Can NRI Obtain loans for acquisition of a house / flat for Residential 
                    purpose from Financial Institution Providing Housing Finance ? </a>
                    <div class="collapse" id="demo6"> 
                    <p>The Reserve Bank has granted some general permission to certain 
                    financial institutions providing housing finance e.g. HDFC, LIC Housing Finance Ltd., 
                    etc, and authorized dealers to grant housing loans to NRI nationals for acquisition of 
                    a NRI house/flat for self-occupation subject to certain conditions. Criteria regarding 
                    the purpose of the loan, margin money and the quantum of loan will be at par with those 
                    applicable to resident Indians. Repayment of the loan should be made within a period not 
                    exceeding 15 years, out of inward remittance through banking channels or out of funds
                    held in the investors' NRE/FCNR/NRO accounts. </p>  </div>
                <br />
                 <br />   
                 
                    
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#demo7"><i class="fa fa-arrow-right "></i> What is Power of Attorney ? </a>
                    <div class="collapse" id="demo7"> 
                    	<ul class="inside-bullet">
                        	<li> A Power Of Attorney (POA) or letter of attorney is a written authorization to 
                            represent or act on another's behalf in private affairs, business, or some other 
                            legal matter, sometimes against the wishes of the other. The person authorizing the
                             other to act is the principal, grantor, or donor (of the power). </li>
                             
                             <li> A Power Of Attorney is not an instrument of transfer in regard to any right, 
                             title or interest in an immovable property.</li>
                             <li> A Power Of Attorney, or letter of attorney, is a document that authorizes another person, 
                             known as the agent or attorney-in-fact—usually a legally competent relative or close friend over 
                             18 years old—to handle any combination of financial, legal and health care decisions. A power 
                             of attorney is also referred to as a POA. Generally, one chooses a POA as a provision if he 
                             or she becomes incapacitated.</li>
                        </ul>
                        <p> <b>Types of Power of Attorney: </b> </p>
                        <ul class="inside-bullet">
                        	<li> A Power Of Attorney (POA) is an instrument that is used by people to confer authority on
                             somebody else to legally act on their behalf. POA are of two types. </li>
                             
                             <li> Special Power of Attorney (SPA), while an SPA is used for transfer of a specific right 
                             to the person on whom it is conferred.</li>
                             
                             <li> General Power of Attorney (GPA), the GPA authorizes the holder to do whatever is necessary. </li>
                             <li> There is no sale clause of immovable property mentioned in POA (notarized) </li>
                             <li> Registered POA from registration office allows sale clause and POA to any one </li>
                        </ul>
                        
                        <p> <b> Following are the important things to be kept in mind while executing the POA </b></p>
                        <ul class="inside-bullet"> 
                        	<li> Customer Prepares POA as per defined format. </li>
                            <li> Executants has to paste his/her photograph along with signature on each page. </li>
                            <li> Authenticate/adjudicate the POA from Indian Embassy or local authority. </li>
                            <li> Send authenticated/adjudicated POA in India. </li>
                            <li> In India, the POA holder has to paste his/her photograph along with his/her left hand thumb 
                            impression and signature. </li>
                            <li> Then this document will have to be stamped for Rs. 500/- (ESBTR, Franking, Stamp paper) 
                            and notarised from a Registered Notary. Please ensure that a stamp of “Before Me” 
                            is affixed on the document. </li>
                            <li> POA holder and executants Photo ID attach before Notary. </li>
                        </ul>
                     </div>
                 <br />
                 <br />   
                   
                   
                    <a data-toggle="collapse" data-target="#demo8" data-parent="#accordion" > <i class="fa fa-arrow-right "></i>What is loan sanction process and it's documentation ? </a>
                    <div class="collapse" id="demo8"> 
                    	<p> The documentation required to be submitted by the NRIs are different from the Resident 
                        Indians as they are required to submit additional documents, like copy of the passport and 
                        a copy of the works contract, etc. and of course NRIs have to follow certain eligibility 
                        criteria in order to get Home Loans in India.</p>
                        <p> Another vital document required while processing an NRI home loan is the power of attorney 
                        (POA). The POA is important because, since the borrower is not based in India; the Home Finance
                         Company would need a 'representative' 'in lieu of' the NRI to deal with and if needed. Although 
                         not obligatory, the POA is usually drawn on the NRI's parents/wife/children/ close 
                         relatives or friends. </p>
                         <p>  <b> The documents needed for obtaining NRI home loans are Bank specific. 
                         General list of documents are as mentioned below: </b> </p>
                         <ul class="inside-bullet"> 
                         	<li> Passport and Visa </li>
                            <li> A copy of the appointment letter and contract from the company employing the applicant. </li>
                            <li> The labour card/identity card (translated in English and countersigned by the consulate) 
                            if the person is employed in the Middle East Salary certificate (in English) specifying name, 
                            date of joining, designation and salary details.</li>
                            <li> Bank Statements for the last six months </li>
                         </ul>
                         
                         <p> <b>List of Classified documents for Salaried and Self Employed NRI Applicants. Banks may have 
                         specific requirements apart from the below listed documents. </b> </p>
                         <p> <b>Salaried NRI Applicants: </b> </p>
                         <ul class="inside-bullet">
                         	<li> Copy of valid passport showing VISA stamps </li>
                            <li> Copy of valid visa / work permit / equivalent document supporting the NRI status of the proposed account holder </li>
                            <li> Overseas Bank A/C for the last 3 months showing salary credits </li>
                            <li> Latest contract copy evidencing Salary / Salary Certificate / Wage Slips </li>
                         </ul>
                         
                         <p> <b> Self-Employed NRI Applicants: </b> </p>
                         <ul class="inside-bullet">
                         	 <li> Passport copy with valid visa stamp </li>
<li> Brief profile of the applicant and business/ Trade license or equivalent document </li>
<li> 6 months overseas bank account statement and NRE/ NRO account </li>
<li> Computation of income, P&L account and B/Sheet for last 3 years
 certified by the C.A. / CPA or any other relevant authority as the case may be (or equivalent company accounts)</li>
                         </ul>
                    </div>
                    
                    
               </div>
 
           	</div>
            </div>
            </div>
</asp:Content>

