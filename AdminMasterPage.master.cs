﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin_id"] == null)
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
              
        }

    }

    protected void btnAdminlogout_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("AdminLogin.aspx");

    }

}
