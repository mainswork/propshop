﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class MasterPage : System.Web.UI.MasterPage
    {
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        bindData();
        if (Session["cust_id"] != null)
        {
            //li_login.style.add("display", "none");
           // btnlogin.Visible = false;
           // btnlogout.Visible = true;
           //hplFav.Visible = true;
            
        }
        else
        {
          // li_login.style.add("display", "inherit");
          //btnlogout.Visible = false;


        } 
    }

    protected void bindData()
    {
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
        }
 
        /*Management team*/
        String query1 = "select * from about_us inner join contact_details on 1=1";
        SqlCommand cmd1 = new SqlCommand(query1, conn);
        SqlDataReader dr1 = cmd1.ExecuteReader();
        if (dr1.Read())
        {
            contactNav.Text = dr1["mob_no"].ToString();
           contactFooter.Text = dr1["mob_no"].ToString();
           lblAddress.Text = dr1["co_address"].ToString();
           lblEmail.Text = dr1["email_id"].ToString();

        }

        
        dr1.Close();

        String query2 = "select COUNT(Prop_id) as 'project_count' from Property where prop='Project'";
        SqlCommand cmd2 = new SqlCommand(query2, conn);
        SqlDataReader dr2 = cmd2.ExecuteReader();
        if (dr2.Read()){
            lblProject.Text = dr2["project_count"].ToString();
         }

        dr2.Close();

        query2 = "select COUNT(Prop_id) as 'prop_rent' from Property where prop='Property' and prop_for='Rent'";
        cmd2 = new SqlCommand(query2, conn);
        dr2 = cmd2.ExecuteReader();
        if (dr2.Read())
        {
            lblPropSale.Text = dr2["prop_rent"].ToString();
        }

        dr2.Close();

        query2 = "select COUNT(Prop_id) as prop_sell from Property where prop='Property' and prop_for='Sell'";
        cmd2 = new SqlCommand(query2, conn);
        dr2 = cmd2.ExecuteReader();
        if (dr2.Read())
        {
            lblPropRent.Text = dr2["prop_sell"].ToString();
        }

        dr2.Close();
        conn.Close();

    }
    //protected void btnlogout_Click(object sender, EventArgs e)
    //{
    //    Session.RemoveAll();
    //    Response.Redirect("Login1.aspx");

    //}

    //protected void btnlogin_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("Login1.aspx");
    //}

    protected void lbAdminLogin_Click(object sender, EventArgs e)
    {
        Response.Redirect("AdminLogin.aspx");
    }

   
}

    

