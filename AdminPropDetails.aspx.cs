﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class AdminPropDetails : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                this.BindData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    private void BindData()
    {
        try
        {
            int prop1 = Convert.ToInt32(Session["propid"]);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            /*slider imges*/
            String imgDataListOld = "";
            String query2 = "select * from property_imgages where property_id=" + prop1;
            SqlCommand cmd2 = new SqlCommand(query2, conn);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            while (dr2.Read())
            {
                imgDataListOld += dr2["img_base_64"].ToString() + "-";
            }

            if (imgDataListOld.Length > 0)
            {
                imgDataListOld = imgDataListOld.Substring(0, imgDataListOld.Length - 1);
            }
            sliderPreImgData.Value = imgDataListOld;
            /*slider images*/
            dr2.Close();

            string query = "select Prop_id,Bhk,Bathroom,types1,prop_for,price,images,Area,Comp_id from Property where Prop_id=" + prop1;
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[7] {
                new DataColumn("Prop_id"),
                new DataColumn("Bhk"), 
                 new DataColumn("Bathroom"),
                new DataColumn("types1"),
                new DataColumn("prop_for"),
                new DataColumn("price"),
                new DataColumn("Area")
            });
            if (dr.Read())
            {
                int cmp = Convert.ToInt32(dr["Comp_id"]);
                ViewState["Compid"] = cmp;
                lblArea.Text = dr["Area"].ToString();
                lblBathrooms.Text = dr["Bathroom"].ToString();
                lblBhk.Text = dr["Bhk"].ToString();
                lblPrice.Text = dr["price"].ToString();
                lblPropFor.Text = dr["prop_for"].ToString();
                lblPropis.Text = dr["types1"].ToString();
                dt.Rows.Add(dr["Prop_id"].ToString(), dr["Bhk"].ToString(), dr["Bathroom"].ToString(), dr["types1"].ToString(), dr["prop_for"].ToString(), dr["price"].ToString(), dr["Area"].ToString());
                ViewState["dtSelectProp"] = dt;

            }
            dr.Close();
            //DataSet ds = new DataSet();
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //da.Fill(ds);
            //Session["dtSelectProp"] = ds.Tables[0];
            //fvProperty.DataSource = ds.Tables[0];
            //fvProperty.DataBind();

            int comp = Convert.ToInt32(ViewState["Compid"]);
            string query1 = "select comp_name,location,city,state,pincode,Latitude, Longitude,address1 from complex where Comp_id=" + comp;
            cmd = new SqlCommand(query1, conn);
            SqlDataReader dr1 = cmd.ExecuteReader();
            DataTable dt1 = new DataTable();
            dt1.Columns.AddRange(new DataColumn[6] {
                new DataColumn("comp_name"),
                new DataColumn("location"), 
                new DataColumn("city"),
                new DataColumn("state"),
                new DataColumn("pincode"),
                new DataColumn("address1")
            });
            if (dr1.Read())
            {
                lblcompname.Text = dr1["comp_name"].ToString();
                lblCity.Text = dr1["city"].ToString();
                lbllocation.Text = dr1["location"].ToString();
                lblpin.Text = dr1["pincode"].ToString();
                lblSate.Text = dr1["state"].ToString();
                dt1.Rows.Add(dr1["comp_name"].ToString(), dr1["location"].ToString(), dr1["city"].ToString(), dr1["state"].ToString(), dr1["pincode"].ToString(), dr1["address1"].ToString());
                ViewState["dtSelectComp"] = dt1;
            }
            dr1.Close();
            //DataSet dts = new DataSet();
            //SqlDataAdapter dta = new SqlDataAdapter(cmd);
            //dta.Fill(dts);
            //Session["dtSelectComp"] = dts.Tables[0];
            //fvlocation.DataSource = dts.Tables[0];
            //fvlocation.DataBind();
            //rptMarkers.DataSource = dts.Tables[0];
            //rptMarkers.DataBind();

            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}