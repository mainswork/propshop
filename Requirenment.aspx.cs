﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Requirenment : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        resetForm();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            String query = "insert into [property_inquiry](name,email,contact,prop_for,type,bedrooms,bathrooms,city,location,budget_min,budget_max) values (@name,@email,@contact,@for,@type,@bedrooms,@bathrooms,@city,@location,@min,@max);";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@contact", txtMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@for",ddlRequirement.SelectedValue);
            cmd.Parameters.AddWithValue("@type",ddlPropType.SelectedValue);
            cmd.Parameters.AddWithValue("@bedrooms",ddlBedrooms.SelectedValue);
            cmd.Parameters.AddWithValue("@bathrooms",ddlBathroom.SelectedValue);
            cmd.Parameters.AddWithValue("@city",txtCity.Text.Trim());
            cmd.Parameters.AddWithValue("@location",txtLocation.Text.Trim());
            cmd.Parameters.AddWithValue("@min",txtMin.Text.Trim());
            cmd.Parameters.AddWithValue("@max",txtMax.Text.Trim());
            //cmd.Parameters.AddWithValue("@parking",ddlParking.SelectedValue);
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            resetForm();
        }
        catch (Exception ex)
        {

        }
    }
    protected void resetForm() {
        txtCity.Text = "";
        txtEmail.Text = "";
        txtLocation.Text = "";
        txtMin.Text = "";
        txtMax.Text = "";
        txtMobileNo.Text = "";
        txtName.Text = "";
        ddlBathroom.SelectedIndex = 0;
        ddlPropType.SelectedIndex = 0;
        ddlBedrooms.SelectedIndex = 0;
        ddlRequirement.SelectedIndex = 0;

    }
    
}