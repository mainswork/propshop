﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Forgotpassword.aspx.cs" Inherits="Forgot_password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-7">
        

            <div class="form-group">
                <div class="col-md-3">
                    <asp:Label CssClass="control-label" For="txtEmail" ID="lblEmail" runat="server" Text="Email"></asp:Label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <asp:Label CssClass="control-label" For="ddlSecurity" ID="lblSecurityQ" runat="server"
                    Text="Select Security Question"></asp:Label><br />
                <div class="col-lg-6">
                    <asp:DropDownList ID="ddlSecurity" runat="server" CssClass="form-control">
                        <asp:ListItem>What is your pet name?</asp:ListItem>
                        <asp:ListItem>Who is your favourite teacher?</asp:ListItem>
                        <asp:ListItem>Who is your favourite subject?</asp:ListItem>
                        <asp:ListItem>Who is your First Mobile No.?</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-lg-6">
                    <asp:TextBox ID="txtAns" runat="server" CssClass="form-control" placeholder="Answer"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvAns" runat="server" ErrorMessage="Answer is Mandatory"
                        Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Answer is Mandatory"
                        ValidationGroup="vgEmployee" ControlToValidate="txtAns"></asp:RequiredFieldValidator>
                </div>
                <asp:Button ID="btndone" runat="server" Text="Done" OnClick="btndone_Click" />
                 <asp:Button ID="btnVerify" runat="server" Text="Verfiy" OnClick="btnverify_Click" />
            </div>
        
    </div>
</asp:Content>
