﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BuyCommercial.aspx.cs" Inherits="BuyCommercial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<style>
        #preview
        {
            position: absolute;
            border: 3px solid #ccc;
            background: #333;
            padding: 5px;
            display: none;
            color: #fff;
            box-shadow: 4px 4px 3px rgba(103, 115, 130, 1);
        }
</style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.num').keypress(function (event) {
                var key = event.which;

                if (!(key >= 48 && key <= 57 || key === 13))
                    event.preventDefault();
            });
        });
    </script>
    <div class="container1">
      <br /><br />
       
        <div class="row">
          <div class="col s12 m12 l12 center"> 
           
                            <h4><asp:Label ID="lblView" runat="server" Text="" /></h4>
                            </div>
            <div class="col s12 m3 l2 z-depth-3">
                <div class="row">
                    <div class="col s12 m12 l12">
                    <h5>Filter<asp:LinkButton runat="server" ID="btnReset" Text="<i class='fa fa-refresh'></i>" 
                 CssClass="btn-flat right" OnClick="btnReset_Click" />
                    </h5>
                </div>
                
                </div>
                <div class="row">
                 <div class="col s12 m12 l12">
                    <label>Location</label>              
                        <asp:DropDownList  ID="ddlLocation" runat="server" CssClass="browser-default">
                            <asp:ListItem>Location</asp:ListItem>
                        </asp:DropDownList>
                </div>
               
                </div>
                 <div class="row">
                <div class="col s12 m12 l12">
                 <label>Property For</label>              
                       <asp:DropDownList ID="ddlPropFor" runat="server" CssClass="browser-default">
                            <asp:ListItem>Property For(Any)</asp:ListItem>
                            <asp:ListItem>Buy</asp:ListItem>
                            <asp:ListItem>Rent</asp:ListItem>
                        </asp:DropDownList>
                </div>
                </div>
                 <div class="row">
                <div class="col s12 m12 l12">
                <label>Bedrooms</label>     
                    <asp:DropDownList ID="ddlBeds" runat="server" CssClass="browser-default" >
                            <asp:ListItem>Min Beds(Any)</asp:ListItem>
                                    <asp:ListItem> 1RK </asp:ListItem>
                                    <asp:ListItem> 1BHK </asp:ListItem>
                                    <asp:ListItem> 1.5BHK </asp:ListItem>
                                    <asp:ListItem> 2BHK </asp:ListItem>
                                    <asp:ListItem> 3BHK </asp:ListItem>
                                    <asp:ListItem> 4BHK </asp:ListItem>
                                    <asp:ListItem> 5+ </asp:ListItem>
                                     </asp:DropDownList>
                </div>
                </div>
                 <%--<div class="row">
                <div class="col s12 m12 l12">
                      <label>Property Type</label>     
                      <asp:DropDownList ID="ddlPropertType" runat="server" CssClass="browser-default">
                            <asp:ListItem>Property Type(Any)</asp:ListItem>
                            <asp:ListItem Value="Commercial">-Commercial</asp:ListItem>
                            <asp:ListItem Value="Residencial">-Residential</asp:ListItem>                       
                        </asp:DropDownList>
                </div>
                </div>--%>
                <div class="row">
                <div class="input-field col s12 m12 l12">
                    <h6>
                        Budget
                    </h6>
                                 </div>
                <div class="input-field col s12 m12 l12">
                      <label for="txtMinPrice">Min</label>  
                       <asp:TextBox ID="txtMinPrice" CssClass="num" runat="server"></asp:TextBox> 
                 
                  </div>
                
                <div class="input-field col s12 m12 l12">
                      <label for="txtMaxPrice">Max</label>   
                    <asp:TextBox ID="txtMaxPrice" CssClass="num" runat="server"></asp:TextBox>
                  
                  </div>
                  <div class="col s12 m12 l12">
                <asp:Label Text=""  CssClass="red-text" ID="lblError1" runat="server" />
                </div>
                </div>
                
                <div class="row">
                
                <div class="col s12 m12 l12 center">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn"  OnClick="btnSearch_Click"/>
                </div>
                </div>
            </div>
            <div class="col s12 m9 l10">
                      <asp:Repeater ID="dlstProperty" runat="server">
                                <ItemTemplate>
                                   <%-- <div class="row">
                                        <div class="col l3 m3 s3">--%>
                                            
                                            <div class="col s6 m4 l3">
                                            <div class="card propCard">
                                                <div class="card-image">
                                                      <asp:HyperLink ID="HyperLink1" class="preview" NavigateUrl='<%# Bind("images") %>' runat="server">
                                                   <asp:ImageButton Width="100%" CssClass="card-img-top"  Height="180px" ID="imgProperty" OnClick="imgProperty_Click" CommandArgument='<%# Eval("Prop_id") %>' ImageUrl='<%# Eval("images") %>' runat="server" />
                                              </asp:HyperLink>
                                            
                                                </div>
                                            <%--<img class="card-img-top" src="img_avatar1.png" alt="Card image">--%>
                                                  <div class="card-content">
                                                    <h4 class="card-title"><asp:Label ID="lblProp" runat="server" Text='<%# Bind("prop") %>'></asp:Label></h4>
                                                    <h6 class="card-text truncate"><b>Location:</b><asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location") %>'></asp:Label></h6>
                                                    <h6 class="card-text truncate"><b>Price:&#8377;</b><asp:Label ID="lblPrice" runat="server" Text=' <%# Bind("price") %>'></asp:Label></asp:Label></h6>
                                                    
                                                  </div>
                                             </div>
                                             </div>  
                                               
                           
                                            <%--<div style=" height: 35px; font-size: 12px;">
                                                <h6><b>Location:</b></h6>
                                                <h6><asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location") %>'></asp:Label></h6>
                                                <h6><b>Price:</b></h6>
                                                <h6><asp:Label ID="lblPrice" runat="server" Text=' <%# Bind("price") %>'></asp:Label></h6>
                                            </div>--%>
                                             
                                        <%--</div>
                                    </div>--%>
                                </ItemTemplate>
                                <%--<SelectedItemStyle  Font-Bold="True" ForeColor="White" />--%>
                                </asp:Repeater>
            </div>
                            
                        
                            <%--<asp:DataList ID="dlstProperty" runat="server"
                                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="5"
                                Font-Size="Small" RepeatColumns="4" RepeatDirection="Horizontal"
                                Width="100%" Height="200px" >
                                <ItemStyle HorizontalAlign="left" />
                                <ItemTemplate>
                                      <div class="row">
                                        <div class="col-lg-12 col-md-12">
                                            <br />
                                            <div class="card bg-light" style="width:250px">
                                            <asp:HyperLink ID="HyperLink1" class="preview" NavigateUrl='<%# Bind("images", "Image/{0}") %>' runat="server">
                                                <asp:ImageButton Width="100%" CssClass="card-img-top"  Height="170px" ID="imgProperty" OnClick="imgProperty_Click" CommandArgument='<%# Eval("Prop_id") %>' ImageUrl='<%# Bind("images", "Image/{0}") %>' runat="server" />
                                           </asp:HyperLink>
                                            
                                                 <div class="card-body">
                                                    <h4 class="card-title"><asp:Label ID="lblProp" runat="server" Text='<%# Bind("Prop") %>'></asp:Label></h4>
                                                    <h6 class="card-text"><b>Location:</b><asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location") %>'></asp:Label></h6>
                                                    <h6 class="card-text"><b>Price:</b><asp:Label ID="lblPrice" runat="server" Text=' <%# Bind("price") %>'></asp:Label></asp:Label></h6>
                                                    
                                                  </div>
                                             </div>
                                             
                                            </div>
                                             
                                        </div>
                         
                                </ItemTemplate>
                                <SelectedItemStyle  Font-Bold="True" ForeColor="White" />
                            </asp:DataList>--%>
                                   </div>            
       
    </div> 
            
    <script type="text/javascript">
        $(document).ready(function () {
            ShowImagePreview();
        });
        // Configuration of the x and y offsets
        function ShowImagePreview() {
            xOffset = -20;
            yOffset = 40;

            $("a.preview").hover(function (e) {
                this.t = this.title;
                this.title = "";
                var c = (this.t != "") ? "<br/>" + this.t : "";
                $("body").append("<p id='preview'><img src='" + this.href + "' height='250' width='350' alt='Image preview'/>" + c + "</p>");
                $("#preview")
                .css("top", (e.pageY - xOffset) + "px")
                .css("left", (e.pageX + yOffset) + "px")
                .fadeIn("slow");
            },

            function () {
                this.title = this.t;
                $("#preview").remove();
            });

            $("a.preview").mousemove(function (e) {
                $("#preview")
                .css("top", (e.pageY - xOffset) + "px")
                .css("left", (e.pageX + yOffset) + "px");
            });
            $("a.preview").click(function (e) {
                $("#preview").fadeOut("fast");

            });
        };
    </script>
    <script type="text/javascript">
        function show() {
            location.reload(true);
        }
    </script>
    
</asp:Content>



