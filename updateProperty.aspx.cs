﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Configuration;

public partial class updateProperty : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {

                string propId = Request.QueryString["propid"];
                ViewState["propId"] = propId;
                Bind_ddlCity();
                this.BindData(propId);
                // this.BindData(prop);
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    public void Bind_ddlCity()
    {
        conn.Open();
        SqlCommand cmd = new SqlCommand("select * from city", conn);

        SqlDataReader dr = cmd.ExecuteReader();
        ddlcity.DataSource = dr;
        ddlcity.Items.Clear();
        ddlcity.Items.Add("Select city");
        ddlcity.DataTextField = "city_name";
        ddlcity.DataValueField = "city_id";
        ddlcity.DataBind();
        dr.Close();
        conn.Close();

    }
    public void Bind_area()
    {
        conn.Open();
        SqlCommand cmd = new SqlCommand("select * from area where city_id ='" + ddlcity.SelectedValue + "'", conn);
        SqlDataReader dr = cmd.ExecuteReader();

        ddlLocation.DataSource = dr;
        ddlLocation.Items.Clear();
        ddlLocation.Items.Add("Select Area");
        ddlLocation.DataTextField = "area_name";
        ddlLocation.DataValueField = "area_id";
        ddlLocation.DataBind();
        dr.Close();
        conn.Close();
    }
    public void bind_pincode()
    {
        conn.Open();
        SqlCommand cmd = new SqlCommand("select * from area where area_id ='" + ddlLocation.SelectedValue + "'", conn);
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
            // txtPincode.Value =Convert.ToString(dr["pincode"]);
            ViewState["pincode"] = dr["pincode"];

        }
        dr.Close();
        conn.Close();
    }
    protected void ddlcity_SelectedIndexChanged(object sender, EventArgs e)
    {
        Bind_area();
    }
    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        bind_pincode();

    }
    private void BindData(string propId)
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            /*property imges*/
            String imgDataList="";
            String query = "select * from property_imgages where property_id = " + propId;
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                imgDataList += dr["img_base_64"].ToString() + "," + dr["img_name"].ToString() + "-";
            }
            imgDataList = imgDataList.Substring(0, imgDataList.Length-1);
            TextBoxImgData.Value = imgDataList;
            /*property images*/

            dr.Close();
            query = "select * from property p JOIN complex c on c.comp_id=p.comp_id where  p.prop_id = " + propId;
            cmd = new SqlCommand(query, conn);
            dr = cmd.ExecuteReader();
             string area="";
            while (dr.Read())
            {
                
                txtCompNameUpdate.Text=dr["comp_name"].ToString();
                var city = dr["city"].ToString().Trim();
                ddlcity.SelectedValue = ddlcity.Items.FindByText(city).Value;

               area = dr["location"].ToString().Trim();
                //dr.Close();

                //SqlCommand cmd1 = new SqlCommand("select * from area where city_id ='" + ddlcity.SelectedValue + "'", conn);
                //SqlDataReader dr1 = cmd.ExecuteReader();

                //ddlLocation.DataSource = dr1;
                //ddlLocation.Items.Clear();
                //ddlLocation.Items.Add("Select Area");
                //ddlLocation.DataTextField = "area_name";
                //ddlLocation.DataValueField = "area_id";
                //ddlLocation.DataBind();

                //var area = dr["location"].ToString().Trim();
                //ddlLocation.SelectedValue = ddlLocation.Items.FindByText(area).Value;
                //ddlLocation.Items.FindByText(area).Selected = true;
                
               
                txtAddress.Text=dr["address1"].ToString();
                txtArea.Text=dr["Area"].ToString();
                txtPrice.Text = dr["price"].ToString();
               
                if (dr["prop"].ToString() == "Project")
                {
                    rbProj.Checked = true;
                }
                else if (dr["prop"].ToString() == "Property") {
                    rbProp.Checked = true;
                }
                
                if(ddlPropType.Items.FindByValue(dr["types1"].ToString()) != null)
                {
                    ddlPropType.Items.FindByValue(dr["types1"].ToString()).Selected = true;
                }
                if (ddlFor.Items.FindByValue(dr["prop_for"].ToString()) != null)
                {
                    ddlFor.Items.FindByValue(dr["prop_for"].ToString()).Selected = true;
                }
                if (ddlBedrooms.Items.FindByValue(dr["Bhk"].ToString()) != null)
                {
                    ddlBedrooms.Items.FindByValue(dr["Bhk"].ToString()).Selected = true;
                }
                if (ddlBathroom.Items.FindByValue(dr["Bathroom"].ToString()) != null)
                {
                    ddlBathroom.Items.FindByValue(dr["Bathroom"].ToString()).Selected = true;
                }
                if (ddlPropertyStatus.Items.FindByValue(dr["prop_status"].ToString()) != null)
                {
                    ddlPropertyStatus.Items.FindByValue(dr["prop_status"].ToString()).Selected = true;
                }
                if (ddlFloor.Items.FindByValue(dr["Floor1"].ToString()) != null)
                {
                    ddlFloor.Items.FindByValue(dr["Floor1"].ToString()).Selected = true;
                }

            }

            dr.Close();
            conn.Close();
             Bind_area();
            ddlLocation.SelectedValue = ddlLocation.Items.FindByText(area).Value;
            ddlLocation_SelectedIndexChanged(null, null);
        }
        catch (Exception ex) 
        {
            throw ex;
        }

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            /*String filename = " ";
            //    string lat_long = getLatLong();
            // string lat = lat_long.Split('#')[0];
            // string lng = lat_long.Split('#')[1];
           
            if (fuImage.HasFile)
            {
                try
                {

                    filename = Path.GetFileName(fuImage.FileName);
                    string path = "~/Image/" + filename;
                    int imgSize = fuImage.PostedFile.ContentLength;
                    if (fuImage.PostedFile != null && fuImage.PostedFile.FileName != "")
                    {
                        if (imgSize > 500)
                        {
                            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "Alert", "alert('image size should be less than 500kb')", true);

                        }
                        else
                        {
                            fuImage.SaveAs(Server.MapPath(path));
                            imgAdmin.ImageUrl = path;

                        }
                    }
                }

                catch (Exception es)
                {
                    throw es;
                }
            }*/

            string prop = " ";
            if (rbProp.Checked)
            {
                prop = rbProp.Text;
            }
            else
            {
                prop = rbProj.Text;
            }

            try
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                int propId = Convert.ToInt16(ViewState["propId"].ToString());
                /*Delete previous and add new img*/
                SqlCommand cmd = new SqlCommand("delete from property_imgages where property_id=" + propId, conn);
                cmd.ExecuteNonQuery();

                String filename = "";
                String imgDataList = TextBoxImgData.Value.Trim();
                String[] imgDataArr=imgDataList.Split('-');
                for (int i = 0; i < imgDataArr.Length; i++){

                    String[] imgData = imgDataArr[i].Split(',');
                    String queryImg = "insert into property_imgages ([img_name],[img_base_64],[property_id]) " +
                    "values (@imgName,@imgBase64,@propertyId)";
                    cmd = new SqlCommand(queryImg, conn);
                    cmd.Parameters.AddWithValue("@imgName", imgData[2]);
                    cmd.Parameters.AddWithValue("@imgBase64", imgData[0] +","+ imgData[1]);
                    cmd.Parameters.AddWithValue("@propertyId", propId);
                    cmd.ExecuteNonQuery();

                    if (filename.Equals("")) {
                        filename = imgData[0] + "," + imgData[1];
                    }
                }

                /*Delete previous and add newimg end*/


                String query2 = "Select Comp_id As compid from Property where Prop_id=" + propId;
                cmd = new SqlCommand(query2, conn);
                cmd = new SqlCommand(query2, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {

                    int compid = Convert.ToInt16(dr["compid"]);
                    ViewState["compid"] = compid;

                }
                dr.Close();
                int compId = Convert.ToInt16(ViewState["compid"].ToString());
                String query = "update [complex] set comp_name=@name,location=@location,address1=@add,city=@city,pincode=@pincode where Comp_id=" + compId;
                // String query = "insert into [complex](comp_name,location,address1,city,state,pincode,Latitude,Longitude) values (@name,@location,@add,@city,@state,@pincode,@lat,@lng);";
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@name", txtCompNameUpdate.Text.Trim());
                cmd.Parameters.AddWithValue("@location", ddlLocation.SelectedItem.Text.Trim());
                cmd.Parameters.AddWithValue("@add", txtAddress.Text.Trim());
                cmd.Parameters.AddWithValue("@city",ddlcity.SelectedItem.Text.Trim());
                //cmd.Parameters.AddWithValue("@state", txtState.Text.Trim());
                cmd.Parameters.AddWithValue("@pincode", ViewState["pincode"].ToString());
                cmd.Parameters.AddWithValue("@lat", 0);
                cmd.Parameters.AddWithValue("@lng", 0);
                cmd.ExecuteNonQuery();
                dr.Close();

                String query1 = "update [property] set Floor1=@floor,Area=@area,Bhk=@bhk,Bathroom=@bathroom,prop=@prop,types1=@type,prop_status=@prop_status,prop_for=@prop_for,price=@price where Prop_id=@propId";
                //String query1 = "insert into [Property] ([Comp_id],[Floor1],[Area],[Bhk],[Bathroom],[prop],[types1],[prop_status],[prop_for],[price],[images],[isApproved],[owner_id]) values (@compid,@floor,@area,@bhk,@bathroom,@prop,@Type,@prop_status,@prop_for,@price,@image,@isApproved,@ownerid);";
                cmd = new SqlCommand(query1, conn);
                cmd.Parameters.AddWithValue("@compid", Convert.ToInt16(ViewState["compid"].ToString()));
                cmd.Parameters.AddWithValue("@floor", ddlFloor.SelectedValue);
                cmd.Parameters.AddWithValue("@area", Convert.ToInt32(txtArea.Text.Trim()));
                cmd.Parameters.AddWithValue("@bhk", ddlBedrooms.SelectedValue);
                cmd.Parameters.AddWithValue("@bathroom", ddlBathroom.SelectedValue);
                cmd.Parameters.AddWithValue("@prop", prop);
                cmd.Parameters.AddWithValue("@prop_status", ddlPropertyStatus.SelectedValue);
                cmd.Parameters.AddWithValue("@type", ddlPropType.SelectedValue);
                cmd.Parameters.AddWithValue("@prop_for", ddlFor.SelectedValue);
                cmd.Parameters.AddWithValue("@price", txtPrice.Text.Trim());
                cmd.Parameters.AddWithValue("@ownerid", 0);
                cmd.Parameters.AddWithValue("@isApproved", 'Y');
                cmd.Parameters.AddWithValue("@propId", propId);
                cmd.ExecuteNonQuery();

                query1 = "update Property set images=@image where Prop_id=" + propId;
                cmd = new SqlCommand(query1, conn);
                cmd.Parameters.AddWithValue("@image", filename);
                cmd.ExecuteNonQuery();

                conn.Close();
                clearAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
    private void clearAll()
    {
        txtCompNameUpdate.Text = String.Empty;
        ddlcity.SelectedIndex = 0;
        ddlLocation.SelectedIndex = 0;
        
        txtAddress.Text = String.Empty;
        txtArea.Text = String.Empty;
        txtPrice.Text = String.Empty;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        clearAll();
    }
}