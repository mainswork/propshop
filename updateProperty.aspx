﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="updateProperty.aspx.cs" Inherits="updateProperty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.5.5/jquery.validate.min.js"></script>
     
    <script type ="text/javascript" >
        var imgData = [];
        var base64Text;
        $(document).ready(function () {

            var imgDataStr = $('#<%= TextBoxImgData.ClientID %>').val();
            var imgDataList=imgDataStr.split("-");
            for (var i = 0; i < imgDataList.length; i++) {
                var imgDataSingle = imgDataList[i].split(",");
                imgData.push([imgDataSingle[0] + "," + imgDataSingle[1], imgDataSingle[2]]);
            }
            resetImageTble();


            $('.num').keypress(function (event) {
                var key = event.which;

                if (!(key >= 48 && key <= 57 || key === 13))
                    event.preventDefault();
            });

            $('#<%= fuImage.ClientID %>').on('change', function (evt) {
                var file = $('#<%= fuImage.ClientID %>')[0].files[0];
                if (file.length == 0) {
                    alert("file not selected");
                    return false;
                }
                var filename = $('#<%= fuImage.ClientID %>').val();
                var extension = filename.replace(/^.*\./, '');
                if (extension == filename) {
                    extension = '';
                } else {
                    extension = extension.toLowerCase();
                }
                switch (extension) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                        {
                            var tgt = evt.target || window.event.srcElement, files = tgt.files;

                            var fileSize = files[0].size;
                            var kb = fileSize / 1024;
                            if (kb > 100) {
                                alert("Please upload less than 100kb sizes image");
                                base64Text = '';
                                $('#<%= fuImage.ClientID %>').val('');
                                $("#imgAdmin").attr('src', '');
                                $("#imgAdmin").change();
                                $('#<%= txtImgName.ClientID %>').val('');
                                return false;
                            }
                            break;
                        }
                    default:
                        alert('Only png and jpeg image is allowed!');
                        base64Text = '';
                        $('#<%= fuImage.ClientID %>').val('');
                        $("#imgAdmin").attr('src', '');
                        $("#imgAdmin").change();
                        $('#<%= txtImgName.ClientID %>').val('');
                        return false;
                }
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    var encodedFile = reader.result;
                    base64Text = encodedFile;
                    $("#imgAdmin").attr('src', encodedFile);
                    $("#imgAdmin").change();
                }
            });

            $('#AddImg').click(function () {

                if (base64Text == '' || base64Text == undefined) {
                    alert("file not selected");
                    return false;
                }

                var imgName = $('#<%= txtImgName.ClientID %>').val();
                if (imgName == '' || imgName == undefined) {
                    alert("Enter Image Name");
                    return false;
                }

                imgData.push([base64Text, imgName]);
                resetImageTble();
                base64Text = '';
                $('#<%= fuImage.ClientID %>').val('');
                $("#imgAdmin").attr('src', '');
                $("#imgAdmin").change();
                $('#<%= txtImgName.ClientID %>').val('');
            });

            $('#<%= btnSubmit.ClientID %>').click(function () {

                if (imgData.length == 0) {
                    alert("Select Images Of Your Properties");
                    return false;
                }

                var imgStr = '';
                for (var i = 0; i < imgData.length; i++) {
                    var base64Text = imgData[i][0];
                    var imgName = imgData[i][1];
                    imgStr += base64Text+","+imgName+"-";

                }
                imgStr=imgStr.substring(0, imgStr.length - 1);
                $('#<%= TextBoxImgData.ClientID %>').val(imgStr);
                
                $('#imageListTbl').empty();
                imgData = [];
            });
        });

        function resetImageTble() {

            $('#imageListTbl').empty();

            for(var i=0; i<imgData.length; i++){
                var base64Text = imgData[i][0];
                var imgName = imgData[i][1];
                $('#imageListTbl').append('<tr>'+
                                            '<td> <img src="' + base64Text + '" height="50px" width="50px"/></td>' +
                                            '<td>' + imgName + '</td>' +
                                            '<td>'+
                                            '    <button class="btn" onclick="deleteImg(' + i + ')"><i class="fa fa-remove"></i></button>' +
                                            '</td>' +
                                        '</tr>');
            }
        }

        function deleteImg(srno) {
            imgData.splice(srno, 1);
            resetImageTble();
        }

    </script>  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">  
   </asp:ScriptManager> 
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12 center">
                <h4>
                    Property selling form</h4>
            </div>
            </div>
            <div class="row"> 
            <div class="col s12 m12 l12 z-depth-3">
              
                        <h5 class="center">Property Address</h5>
                    <div class="row">
                    <div class="input-field col s12 m6 l6">
                            
                                <label ID="Label3" runat="server" for="txtCompNameUpdate">Complex Name</label>                       
                          
                                <asp:TextBox ID="txtCompNameUpdate" CssClass="" runat="server" required></asp:TextBox>
                          
                        </div>
                         <div class="col s12 m6 l6">
                               
                                    <label ID="ddlcity1" runat="server">City</label>
                                    <asp:UpdatePanel ID="citypanel" runat="server">       
                                       <ContentTemplate>        
                                                  
                                         <asp:DropDownList ID="ddlcity" AutoPostBack ="true" OnSelectedIndexChanged="ddlcity_SelectedIndexChanged" CssClass="browser-default" AppendDataBoundItems="true" runat="server">  
                                         </asp:DropDownList>  
                                      </ContentTemplate>  
                                      <Triggers>  
                                        <asp:AsyncPostBackTrigger ControlID ="ddlcity"/>        
                                         </Triggers>    
                                     </asp:UpdatePanel>  
                                </div>
                                </div>
                <div class="row">
                      <div class="col s12 m6 l6">
                               
                                    <label id="Label4" runat="server">Area</label>
                                    <asp:UpdatePanel ID="areaPanel" runat="server">       
                                       <ContentTemplate>        
                                                  
                                         <asp:DropDownList ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack ="true" CssClass="browser-default" AppendDataBoundItems="true" runat="server">  
                                         </asp:DropDownList>  
                                        
                                      </ContentTemplate>  
                                      <Triggers>  
                                        <asp:AsyncPostBackTrigger ControlID ="ddlLocation"/>        
                                         </Triggers>    
                                     </asp:UpdatePanel>  
                                </div>
                                 <div class="input-field col s12 m6 l6">
                                       <label ID="Label5" runat="server" for="txtAddress"> Address</label>
                                         <asp:TextBox ID="txtAddress"  CssClass="materialize-textarea" Columns="80" Rows="3" runat="server" required TextMode="MultiLine"></asp:TextBox>
                                </div>
                           
                </div>
                      </div>
                        </div>          
       <div class="row">
            <div class="col s12 m12 l12 z-depth-3">
                 <h5 class="center">Property Information
            </h5>
                <div class="row">
                
                <div class="input-field col s12 m6 l6">
                    <label for="txtWant">I want To</label>
                     <asp:TextBox ID="txtWant"  runat="server" Text="Sell" ReadOnly="True"></asp:TextBox>
                </div>
                    <div class="col s12 m6 l6 center">
                <p>
                <label>Type</label>
                    <asp:RadioButton ID="rbProp"  GroupName="prop" runat="server" Text="Property"  Checked="True" />
                      <asp:RadioButton ID="rbProj"  GroupName="prop" runat="server" Text="Project" />
                      </p>
                </div>    
                          
                       
              </div>
              <div class="row">
                
                <div class="col s12 m6 l6">
                <label for="ddlPropType">Property Type</label>
                 <asp:DropDownList ID="ddlPropType" runat="server" CssClass="browser-default"  required>
                                        <asp:ListItem>Commercial</asp:ListItem>
                                        <asp:ListItem>Residencial</asp:ListItem>
                                    </asp:DropDownList>
                </div>
                <div class="col s12 m6 l6">
                <label for="ddlFor">For</label>
                  <asp:DropDownList ID="ddlFor" runat="server" CssClass="browser-default" required>
                                    <asp:ListItem>Rent</asp:ListItem>
                                    <asp:ListItem>Sell</asp:ListItem>
                                </asp:DropDownList>
                </div>
                </div>
                                <div class="row">
                
                <div class="col s12 m6 l6">
                 <label for="ddlFor">Bedrooms</label>
                  <asp:DropDownList ID="ddlBedrooms" runat="server" CssClass="browser-default" required>
                                    <asp:ListItem>1RK</asp:ListItem>
                                    <asp:ListItem>1BHK</asp:ListItem>
                                    <asp:ListItem>1.5BHK</asp:ListItem>
                                    <asp:ListItem>2BHK</asp:ListItem>
                                    <asp:ListItem>3BHK</asp:ListItem>
                                    <asp:ListItem>4BHK</asp:ListItem>
                                    <asp:ListItem>5+</asp:ListItem>
                                </asp:DropDownList>
                </div>
                <div class="col s12 m6 l6">
                  <label for="ddlBathroom">Bathrooms</label>
                 <asp:DropDownList ID="ddlBathroom" runat="server" CssClass="browser-default" required>
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5+</asp:ListItem>
                                </asp:DropDownList>
                </div>
                </div>
                     <div class="row">
                
                <div class="col s12 m6 l6">
                  <label for="ddlFloor">Floor</label>
                  <asp:DropDownList ID="ddlFloor" runat="server" CssClass="browser-default" required>
                                    <asp:ListItem>below 10 floor</asp:ListItem>
                                    <asp:ListItem>Above 10 floor</asp:ListItem>
                                    <asp:ListItem>Ground floor only</asp:ListItem>
                                </asp:DropDownList>
                </div>
                <div class="input-field col s12 m6 l6">
                <label for="txtArea">Area(Sq.Ft)</label>
                 <asp:TextBox ID="txtArea" CssClass="num" type="number" runat="server" required></asp:TextBox>
                </div>
                </div>
               
                 <div class="row">
                    <div class="input-field col s12 m6 l6">
                <label for="txtPrice">Price</label>
                  <asp:TextBox ID="txtPrice" CssClass="form-control num" type="number" runat="server" required></asp:TextBox>
                </div>
                    <div class="col s12 m6 l6">
                     <label for="ddlPropertyStatus">Property Status</label>
                     <asp:DropDownList ID="ddlPropertyStatus" runat="server" CssClass="browser-default">
                                    
                                    <asp:ListItem>Pre-Launch</asp:ListItem>
                                    <asp:ListItem>Ready Possession</asp:ListItem>
                                    <asp:ListItem>Resale</asp:ListItem>
                                    <asp:ListItem>Under Construction</asp:ListItem>
                                </asp:DropDownList>
                    </div>
                </div>

                           </div>
                            </div>


                             <div class="row">
            <div class="col s12 m12 l12 z-depth-3">
           
            <h5 class="center">Property Images
            </h5>
                <div class="row">
                    <div class="col s12 m4 l4">
                       <asp:FileUpload runat="server" ID="fuImage" accept="image/.jpg" data-type='image'/>
                    </div>
                    <div class="col s6 m2 l2 center">
                       
                                <img src="" Height="50" Width="50" Visible="true" id="imgAdmin" />                                
                    </div>
                    <div class="input-field col s6 m4 l4">
                    <label for="txtImgName">Image Name</label>
                    <asp:TextBox ID="txtImgName" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col s12 m2 l2 center">
                        <input id="AddImg"  type="button"  class="btn center-block" value="Add"/>
                    </div>
                </div>
                <div class="col s12 m12 l12 center">
                 <table class="table">
                                <thead>
                                    <tr>
                                    <th>Image</th>
                                    <th>Image Name</th>
                                    <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody id="imageListTbl">
                                    <tr>
                                        <td> <img src="" height="50px" width="50px"/></td>
                                        <td>image name</td>
                                        <td>
                                            <button class="btn"><i class="fa fa-remove"></i></button>
                                        </td>
                                    </tr>
                                 </tbody>
                                </table>
                            <asp:HiddenField ID="TextBoxImgData" runat="server"></asp:HiddenField>
                </div>
             </div>
        </div>
         <div class="row">
            <div class="col s6 m6 l6 center">
            <button id="Button1" type="reset" class="btn center-block" >cancel</button>
            </div>
                     
                      <div class="col s6 m6 l6 center">
                                <asp:Button ID="btnSubmit" type="submit" runat="server" CssClass=" btn btn-info center-block " Text="Submit" OnClick="btnSubmit_Click" />
                            </div>  
                      
                        
                   
                </div>
            
         
</asp:Content>

