﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AdminPropDetails.aspx.cs" Inherits="AdminPropDetails" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


   


 <style type="text/css">
.modal
{
    height:50%;
    }
    .modal .modal-footer
    {
        text-align: center !important; 
        }
    .middle-indicator{
   position:absolute;
   top:42%;
   }
  .middle-indicator-text{
   font-size: 2.6rem;
  }
  a.middle-indicator-text{
    color:black !important;
  }
.content-indicator{
    width: 40px;
    height: 40px;
    background: none;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    border-radius: 50px;
     cursor:pointer; 
  }
  .carousel .carousel-item>img {
    height:50vh !important;
}
   
     .carousel .indicators .indicator-item.active {
    background-color: black !important;
}
.carousel .indicators .indicator-item {
        background-color: rgba(255,255,255,1) !important;
}
.leftSide
{
    background-color:#78909c;
    color:#f5f5f5 ;
}
@media screen and (max-width: 600px) 
{
    .modal
    {
        height:90vh !important;
        width:90vw !important;
        }
}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
         
            $('#sliderImages').empty();
            var imgDataStr = $('#<%= sliderPreImgData.ClientID %>').val();
            if (imgDataStr != '' && imgDataStr != undefined) {
                var imgDataList = imgDataStr.split("-");
                for (var i = 0; i < imgDataList.length; i++) {
                    var imgDataSingle = imgDataList[i];
                    $('#sliderImages').append('<a class="carousel-item" href="#one!"><img src="' + imgDataSingle + '"></a>');
                }
            }
            $('.carousel.carousel-slider').carousel(
            {
                fullWidth: true,
                indicators: true
            }
            );
        });
        //        window.onload = function () {
        //            var mapOptions = {
        //                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
        //                zoom: 8,
        //                mapTypeId: google.maps.MapTypeId.ROADMAP
        //            };
        //            var infoWindow = new google.maps.InfoWindow();
        //            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        //            for (i = 0; i < markers.length; i++) {
        //                var data = markers[i]
        //                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
        //                var marker = new google.maps.Marker({
        //                    position: myLatlng,
        //                    map: map,
        //                    title: data.title
        //                });
        //                (function (marker, data) {
        //                    google.maps.event.addListener(marker, "click", function (e) {
        //                        infoWindow.setContent(data.description);
        //                        infoWindow.open(map, marker);
        //                    });
        //                })(marker, data);
        //            }
        //        }
    </script>
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h4 class="center">Property Details</h4>
            </div>
            
        </div>
       
           

                    <div class="row">
                        <div class="col s12 m8 l10 offset-l1 offset-m2">
                            <div class="carousel carousel-slider center">
     <div class="carousel-fixed-item center middle-indicator">
     <div class="left">
      <a  class="movePrevCarousel middle-indicator-text grey lighten-5 waves-effect waves-light content-indicator z-depth-3" onclick="$('.carousel').carousel('prev');"><i class="material-icons left  middle-indicator-text">chevron_left</i></a>
     </div>
     
     <div class="right">
     <a  class="moveNextCarousel middle-indicator-text grey lighten-5 waves-effect waves-light content-indicator z-depth-3" onclick="$('.carousel').carousel('next');"><i class="material-icons right middle-indicator-text">chevron_right</i></a>
     </div>
    </div>
      <div id="sliderImages">
    
    <a class="carousel-item" href="#one!"><img src="img/slider2.jpg"></a>
    <a class="carousel-item" href="#two!"><img src="img/slider3.jpg"></a>
    <a class="carousel-item" href="#three!"><img src="img/slider4.jpg"></a>
    
    </div>
  </div>


    
  <asp:HiddenField ID="sliderPreImgData" runat="server"></asp:HiddenField>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col s12 m11 l10 offset-l2 offset-m1">
                        <div class="row">
                            <div class="col s12 m5 l4 leftSide">
                                <h5>Complex Name <i class="material-icons left">location_city</i> </h5>
                            </div>
                            <div class="col s12 m7 l8">
                                <h5>  <asp:Label ID="lblcompname" CssClass="grey-text" runat="server" /></h5>
                            </div>
                              
                               
                                 
                            </div> 
                            <div class="row">
                             <div class="col s12 m5 l4 leftSide">
                               <h5>Area 
                                <i class="material-icons left Medium">aspect_ratio</i> </h5>
                            </div>
                            <div class="col s12 m7 l8">
                                <h5>   <asp:Label ID="lblArea" CssClass="grey-text" runat="server" /> <span style="font-size:12px">SQFT</span></h5>
                            </div>
                                
                                
                                 
                            </div>
                          
                             <div class="row">
                              <div class="col s12 m5 l4 leftSide">
                                <h5>Bedrooms  
                                <i class="material-icons left">weekend</i></h5>
                            </div>
                            <div class="col s12 m7 l8">
                                <h5>  <asp:Label ID="lblBhk" CssClass="grey-text"  runat="server" /></h5>
                            </div>
                               
                               
                                 
                            </div>
                            <div class="row">
                             <div class="col s12 m5 l4 leftSide">
                                 <h5>Bathrroms  
                                <i class="fa fa-bath left"></i></h5>
                            </div>
                            <div class="col s12 m7 l8">
                                <h5> <asp:Label ID="lblBathrooms" CssClass="grey-text" runat="server" /></h5>
                            </div>
                               
                               
                                 
                            </div>
                            <div class="row">
                             <div class="col s12 m5 l4 leftSide">
                                 <h5>Price  
                                <i class="fa fa-rupee left"></i></h5>
                            </div>
                            <div class="col s12 m7 l8">
                                <h5> <asp:Label ID="lblPrice" CssClass="grey-text" runat="server" /></h5>
                            </div>
                               
                                 
                            </div> 
                              <div class="row">
                            <div class="col s12 m5 l4 leftSide">
                                <h5>Property For   
                                <i class="material-icons left">vpn_key</i></h5> 
                            </div>
                            <div class="col s12 m7 l8">
                                <h5>  <asp:Label ID="lblPropFor" CssClass="grey-text"  runat="server" /></h5>
                            </div>
                               
                                 
                            </div>
                            <div class="row">
                              <div class="col s12 m5 l4 leftSide">
                                <h5>Type
                                <i class="material-icons left">home</i></h5>
                                
                            </div>
                             <div class="col s12 m7 l8">
                                <h5> <asp:Label ID="lblPropis" CssClass="grey-text" runat="server" /></h5>
                            </div>
                                 
                            </div>
                            <div class="row">
                                 <div class="col s12 m5 l4 leftSide">
                                 <h5>Location  
                                <i class="material-icons left">location_on</i></h5>
                            </div>
                            <div class="col s12 m7 l8">
                                <h5> <asp:Label ID="lbllocation" CssClass="grey-text" runat="server"/></h5>
                            </div>
                                
                                 
                            </div>
                            <div class="row">
                              <div class="col s12 m5 l4 leftSide">
                                <h5>City 
                                <i class="material-icons left">my_location</i></h5>
                                
                            </div>
                            <div class="col s12 m7 l8">
                                <h5> <asp:Label ID="lblCity" CssClass="grey-text" runat="server" /></h5>
                            </div>
                                 
                            </div>
                            <div class="row">
                            <div class="col s12 m5 l4 leftSide">
                               <h5>State 
                                <i class="material-icons left">add_location</i></h5>
                            </div>
                            <div class="col s12 m7 l8">
                                <h5>  <asp:Label ID="lblSate" CssClass="grey-text" runat="server" /></h5>
                            </div>
                               
                                 
                            </div>
                            <div class="row">
                                <div class="col s12 m5 l4 leftSide">
                             <h5>Pincode 
                                <i class="material-icons left">person_pin_circle</i></h5>
                                 
                            </div>
                            <div class="col s12 m7 l8">
                                <h5>  <asp:Label ID="lblpin" CssClass="grey-text" runat="server"/></h5>
                            </div>
                                
                            </div>
                             
                        </div>
                      
                    </div>
    
                    
        
         </div>
        
       
                 
</asp:Content>

