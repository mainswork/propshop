﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class services : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                this.bindData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    protected void bindData()
    {
        
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            String query = "select * from services";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {

                lblFinance.Text = dr["finance"].ToString();
                lblInterior.Text = dr["interior"].ToString();
                lblNri.Text = dr["nri"].ToString();

            }
      
    }
}