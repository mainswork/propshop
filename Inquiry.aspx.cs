﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Inquiry : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            String query = "SELECT inq_id,name,email,contact,prop_for,type,bedrooms,bathrooms,city,location,budget_min +'-'+budget_max AS Budget,parking_type FROM property_inquiry ";
            SqlCommand cmd = new SqlCommand(query, conn);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            gvInquiry.DataSource = ds.Tables[0];

            gvInquiry.DataBind();

        }
    }
    

    //protected void TxtSearch_TextChanged1(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (conn.State == ConnectionState.Closed)
    //        {
    //            conn.Open();
    //        }
    //        string query = "SELECT inq_id,name,email,contact,prop_for,type,bedrooms,bathrooms,city,location,budget_min +'-'+budget_max AS Budget,parking_type FROM property_inquiry WHERE [" + DropDownList1.SelectedValue + "] = '" + TxtSearch.Text + "';";
    //        SqlCommand cmd = new SqlCommand(query, conn);
    //        DataSet ds = new DataSet();
    //        SqlDataAdapter da = new SqlDataAdapter(cmd);
    //        da.Fill(ds);
    //        gvInquiry.DataSource = ds.Tables[0];

    //        gvInquiry.DataBind();

    //        if (conn.State == ConnectionState.Open)
    //        {
    //            conn.Close();
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    //protected void Button1_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (conn.State == ConnectionState.Closed)
    //        {
    //            conn.Open();
    //        }
    //        string query = "SELECT inq_id,name,email,contact,prop_for,type,bedrooms,bathrooms,city,location,budget_min +'-'+budget_max AS Budget,parking_type FROM property_inquiry WHERE [" + DropDownList1.SelectedValue + "] = '" + TxtSearch.Text + "';";
    //        SqlCommand cmd = new SqlCommand(query, conn);
    //        DataSet ds = new DataSet();
    //        SqlDataAdapter da = new SqlDataAdapter(cmd);
    //        da.Fill(ds);
    //        gvInquiry.DataSource = ds.Tables[0];

    //        gvInquiry.DataBind();

    //        if (conn.State == ConnectionState.Open)
    //        {
    //            conn.Close();
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
}