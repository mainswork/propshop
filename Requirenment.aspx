﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Requirenment.aspx.cs" Inherits="Requirenment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div class="row">
    <div class="col s12 m12 l12 center">
        <br />
            <h4>List Your Requirement</h4>
     </div>
</div>
 <div class="container">
    
     <div class="row ">
         <div class="col s12 m12 l12 z-depth-3">
      
        <div class="row">
        <div class="col s12 m12 l12 center">
                    <h5 class="">Personal Details</h5>
                    </div>
                  
     </div>
                    
                         <div class="row">
                            <div class="input-field col s12 m4 l4">
                              <asp:TextBox ID="txtName"  CssClass="validate" runat="server" required></asp:TextBox>

                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Name is Mandatory"
                                Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Name is Mandatory"
                                ValidationGroup="vgEmployee" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="recName" runat="server" ErrorMessage="Please enter only character"
                                ControlToValidate="txtName" Display="Dynamic" ToolTip="Please enter only character" Text="*"
                                ValidationGroup="vgEmployee" ValidationExpression="^[a-zA-Z\s]+$"></asp:RegularExpressionValidator>
                              <label for="txtName">Name</label>
                            </div>
                            <div class="input-field col s12 m4 l4">
                               <asp:TextBox ID="txtEmail"  CssClass="validate" runat="server"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email."
                                ControlToValidate="txtEmail" Display="Dynamic" ToolTip="Invalid Email." Text="*"
                                ValidationGroup="vgEmployee" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                              <label for="txtEmail">Email</label>
                            </div>
                        
                             <div class="input-field col s12 m4 l4">
                               <asp:TextBox ID="txtMobileNo"  CssClass="validate" runat="server"
                                MaxLength="10" required></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMobileNo"
                                        ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
                                        Text="Enter Valid Mobile Number." ToolTip="Enter Valid Mobile Number." ValidationGroup="vgEmployee"
                                        ForeColor="Red" SetFocusOnError="True"></asp:RegularExpressionValidator>

                              <label for="txtMobileNo">Mobile Number</label>
                            </div>
                          </div>
                       
                        
             
                        
                   
                    <div class="col s12 m12 l12">
                   <h5 class="center">Property Information</h5>
                        <br />
                    </div>
             <div class="row">

             
                     <div class="col s12 m3 l3">
                     <asp:Label ID="lblWant" runat="server" for="txtWant" Text="I want to"></asp:Label>
                            <asp:DropDownList ID="ddlRequirement" runat="server" CssClass="browser-default">
                                <asp:ListItem>Buy</asp:ListItem>
                                <asp:ListItem>Rent/Lease</asp:ListItem>
                            </asp:DropDownList>
             </div>
                    <div class="col s12 m3 l3">
                         <asp:Label ID="lblPropType" runat="server" Text="Property Type"></asp:Label>
                             <asp:DropDownList ID="ddlPropType" runat="server" CssClass="browser-default">
                                <asp:ListItem>Commercial</asp:ListItem>
                                <asp:ListItem>Residencial</asp:ListItem>
                            </asp:DropDownList>
                       
             </div>
             <div class="col s12 m3 l3">
                <asp:Label ID="lblBedrooms" runat="server" Text="Bedrooms"></asp:Label>    <br />                    
                            <asp:DropDownList ID="ddlBedrooms" runat="server" CssClass="browser-default">
                                <asp:ListItem>1RK</asp:ListItem>
                                <asp:ListItem>1BHK</asp:ListItem>
                                <asp:ListItem>1.5BHK</asp:ListItem>
                                <asp:ListItem>2BHK</asp:ListItem>
                                <asp:ListItem>3BHK</asp:ListItem>
                                <asp:ListItem>4BHK</asp:ListItem>
                                <asp:ListItem>5+</asp:ListItem>
                            </asp:DropDownList>
             </div>
             <div class="col s12 m3 l3">
                 <asp:Label ID="lblBathroom" runat="server" Text="Bathrooms"></asp:Label>
                        
                            <asp:DropDownList ID="ddlBathroom" runat="server" CssClass="browser-default">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5+</asp:ListItem>
                            </asp:DropDownList>
             </div>
             </div>
             <div class="row">
                 <div class="input-field col s12 m3 l3">
                     <asp:TextBox ID="txtCity" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtCity" runat="server" ErrorMessage="City is Mandatory"
                                Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="City is Mandatory"
                                ValidationGroup="vgEmployee" ControlToValidate="txtCity"></asp:RequiredFieldValidator>
                                  <label for="txtCity">City</label>
                 </div>
                 <div class="input-field col s12 m3 l3">
                     <asp:TextBox ID="txtLocation" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ErrorMessage="Location is Mandatory"
                                Display="Dynamic" SetFocusOnError="True" Text="*" ToolTip="Location is Mandatory"
                                ValidationGroup="vgEmployee" ControlToValidate="txtCity"></asp:RequiredFieldValidator>
                                <label for="txtLocation">Location</label>
                 </div>
                 <div class="input-field col s12 m3 l3">
                   <asp:TextBox ID="txtMin" CssClass="form-control" runat="server"></asp:TextBox>
                  <label for="txtMin">Min Budget</label>
                 </div>
                 
                 <div class="input-field col s12 m3 l3">
                  <asp:TextBox ID="txtMax" CssClass="form-control" runat="server"></asp:TextBox>
                 <label for="txtCity">Max Budget</label>
                 </div>
             </div>
             <div class="row">
             
                <div class="input-field col s12 m12 l12 center">
                   
                   
                            <asp:Button ID="btnCancel" type="reset" runat="server" Text="Cancel" CssClass="btn btn-default"
                                OnClick="btnCancel_Click" UseSubmitBehavior="False" />
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary"
                                ValidationGroup="vgEmployee" OnClick="btnSubmit_Click" />
                        
                        </div>
                        </div>
        </div>
     </div>
          
                   <%-- <div class="col-md-6 offset-md-3">
                        <div class="col-md-11">
                            <asp:Label ID="lblParking" runat="server" Text="Parking"></asp:Label>                        
                            <asp:DropDownList ID="ddlParking" runat="server" CssClass="browser-default">
                                <asp:ListItem>None</asp:ListItem>
                                <asp:ListItem>Common</asp:ListItem>
                                <asp:ListItem>Private</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>--%>
                    

    </div>
    
   
   
</asp:Content>
