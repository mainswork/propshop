﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Configuration;
public partial class AdminSell : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind_ddlCity();
        }
    }

    public void Bind_ddlCity()
    {
        conn.Open();
        SqlCommand cmd = new SqlCommand("select * from city", conn);

        SqlDataReader dr = cmd.ExecuteReader();
        ddlcity.DataSource = dr;
        ddlcity.Items.Clear();
        ddlcity.Items.Add("Select city");
        ddlcity.DataTextField = "city_name";
        ddlcity.DataValueField = "city_id";
        ddlcity.DataBind();
        dr.Close();
        conn.Close();
    }

    public void Bind_area()
    {
        conn.Open();
        SqlCommand cmd = new SqlCommand("select * from area where city_id ='" + ddlcity.SelectedValue + "'", conn);
        SqlDataReader dr = cmd.ExecuteReader();
        
        ddlLocation.DataSource = dr;
        ddlLocation.Items.Clear();
        ddlLocation.Items.Add("Select Area");
        ddlLocation.DataTextField = "area_name";
        ddlLocation.DataValueField = "area_id";
        ddlLocation.DataBind();
        dr.Close();
        conn.Close();
    }
    public void bind_pincode(){
            conn.Open();
        SqlCommand cmd = new SqlCommand("select * from area where area_id ='" + ddlLocation.SelectedValue + "'", conn);
        SqlDataReader dr = cmd.ExecuteReader();
        while(dr.Read()) {
           // txtPincode.Value =Convert.ToString(dr["pincode"]);
            ViewState["pincode"] = dr["pincode"];
           
        }
        dr.Close();
        conn.Close();
    }
    protected void ddlcity_SelectedIndexChanged(object sender, EventArgs e)
    {
        Bind_area();
    }
    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        bind_pincode();
        
    }  
            protected void  btnSubmit_Click(object sender, EventArgs e)
            {
                /*String filename = " ";
            //    string lat_long = getLatLong();
               // string lat = lat_long.Split('#')[0];
               // string lng = lat_long.Split('#')[1];
                if (fuImage.HasFile)
                {
                    try
                    {
          
                        filename = Path.GetFileName(fuImage.FileName);
                        string path = "~/Image/" + filename;
                        int imgSize = fuImage.PostedFile.ContentLength;
                        if (fuImage.PostedFile != null && fuImage.PostedFile.FileName != "")
                        {
                            if (imgSize > 500)
                            {
                                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "Alert", "alert('image size should be less than 500kb')", true);
                                
                            }
                            else
                            {   
                                fuImage.SaveAs(Server.MapPath(path));
                                imgAdmin.ImageUrl = path;

                            }
                        }
                    }

                    catch (Exception es)
                    {   
                        throw es;
                    }
                }*/
                string prop = " ";
                if (rbProp.Checked)
                {
                    prop = rbProp.Text;
                }
                else {
                    prop = rbProj.Text;  
                }
                
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    SqlCommand cmd;
                    String val = ddlLocation.SelectedItem.ToString().Trim();
                    String query = "insert into [complex](comp_name,location,address1,city,state,pincode) values (@name,@location,@add,@city,@state,@pincode);";
                    cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@name", txtCompName.Text.Trim());
                    cmd.Parameters.AddWithValue("@location", ddlLocation.SelectedItem.ToString().Trim());
                    cmd.Parameters.AddWithValue("@add", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@city", ddlcity.SelectedItem.ToString().Trim());
                    cmd.Parameters.AddWithValue("@state", " ");
                    cmd.Parameters.AddWithValue("@pincode", ViewState["pincode"].ToString());
                   // cmd.Parameters.AddWithValue("@lat", 0);
                   // cmd.Parameters.AddWithValue("@lng", 0);
            
                    cmd.ExecuteNonQuery();

                    String query2 = "Select Max (Comp_id) As compid from complex;";
                    cmd = new SqlCommand(query2, conn);
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        int compid = Convert.ToInt16(dr["compid"]);
                        ViewState["compid"] = compid;
                    }
                    dr.Close();

                    String query1 = "insert into [Property] ([Comp_id],[Floor1],[Area],[Bhk],[Bathroom],[prop],[types1],[prop_status],[prop_for],[price],[isApproved],[owner_id],[featured_status]) values (@compid,@floor,@area,@bhk,@bathroom,@prop,@Type,@prop_status,@prop_for,@price,@isApproved,@ownerid,@featured);";
                    query1 += "select max(Prop_id) from Property";
                    cmd = new SqlCommand(query1, conn);
                    cmd.Parameters.AddWithValue("@compid", Convert.ToInt16(ViewState["compid"].ToString()));
                    cmd.Parameters.AddWithValue("@floor", ddlFloor.SelectedValue);
                    cmd.Parameters.AddWithValue("@area", Convert.ToInt32(txtArea.Text.Trim()));
                    cmd.Parameters.AddWithValue("@bhk", ddlBedrooms.SelectedValue);
                    cmd.Parameters.AddWithValue("@bathroom", ddlBathroom.SelectedValue);
                    cmd.Parameters.AddWithValue("@prop",prop);
                    cmd.Parameters.AddWithValue("@prop_status", ddlPropertyStatus.SelectedValue);
                    cmd.Parameters.AddWithValue("@type", ddlPropType.SelectedValue);
                    cmd.Parameters.AddWithValue("@prop_for", ddlFor.SelectedValue);
                    cmd.Parameters.AddWithValue("@price", txtPrice.Text.Trim());
                    cmd.Parameters.AddWithValue("@ownerid", 0);
                    cmd.Parameters.AddWithValue("@isApproved", 'Y');
                    cmd.Parameters.AddWithValue("@featured","UnFeatured");
                    int propId = Convert.ToInt32(cmd.ExecuteScalar());


                    /*add img*/
                    String filename = "";
                    String imgDataList = TextBoxImgData.Value.Trim();
                    String[] imgDataArr = imgDataList.Split('-');
                    for (int i = 0; i < imgDataArr.Length; i++)
                    {
                        String[] imgData = imgDataArr[i].Split(',');
                        String queryImg = "insert into property_imgages ([img_name],[img_base_64],[property_id]) " +
                        "values (@imgName,@imgBase64,@propertyId);";
                        cmd = new SqlCommand(queryImg, conn);
                        cmd.Parameters.AddWithValue("@imgName", imgData[2]);
                        cmd.Parameters.AddWithValue("@imgBase64", imgData[0] + "," + imgData[1]);
                        cmd.Parameters.AddWithValue("@propertyId", propId);
                        cmd.ExecuteNonQuery();

                        if (filename.Equals("")) {
                            filename = imgData[0] + "," + imgData[1];
                        }
                    }

                    /*add end*/

                    query1 = "update Property set images=@image where Prop_id=" + propId;
                    cmd = new SqlCommand(query1, conn);
                    cmd.Parameters.AddWithValue("@image", filename);
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    clearAll();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            //private string getLatLong()
            //{
            //    string latLong = "";
            //    string url = "http://maps.google.com/maps/api/geocode/xml?address=" + txtLocation.Text + "&sensor=false";
            //    WebRequest request = WebRequest.Create(url);
            //    using (WebResponse response = (HttpWebResponse)request.GetResponse())
            //    {
            //        using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            //        {
            //            DataSet dsResult = new DataSet();
            //            dsResult.ReadXml(reader);
            //            foreach (DataRow row in dsResult.Tables["result"].Rows)
            //            {
            //                string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
            //                DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
            //                //dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);
            //                latLong = location["lat"] + "#" + location["lng"];
            //            }

            //        }
            //    }
            //    return latLong;
            //}

            protected void btnCancel_Click(object sender, EventArgs e)
            {
                clearAll();
            }
            private void clearAll()
            {
                txtCompName.Text = String.Empty;
                ddlcity.SelectedIndex = 0;
                ddlLocation.SelectedIndex = 0;  
                txtAddress.Text = String.Empty;
                txtArea.Text = String.Empty;
                txtPrice.Text = String.Empty;

            }
    

}