﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PurchaseRequest : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
        }
    }
    protected void fillgrid()
    {
        String query = "SELECT DISTINCT Prop_id,Cust_id,Status1 FROM PurchaseRequest;";
        SqlCommand cmd = new SqlCommand(query, conn);
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        gvPurchaseReq.DataSource = ds.Tables[0];
        gvPurchaseReq.DataBind();

    }
    protected void imgShowProperty_Click(object sender, EventArgs e)
    {
        try
        {
            gvOwner.Visible = true;
            int PID = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            String query = "select p.Prop_id,p.owner_id,c.name,c.contact,c.email from Property p INNER JOIN customer c on c.cust_id=p.owner_id where p.prop_id="+PID ;
            SqlCommand cmd = new SqlCommand(query, conn);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            gvOwner.DataSource = ds.Tables[0];
            gvOwner.DataBind();
            conn.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    

}
    