﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Login1 : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
        
    protected void Page_Load(object sender, EventArgs e)
    {



    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            SqlCommand cmd = new SqlCommand("SELECT cust_id,name,email from customer where email=@emailid and password=@password", conn);
            cmd.Parameters.AddWithValue("@emailid", txtEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@password", txtPassword.Text.Trim());
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                Session["Cust_id"] = Convert.ToInt32(dr["cust_id"]);
                Session["name"] = Convert.ToString(dr["name"]);
                Session["email"] = Convert.ToString(dr["email"]);
                Response.Redirect("Home.aspx");
            }
            else
            {
                lblloginfailure.Visible = true;

            }
            dr.Close();
            conn.Close();
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }

    protected void btnSignIn_Click(object sender, EventArgs e)
    {
        Response.Redirect("Registraion.aspx");
    }
    protected void lblForgotPass_Click(object sender, EventArgs e)
    {
        Response.Redirect("Forgotpassword.aspx");
    }
}