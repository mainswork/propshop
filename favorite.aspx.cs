﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class favorite : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        loadProperty();
    }
    protected void loadProperty()
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            int cust=Convert.ToInt32(Session["Cust_id"]);
            String query = "SELECT c.comp_name,c.location,c.pincode,p.Prop_id,p.price,p.images,p.Bhk,p.Bathroom,p.Area,p.types1 FROM complex c JOIN Property p ON c.Comp_id=p.Comp_id where Prop_id in (Select Prop_id from purchaserequest where cust_id="+cust+")"; 
            SqlCommand cmd = new SqlCommand(query, conn);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            dlstProperty.DataSource = ds.Tables[0];
            dlstProperty.DataBind();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void imgProperty_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
                int propid = Convert.ToInt32(((ImageButton)sender).CommandArgument);
                Session["propid"] = propid;
                Response.Redirect("PropertyDetails.aspx");
        }
       catch (Exception es)
        {
            throw es;
        }
    }

    protected void btndelete_Click(object sender, EventArgs e)
    {
        int propid = Convert.ToInt32(((Button)sender).CommandArgument);
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
        }
        string query = "DELETE FROM PurchaseRequest WHERE cust_id="+ Convert.ToInt32(Session["cust_id"])+ "And Prop_id=" +propid;
        SqlCommand cmd = new SqlCommand(query, conn);
        cmd.ExecuteNonQuery();
        loadProperty(); 
        if (conn.State == ConnectionState.Open)
        {
            conn.Close();
        }


    }
   
}

    
                            