﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Text;
using System.Configuration;
public partial class PropertyDetails : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
       // int prop = Convert.ToInt32(Session["propid"]);
        if (!IsPostBack)
        {
            try
            {
                this.BindData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

        
    private void BindData()
    {
        try
        {
            int prop1 = Convert.ToInt32(Session["propid"]);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            /*slider imges*/
            String imgDataListOld = "";
            String query2 = "select * from property_imgages where property_id="+ prop1;
            SqlCommand cmd2 = new SqlCommand(query2, conn);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            while (dr2.Read())
            {
                imgDataListOld += dr2["img_base_64"].ToString() + "-";
            }

            if (imgDataListOld.Length > 0)
            {
                imgDataListOld = imgDataListOld.Substring(0, imgDataListOld.Length - 1);
            }
            sliderPreImgData.Value = imgDataListOld;
            /*slider images*/
            dr2.Close();
           
            string query = "select Prop_id,Bhk,Bathroom,types1,prop_for,price,images,Area,Comp_id from Property where Prop_id="+ prop1;
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[7] {
                new DataColumn("Prop_id"),
                new DataColumn("Bhk"), 
                 new DataColumn("Bathroom"),
                new DataColumn("types1"),
                new DataColumn("prop_for"),
                new DataColumn("price"),
                new DataColumn("Area")
            });
            if (dr.Read())
            {
                int cmp = Convert.ToInt32(dr["Comp_id"]);
                ViewState["Compid"] = cmp;
                lblArea.Text = dr["Area"].ToString();
                lblBathrooms.Text = dr["Bathroom"].ToString();
                lblBhk.Text = dr["Bhk"].ToString();
               lblPrice.Text=dr["price"].ToString();
               lblPropFor.Text = dr["prop_for"].ToString();
                lblPropis.Text=dr["types1"].ToString();
               dt.Rows.Add(dr["Prop_id"].ToString(), dr["Bhk"].ToString(), dr["Bathroom"].ToString(), dr["types1"].ToString(), dr["prop_for"].ToString(), dr["price"].ToString(), dr["Area"].ToString());
               ViewState["dtSelectProp"] = dt;
                
            }
            dr.Close();
            //DataSet ds = new DataSet();
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //da.Fill(ds);
            //Session["dtSelectProp"] = ds.Tables[0];
            //fvProperty.DataSource = ds.Tables[0];
            //fvProperty.DataBind();

            int comp = Convert.ToInt32(ViewState["Compid"]);
            string query1 = "select comp_name,location,city,state,pincode,Latitude, Longitude,address1 from complex where Comp_id=" + comp;
            cmd = new SqlCommand(query1, conn);
            SqlDataReader dr1 = cmd.ExecuteReader();
            DataTable dt1 = new DataTable();
            dt1.Columns.AddRange(new DataColumn[6] {
                new DataColumn("comp_name"),
                new DataColumn("location"), 
                new DataColumn("city"),
                new DataColumn("state"),
                new DataColumn("pincode"),
                new DataColumn("address1")
            });
            if (dr1.Read())
            {
                lblcompname.Text = dr1["comp_name"].ToString();
                lblCity.Text = dr1["city"].ToString();
                lbllocation.Text = dr1["location"].ToString();
                lblpin.Text = dr1["pincode"].ToString();
                lblSate.Text = dr1["state"].ToString();
                dt1.Rows.Add(dr1["comp_name"].ToString(), dr1["location"].ToString(), dr1["city"].ToString(), dr1["state"].ToString(), dr1["pincode"].ToString(), dr1["address1"].ToString());
                ViewState["dtSelectComp"] = dt1;
            }
            dr1.Close();
            //DataSet dts = new DataSet();
            //SqlDataAdapter dta = new SqlDataAdapter(cmd);
            //dta.Fill(dts);
            //Session["dtSelectComp"] = dts.Tables[0];
            //fvlocation.DataSource = dts.Tables[0];
            //fvlocation.DataBind();
            //rptMarkers.DataSource = dts.Tables[0];
            //rptMarkers.DataBind();

            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //protected void imgProperty_Click(object sender, ImageClickEventArgs e)
    //{
    //    try
    //    {

    //        // MPE_CallHistoryDetails.Show();
    //        int PropertyId = Convert.ToInt32(((ImageButton)sender).CommandArgument);
    //        DataTable Prop = (DataTable)ViewState["PropertyId"];
    //        if (Prop.Rows.Count > 0)
    //        {
    //            //IEnumerable result = from myRow in dtProduct.AsEnumerable()
    //            //             where myRow.Field<int>("ProductID") == ProductID
    //            //             select myRow;
    //            //fvProduct.DataSource = result;
    //            //fvProduct.DataBind();
    //            DataTable dt = Prop.Clone();
    //            foreach (DataRow dr in Prop.Rows)
    //            {
    //                if (Convert.ToInt32(dr["Prop_id"]) == PropertyId)
    //                {
    //                    dt.Rows.Add(dr["Prop_id"].ToString(), dr["Area"].ToString(), dr["Bhk"].ToString(),dr["Bathroom"].ToString(),dr["types1"].ToString(), dr["price"].ToString(), dr["images"].ToString(),);
    //                    break;
    //                }
    //            }
    //            ViewState["dtSelectProduct"] = (DataTable)dt;
    //            fvProperty.DataSource = dt;
    //            fvProperty.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }

    //}
    protected void btnInterested_Click(object sender, EventArgs e)
    {
        int prop1 = Convert.ToInt32(Session["propid"]);
        DateTime d = DateTime.Today;
        ViewState["custName"] = name.Text.ToString();
        ViewState["custMobileNo"] = mobileNo.Text.ToString();
        ViewState["custEmail"] = txtEmail.Text.ToString();
        ViewState["comment"] = comment.Text.ToString();
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            String query = "insert into [PurchaseRequest_New](Prop_id,cust_name,cust_email,cust_contact,comment,date) values (@propid,@name,@email,@contact,@comment,getdate());";
            SqlCommand cmd = new SqlCommand(query, conn);
            
            cmd.Parameters.AddWithValue("@propid",prop1);
            cmd.Parameters.AddWithValue("@name",name.Text.ToString());
            cmd.Parameters.AddWithValue("@email",txtEmail.Text.ToString());
            cmd.Parameters.AddWithValue("@contact",mobileNo.Text.ToString());
            cmd.Parameters.AddWithValue("@comment", comment.Text.ToString());
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            intmail();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('Thank you for Your interest. We will contact you soon.');", true);
            Response.Redirect("Home.aspx");
        }
        catch (Exception ex)
        {

        }
    
    }
    protected void intmail()
    {
        try
        {
           string msg1 = genmail();
            string ToEmail = "nehaljethwa6@gmail.com";
            SendEmail1 s = new SendEmail1();
            
            string subject = "Customer Details";
            s.sendEMail(ToEmail,subject,msg1);
            

        }
        catch(Exception er)
        {

        }
    
    }
    private string genmail()
    {
        DataTable dtSelcomp = (DataTable)ViewState["dtSelectComp"];
        DataTable dtSelPop = (DataTable)ViewState["dtSelectProp"];
        string str = " ";
        try{
            if (ViewState["dtSelectComp"] != null && ViewState["dtSelectProp"] != null)
            {

                StringBuilder sb = new StringBuilder();         //comp_name,location,city,state,pincode
                sb.Append("<table cellpadding=10 cellspacing=5>");
                sb.Append("<tr><th>Intrested Customer information for Property</th></tr>");
                sb.Append("<tr><td>Name - " + ViewState["custName"].ToString() + "</td></tr>");
                sb.Append("<tr><td>Mobile No - " + ViewState["custMobileNo"].ToString() + "</td></tr>");
                sb.Append("<tr><td>Email - " + ViewState["custEmail"].ToString() + "</td></tr>");
                sb.Append("<tr><td>Comment - " + ViewState["comment"].ToString() + "</td></tr>");
                sb.Append("</table>");
                foreach (DataRow dr in dtSelcomp.Rows)
                {
                    sb.Append("<table cellpadding=10 cellspacing=5 border='1' width='100%'>");
                    sb.Append("<tr><th colspan='7'>Interested Property </th></tr>");
                    sb.Append("<tr><th colspan='7'>Complex Information</th></tr>");
                    sb.Append("<tr><th colspan='2'>Complex Name</th><th>Location</th><th>City</th><th>State</th><th>Pincode</th></tr>");
                    sb.Append("<tr><td colspan='2'>" + dr["comp_name"].ToString() + "</td>");
                    sb.Append("<td>" + dr["location"].ToString() + "</td>");
                    sb.Append("<td>" + dr["city"].ToString() + "</td>");
                    sb.Append("<td>" + dr["state"].ToString() + "</td>");
                    sb.Append("<td align='right'>" + dr["pincode"].ToString() + "</td>");
                    
                }
               

                foreach (DataRow du in dtSelPop.Rows)
                {
                    sb.Append("<tr><th colspan='7'>Property Information</th></tr>");
                     sb.Append("<tr><th>Property Id</th><th>Bhk</th><th>Bathroom</th><th>Property type</th><th>Price</th><th>Area sqft</th><th>Propert For</th></tr>");
                    sb.Append("<tr><td>" + du["Prop_id"].ToString() + "</td>");
                    sb.Append("<td>" + du["Bhk"].ToString() + "</td>");
                    sb.Append("<td>" + du["Bathroom"].ToString() + "</td>");               
                    sb.Append("<td>" + du["types1"].ToString() + "</td>");
                    sb.Append("<td>" + du["price"].ToString() + "</td>");
                    sb.Append("<td>" + du["Area"].ToString() + "</td>");
                    sb.Append("<td align='right'>" + du["prop_for"].ToString() + "</td>");
                }
                
                
                sb.Append("</table>");
                str = Convert.ToString(sb);
            }
           
        }
        catch (Exception ey)
        {
            
        }
        return str;

    }

}