﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class AllProperty : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        try {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string propId = Request.QueryString["propid"];
            string type = Request.QueryString["type"];
            if (type != null)
            {
                if (type.Equals("featured"))
                {
                    String featuredStatus = "";
                    String query = "select * from property where Prop_id=" + propId;
                    SqlCommand cmd = new SqlCommand(query, conn);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        featuredStatus = dr["featured_status"].ToString();
                    }
                    dr.Close();
                    query = "update property set featured_status='" + (featuredStatus.Equals("Featured") ? "UnFeatured" : "Featured") + "' where Prop_id=" + propId;
                    cmd = new SqlCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    Response.Redirect("AllProperty.aspx");
                }
                else if (type.Equals("disable"))
                {
                    String isApproved = "";
                    String query = "select * from property where Prop_id=" + propId;
                    SqlCommand cmd = new SqlCommand(query, conn);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        isApproved = dr["isApproved"].ToString();
                    }
                    dr.Close();

                    query = "update property set isApproved='" + (isApproved.Equals("N") ? "Y" : "N") + "' where Prop_id=" + propId;
                    cmd = new SqlCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    Response.Redirect("AllProperty.aspx");
                }
            }
        }
        catch (Exception err)
        {
            Response.Redirect("AllProperty.aspx");
        }

        if (!IsPostBack)
        {
            fillgridview();
        }
    }
    protected void fillgridview()
    {
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
        }
        String query = "select * from property p JOIN complex c on c.comp_id=p.comp_id;";
        SqlCommand cmd = new SqlCommand(query, conn);
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        
        gvProperty.DataSource = ds.Tables[0];
        
        gvProperty.DataBind();       
       

    }
    //protected void imgShowProperty_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int PID = Convert.ToInt32(((LinkButton)sender).CommandArgument);
    //        if (conn.State == ConnectionState.Closed)
    //        {
    //            conn.Open();
    //        }
    //        String query = "select * from property p JOIN complex c on c.comp_id=p.comp_id where  p.prop_id = " + PID;
    //        SqlCommand cmd = new SqlCommand(query, conn);
    //        DataSet ds = new DataSet();
    //        SqlDataAdapter da = new SqlDataAdapter(cmd);
    //        da.Fill(ds);
    //        gvPropinfo.DataSource = ds.Tables[0];
    //        gvPropinfo.DataBind();
    //        conn.Close();
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }

    //}
  
}