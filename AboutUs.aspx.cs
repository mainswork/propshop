﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;
using System.Text;

public partial class ContactUs : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindData();
            
        }
    }
    private void BindData()
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            String query = "SELECT * FROM about_us join management_team on(about_us.about_us_id=management_team.about_us_id)  where 1=1 ";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read()) {
                misionvision.Text = sdr["mission_vision"].ToString();
                abtCompany.Text = sdr["about_co"].ToString();
                csr.Text = sdr["csr"].ToString();   
                address.Text = sdr["co_address"].ToString();
                lat.Value = sdr["lat"].ToString();
                lng.Value = sdr["long"].ToString();
                ListItem li = new ListItem();
                li.Text = sdr["name"].ToString();
                teamMember.Items.Add(li);
            }
            sdr.Close();
            String query2 = "select * from contact_details";
            SqlCommand cmd1 = new SqlCommand(query2, conn);
            SqlDataReader sdr1 = cmd1.ExecuteReader();
            while (sdr1.Read()) {
                ListItem li1 = new ListItem();
                li1.Text = sdr1["contact_name"].ToString();
                ListItem li3 = new ListItem();
                li3.Text = sdr1["mob_no"].ToString();
                contactPerson.Items.Add(li1 + "-" + li3);
                
                ListItem li2 = new ListItem();
                li2.Text = sdr1["email_id"].ToString();
                email.Items.Add(li2);
            }
            sdr1.Close();
            conn.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}