﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class RegisterAdmin : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("AdminHome.aspx");
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            String query = "insert into [Admin](Admin_name,Admin_username,Admin_password) values (@name,@email,@password);";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@email", txtUsername.Text.Trim());
            cmd.Parameters.AddWithValue("@password", txtPassword.Text.Trim());

            int i = cmd.ExecuteNonQuery();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('Register Successfully.');", true);
            txtName.Text = "";
            txtUsername.Text = "";
            conn.Close();

        }
        catch (Exception ex)
        {

        }
    }
}