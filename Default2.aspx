﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container">
        <div class="col-md-5">
            <div class="panel">
                <div class="form-group">
                    <label for="txtProdName">
                        Product Name</label>
                    <div>
                        <asp:TextBox ID="txtProdName" placeholder="Product Name" CssClass="form-control"
                            runat="server"></asp:TextBox>

                    </div>
                </div>
                <div class="form-group">
                    <label for="txtPrice">
                        Price</label>
                    <div>
                        <asp:TextBox ID="txtPrice" placeholder="Price" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtdescr">
                        Description</label>
                    <div>
                        <asp:TextBox ID="txtdescr" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div>
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Products Details</h3>
                </div>
                <div class="panel-body">

                    <asp:GridView ID="gvProduct" runat="server" AutoGenerateColumns="False" 
                        onselectedindexchanged="gvProduct_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="PId" HeaderText="ID" />
                            <asp:BoundField DataField="PName" HeaderText="Name" />
                            <asp:BoundField DataField="PPrice" HeaderText="Price" />
                            <asp:BoundField DataField="PDescription" HeaderText="Description" />
                            <asp:TemplateField>
                                <HeaderTemplate>Photo</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" Height="100" Width="100" ImageUrl='<%# "~/Image/"+Eval("Image") %>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField HeaderText="Edit" SelectText="Edit" ShowSelectButton="True" />
                            <asp:CommandField HeaderText="Delete" SelectText="Delete" 
                                ShowDeleteButton="True" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

