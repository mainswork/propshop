﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class AboutusForm : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                this.bindData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    protected void bindData() {
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
        }
        /*Management team */
        String manageMentTeam = "";
        String contactPerson = "";
        String query = "select * from management_team";
        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
            manageMentTeam += dr["name"].ToString() + "-";
        }

        if (manageMentTeam.Length > 0)
        {
            manageMentTeam = manageMentTeam.Substring(0, manageMentTeam.Length - 1);
        }
        
        managementTeamPreData.Value = manageMentTeam;
        dr.Close();
        /*Management team*/
        String query1 = "select * from contact_details";
        SqlCommand cmd1 = new SqlCommand(query1, conn);
        SqlDataReader dr1 = cmd1.ExecuteReader();
        while (dr1.Read())
        {
            contactPerson += dr1["contact_name"].ToString() + "," + dr1["mob_no"].ToString() +","+ dr1["email_id"].ToString() + "-";
        }

        if (contactPerson.Length > 0)
        {
            contactPerson = contactPerson.Substring(0, contactPerson.Length - 1);
        }
        dr1.Close();
        contactPersonPreData.Value = contactPerson;
        String query2 = "select * from about_us";
        SqlCommand cmd2 = new SqlCommand(query2, conn);
        SqlDataReader dr2 = cmd2.ExecuteReader();
        while (dr2.Read())
        {
            txtMissionVison.Text = dr2["mission_vision"].ToString();
            txtAboutCompany.Text = dr2["about_co"].ToString();
            txtAddress.Text = dr2["co_address"].ToString();
            txtCSR.Text = dr2["csr"].ToString();
            txtlat.Value = dr2["lat"].ToString();
            txtlong.Value = dr2["long"].ToString();
        }

        dr2.Close();
        conn.Close();

    }
    protected void btnAboutUsSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            String mission_vision = txtMissionVison.Text;
            String about_co = txtAboutCompany.Text;
            String co_address = txtAddress.Text;
            String csr = txtCSR.Text;
            String lat = txtlat.Value;
            String lon = txtlong.Value;
            SqlCommand cmd = new SqlCommand("update about_us set " +
                "mission_vision=@missionVision," +
                  "about_co=@aboutCo," +
                  "co_address=@coAddress," +
                  "csr=@csr," +
                  "lat=@lat,"+
                  "long=@long"+
                " where about_us_id=1", conn);
            cmd.Parameters.AddWithValue("@missionVision", mission_vision);
            cmd.Parameters.AddWithValue("@aboutCo", about_co);
            cmd.Parameters.AddWithValue("@coAddress", co_address);
            cmd.Parameters.AddWithValue("@csr", csr);
            cmd.Parameters.AddWithValue("@lat", lat);
            cmd.Parameters.AddWithValue("@long", lon);

            int rowsAffected = cmd.ExecuteNonQuery();
            if (rowsAffected > 0)
            {
                //team members update
                cmd.CommandText = "delete from management_team where about_us_id=1";
                cmd.ExecuteNonQuery();
                String teamMemberList = txtTeamMemberList.Value;

                String[] teamMembers = teamMemberList.Split(',');
                for (int i = 0; i < teamMembers.Length; i++)
                {
                    String name = teamMembers[i];
                    String position = "NA";
                    cmd = new SqlCommand("insert into management_team(name,about_us_id,position) values(@name,'1',@position)",conn);
                    cmd.Parameters.AddWithValue("@name", name);
                    cmd.Parameters.AddWithValue("@position", position);
                    cmd.ExecuteNonQuery();
                }

                // contact person list update
                cmd.CommandText = "delete from contact_details where about_us_id=1";
                cmd.ExecuteNonQuery();
                String contactPersonList = txtContactPersonList.Value;

                String[] contactPersons = contactPersonList.Split('-');
                for (int i = 0; i < contactPersons.Length; i++)
                {
                    String[] contactPerson = contactPersons[i].Split(',');


                    cmd.CommandText = "insert into contact_details(contact_name,mob_no,email_id,about_us_id) values(@contactName,@mobNo,@emailId,'1')";
                     cmd = new SqlCommand("insert into contact_details(contact_name,mob_no,email_id,about_us_id) values(@contactName,@mobNo,@emailId,'1')",conn);
                    cmd.Parameters.AddWithValue("@contactName", contactPerson[0]);
                    cmd.Parameters.AddWithValue("@mobNo", contactPerson[1]);
                    cmd.Parameters.AddWithValue("@emailId", contactPerson[2]);
                    cmd.ExecuteNonQuery();
                }
            }
            conn.Close();
            Response.Redirect("AboutUsForm.aspx");
        }
        catch (Exception ex)
        {
            // Response.Redirect("AboutUsForm.aspx");
        }
    }

}