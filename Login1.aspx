﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login1.aspx.cs" Inherits="Login1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <br /><br />
    <div class="row">
    
    <div class="col-md-6 center offset-3">
     
     
                  <div class="container">
                 
                <div class="card bg-light text-dark">
                    <div class="card-body">
                     <h4 class="card-title text-center display-4">Login Form</h4>
                     <hr />
                    <div class="form-group">
                
                        <div class="col-md-3 offset-3">

                            <asp:Label ID="lblEmail" runat="server" for="txtEmail" Text="Email"></asp:Label></div>
                        <div class="col-md-7 offset-3">
                            <asp:TextBox ID="txtEmail" placeholder="Email Address" type="email" CssClass="form-control" runat="server" required></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Invalid Email."
                                ControlToValidate="txtEmail" Display="Dynamic" ToolTip="Invalid Email." Text="*"
                                ValidationGroup="vgEmployee" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3 offset-3">
                            <asp:Label CssClass="control-label" For="txtPassword" ID="Password" runat="server" Text="Password"></asp:Label>
                        </div>
                        <div class="col-md-7 offset-3">
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" 
                                placeholder="Password" TextMode="Password" required></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 offset-4">                     
                            <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-success card-link" OnClick="btnLogin_Click" />
                        </div>
                        <div class="col-md-4">                   
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary card-link" OnClick="btnCancel_Click" />
                        </div>
                    </div>
                     <div class="form-group">
                                <div class="col-ld-3 offset-3">
                                  <asp:LinkButton ID="lblForgotPass" runat="server" Text="Forgot Password?" 
                                      onclick="lblForgotPass_Click" ></asp:LinkButton>
                            </div>
                        </div>
                       <div class="form-group">
                                 <div class="col-ld-3 offset-3">
                                        <asp:Label CssClass="control-label"  ID="lblloginfailure" runat="server" Text="Invalid Login Id or Password" Visible="False" ForeColor="Red"></asp:Label>
                                </div>
                        </div>
                         <div class="form-group">
                            <div class="col-ld-3 offset-3">
                                     <asp:Label CssClass="control-label"  ID="Label1" runat="server" Text=" Not Registered Yet?"  ></asp:Label>
                              &nbsp&nbsp&nbsp&nbsp 
                                    <asp:LinkButton ID="btnSignIn" runat="server" Text="Sign In" OnClick="btnSignIn_Click"></asp:LinkButton>
                        </div>
                    </div> 
                  </div>
                </div>
               
                
                    

                       
                   
                
            </div>
        </div>
     
        </div>
       
</asp:Content>

