﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="Client.aspx.cs" Inherits="Client" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">

    $(document).ready(function () {
    });

    </script>

    <style type="text/css">
        td, th
        {
            text-align: center;
        }
                .container
         {
             margin-left:3% !important;
             margin-right:3% !important;
             }
             /*css for length (display records) start*/
     
 
	.dataTables_length select
	{
	    display:block !important;
	    
	    }

    </style>
    <div class="row">
       
            <h5 class="center">
                Client Information</h5>
        </div>
        <div class="row">
            <div class="col s12 m12 l12">

               
              
                          <asp:GridView ID="gvClient" runat="server" AutoGenerateColumns="False" Visible="True"
                            DataKeyNames="Purchase_id" CssClass="table table-bordered table-condensed table-hover table-responsive" BackColor="White">
                            <AlternatingRowStyle BackColor="LightGray" />
                            <Columns>
                                 <asp:BoundField DataField="" HeaderText="Sr. No" />
                                <asp:BoundField DataField="cust_name" HeaderText="Name" />
                                <asp:BoundField DataField="cust_email" HeaderText="Email" />
                                <asp:BoundField DataField="cust_contact" HeaderText="Contact" />
                                <asp:BoundField DataField="comment" HeaderText="comment" />
                                 <asp:BoundField DataField="date" HeaderText="Date" />
                                
                                <asp:TemplateField HeaderText="View Property">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="imgShowProperty" CommandArgument='<%# Eval("cust_contact") %>' CausesValidation="False"
                                            OnClick="imgShowProperty_Click" CssClass="fa fa-building-o" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        </div>
                        </div>
                        <div class="row">

                        <%--<asp:DataList ID="dlstAllProperty" runat="server" Font-Size="Small" RepeatColumns="5" RepeatDirection="Horizontal" Width="400px" Height="300px">
                                                 <ItemStyle HorizontalAlign="Center"  />
                                                <ItemTemplate>--%>
                                                  <asp:Repeater ID="dlstAllProperty" runat="server">
                                            <ItemTemplate>
                                                        <div class="col s12 m4 l2">
                                                            
                                                            <asp:HyperLink ID="HyperLink1" class="preview" NavigateUrl='<%# Bind("images") %>'
                                                                runat="server">
                                                                <asp:ImageButton Width="100%" Style="border: 3px solid #ccc;" Height="170px" ID="imgProperty"
                                                                    OnClick="imgProperty_Click" CommandArgument='<%# Eval("Prop_id") %>' ImageUrl='<%# Bind("images") %>'
                                                                    runat="server" />
                                                            </asp:HyperLink>
                                                           
                                                               <p><b>Property Id:</b>
                                                                <asp:Label ID="lblPropID" runat="server" Text='<%# Bind("Prop_id") %>'></asp:Label></p>
                                                               
                                                                <p><b>Price:</b>
                                                                <asp:Label ID="lblPrice" runat="server" Text=' <%# Bind("price") %>'></asp:Label></p>
                                                            </div>
                                                       
                                                </ItemTemplate>
                                                </asp:Repeater>
                                                <%--<SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                            </asp:DataList>--%>
                            
                        
                    </div>
               
            
   
</asp:Content>
