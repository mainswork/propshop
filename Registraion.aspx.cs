﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Registraion : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PropshopConnectionString"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
       //SqlConnection conn = new SqlConnection(@"Data Source=NehalJethwa;Initial Catalog=Propshop;Integrated Security=True");
        try
        {
            if (conn.State != ConnectionState.Open)
                {
                conn.Open();
            }
            String query = "insert into [customer](name,email,contact,password,SecurityQuestion,Answer) values (@name,@email,@contact,@password,@question,@ans);";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@contact", txtMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@password", txtPassword.Text.Trim());
            cmd.Parameters.AddWithValue("@question", Convert.ToString(ddlSecurity.SelectedItem));
            cmd.Parameters.AddWithValue("@ans",txtAns.Text.Trim());
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("Login1.aspx");
        }
        catch (Exception ex)
        {
           
        }
    }
}

